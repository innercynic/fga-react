self.addEventListener('message', function(e) { 
  console.log(e);
  sortSongSetList(e.data); 
});

      console.log("Starting to sort list");
      songSortingWebWorker = new Worker('app/songComparer.js');
      songSortingWebWorker.addEventListener("message", processWorkerResponse, false);
      songSortingWebWorker.postMessage(songList);


      sortSongsDeffered = $q.defer();

          function processWorkerResponse(workerResponse) {
      sortSongsDeffered.resolve(workerResponse.data);
      resetWebWorker();
    }

    function resetWebWorker() {
      songSortingWebWorker.terminate();
      songSortingWebWorker = null;
    }

        var service = {
      compareTunings: compareTunings,
      sortSongSetList: sortSongSetList,
      getSongToSongTuningInstructions: getSongToSongTuningInstructions,
      getSimilarSongs: getSimilarSongs,
      getGuitarStringsRange: getGuitarStringsRange
    };