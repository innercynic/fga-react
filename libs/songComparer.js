/* eslint-disable */

const STRING_NOTE_RANGE = 8;

const START_INDEX = 13;

const chromaticScale = [
  'A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#',
  'A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#',
  'A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#',
];

const guitarstrings = [{
  name: 'first',
  noteRange: getNoteRange(chromaticScale.indexOf('E', START_INDEX), STRING_NOTE_RANGE),
}, {
  name: 'second',
  noteRange: getNoteRange(chromaticScale.indexOf('B', START_INDEX), STRING_NOTE_RANGE),
}, {
  name: 'third',
  noteRange: getNoteRange(chromaticScale.indexOf('G', START_INDEX), STRING_NOTE_RANGE),
}, {
  name: 'fourth',
  noteRange: getNoteRange(chromaticScale.indexOf('D', START_INDEX), STRING_NOTE_RANGE),
}, {
  name: 'fifth',
  noteRange: getNoteRange(chromaticScale.indexOf('A', START_INDEX), STRING_NOTE_RANGE),
}, {
  name: 'sixth',
  noteRange: getNoteRange(chromaticScale.indexOf('E', START_INDEX), STRING_NOTE_RANGE),
}].reverse();

const tuningComparisonCache = {};

/**
 * Returns the total difference in steps after tuning between two guitar tunings.
 * Uses memoization for some performance gains.
 */
function compareTunings(stringtunings1, stringtunings2) {
  const tuningComparisonKey = stringtunings1.join('') + stringtunings2.join('');

  if (tuningComparisonKey in tuningComparisonCache) {
    return tuningComparisonCache[tuningComparisonKey];
  }

  const stepsDifferentTotal = loop(guitarstrings.length)
    .reduce((total, string, stringIndex) => total += compareStrings(stringtunings1[stringIndex], stringtunings2[stringIndex], guitarstrings[stringIndex]), 0);

  tuningComparisonCache[tuningComparisonKey] = stepsDifferentTotal;

  return stepsDifferentTotal;
}

/**
 * Returns the difference in steps between two given values for the same guitar string.
 */
function compareStrings(stringtuning1, stringtuning2, guitarString) {
  const stringOneChromaticIndex = guitarString.noteRange.indexOf(stringtuning1);
  const stringTwoChromaticIndex = guitarString.noteRange.indexOf(stringtuning2);
  let numberOfStepsDifference = 0;

  if (stringOneChromaticIndex < stringTwoChromaticIndex) {
    numberOfStepsDifference = stringTwoChromaticIndex - stringOneChromaticIndex;
  } else if (stringOneChromaticIndex > stringTwoChromaticIndex) {
    numberOfStepsDifference = stringOneChromaticIndex - stringTwoChromaticIndex;
  }
  return numberOfStepsDifference;
}


/**
 * Sort song set list by using brute force and comparing all possibile permutations.
 */
export function sortSetlistBruteForce(setList) {
  const permArr = permute(setList);

  let mostEfficientSongSequence;
  let mostEfficientSongSequenceTotal;

  for (let x = 0; x < permArr.length; x++) {
    let stepsDifferentTotal = 0;
    for (let i = 0; i < permArr[x].length; i++) {
      const currentSong = permArr[x][i];
      const nextSong = permArr[x][i + 1];
      if (nextSong) {
        const stepsDifferent = compareTunings(currentSong.stringtunings, nextSong.stringtunings);
        stepsDifferentTotal += stepsDifferent;
      }
    }

    if (!mostEfficientSongSequenceTotal || mostEfficientSongSequenceTotal > stepsDifferentTotal) {
      mostEfficientSongSequenceTotal = stepsDifferentTotal;
      mostEfficientSongSequence = permArr[x];
    }
  }

  return mostEfficientSongSequence || 'Problem';
}

/**
 * Because logic based (sortSetlist) function is not perfect, 
 * performing multiple passes with same set list but in a different order can yield different results.
 *
 * By performing sortSetlist with randomly ordered variables of given set list, we can locate the optimal tuning.
 * Much faster than brute force as setlist size increases.
 *
 * Giving a higher numberOfPasses value can yield better results.
 *
 * Default of 15 passes usually yields optimal result.
 */
export function sortSetlistMultiplePass(setlistOriginal, numberOfPasses = 15) {
  return loop(numberOfPasses)
    .map(() => sortSetlist(fisherYatesShuffle(setlistOriginal)))
    .reduce((optimal, sortResult) => {
      const tuningDifference = getTuningDifferenceInSteps(sortResult);

      return optimal == null || tuningDifference < optimal.tuningDifference
        ? { sortResult, tuningDifference }
        : optimal;
    }, null)
    .sortResult;
}

/**
 * Logic based sorting of set list.
 * Involves placing set list items into new ordered set list one at a time, positioning by finding optimal placement based on minimal tuning.
 */
function sortSetlist(setlistOriginal) {
  const setlist = [...setlistOriginal];
  const sortedSetlist = setlist.splice(0, 1);

  while (setlist.length > 0) {
    const nextSetlistItem = setlist.splice(0, 1)[0];
    const closest = getClosestTuning(sortedSetlist, nextSetlistItem);
    const nextDifferentTuning = getNextDifferentTuning(closest, sortedSetlist);
    const previousDifferentTuning = getPreviousDifferentTuning(closest, sortedSetlist);

    let placementIndex;

    if (nextDifferentTuning && previousDifferentTuning) {
      const difBetweenCurrentAndPreviousDifferent = compareTunings(nextSetlistItem.stringtunings, sortedSetlist[previousDifferentTuning.index].stringtunings);
      const difBetweenCurrentAndNextDifferent = compareTunings(nextSetlistItem.stringtunings, sortedSetlist[nextDifferentTuning.index].stringtunings);

      if (closest.difference + difBetweenCurrentAndNextDifferent < closest.difference + difBetweenCurrentAndPreviousDifferent) {
        placementIndex = nextDifferentTuning.index;
      } else {
        placementIndex = previousDifferentTuning.index + 1;
      }
    } else if (nextDifferentTuning && !previousDifferentTuning) {
      const difBetweenCurrentAndNextDifferent = compareTunings(nextSetlistItem.stringtunings, sortedSetlist[nextDifferentTuning.index].stringtunings);

      if (closest.difference + difBetweenCurrentAndNextDifferent <= nextDifferentTuning.difference) {
        placementIndex = nextDifferentTuning.index;
      } else {
        placementIndex = 0;
      }
    } else if (previousDifferentTuning && !nextDifferentTuning) {
      const difBetweenCurrentAndPreviousDifferent = compareTunings(nextSetlistItem.stringtunings, sortedSetlist[previousDifferentTuning.index].stringtunings);

      if (closest.difference + difBetweenCurrentAndPreviousDifferent <= previousDifferentTuning.difference) {
        placementIndex = previousDifferentTuning.index + 1;
      }
    }

    if (typeof placementIndex !== 'undefined') {
      sortedSetlist.splice(placementIndex, 0, nextSetlistItem);
    } else {
      sortedSetlist.push(nextSetlistItem);
    }
  }

  return sortedSetlist;
}

/**
 * Returns the number of tuning steps involved when iterating over a given set list ordering.
 */
export function getTuningDifferenceInSteps(setlist) {
  return setlist.reduce((acc, song, index, setlist) =>
    acc += index === 0 ? 0 : compareTunings(setlist[index - 1].stringtunings, song.stringtunings), 0);
}

function getClosestTuning(setlist, song) {
  return setlist.reduce((closestMatch, currentSetlistItem, currentSetlistItemIndex) => {
    const difference = compareTunings(song.stringtunings, currentSetlistItem.stringtunings);
    if (closestMatch === null || difference < closestMatch.difference) {
      return { song: currentSetlistItem, index: currentSetlistItemIndex, difference };
    }
    return closestMatch;
  }, null);
}

/**
 * Given the closest match in a sorted set list, return the next item in the sorted set list that has a different tuning.
 * @param  {[type]} closestMatch  The item in the sorted set list with the most similar tuning
 * @param  {[type]} sortedSetlist The sorted set list
 * @return {object} An object containing index of the next item in the 
 *                  sorted set list with a different tuning to the closest match.           
 */
function getPreviousDifferentTuning(closestMatch, sortedSetlist) {
  let counter = 1;
  let previousDifferentTuning = null;
  while (sortedSetlist[closestMatch.index - counter]) {
    const currentIndex = closestMatch.index - counter;
    if (isSameTuning(closestMatch.song.stringtunings, sortedSetlist[currentIndex].stringtunings)) {
      counter += 1;
      continue;
    }

    const difference = compareTunings(closestMatch.song.stringtunings, sortedSetlist[currentIndex].stringtunings);
    previousDifferentTuning = { index: currentIndex, difference };
    break;
  }
  return previousDifferentTuning;
}

/**
 * Given the closest match in a sorted set list, return the previous item in the sorted set list that has a different tuning.
 * @param  {[type]} closestMatch  The item in the sorted set list with the most similar tuning
 * @param  {[type]} sortedSetlist The sorted set list
 * @return {object} An object containing index of the previous item in the 
 *                  sorted set list with a different tuning to the closest match.           
 */
function getNextDifferentTuning(closestMatch, sortedSetlist) {
  let counter = 1;
  let nextDifferentTuning = null;
  while (sortedSetlist[closestMatch.index + counter]) {
    const currentIndex = closestMatch.index + counter;
    if (isSameTuning(closestMatch.song.stringtunings, sortedSetlist[currentIndex].stringtunings)) {
      counter += 1;
      continue;
    }

    const difference = compareTunings(closestMatch.song.stringtunings, sortedSetlist[currentIndex].stringtunings);
    nextDifferentTuning = { index: currentIndex, difference };
    break;
  }
  return nextDifferentTuning;
}

/**
 * Returns true if two given tunings are the same.
 */
function isSameTuning(stringtunings1, stringtunings2) {
  let isSame = true;

  for (let stringIndex = 0; stringIndex < 6; stringIndex++) {
    if (stringtunings1[stringIndex] !== stringtunings2[stringIndex]) {
      isSame = false;
      break;
    }
  }
  return isSame;
}

/**
 * Returns a set list sorted by tunings in alphabetical order.
 */
export function sortSetlistByTuningAlphabetically(setlist) {
  const setlistCopy = [...setlist];
  return setlistCopy.sort((a, b) => a.stringtunings.join('').localeCompare(b.stringtunings.join('')));
}

/**
 * Returns a set list sorted by name in alphabetical order.
 */
export function sortSetlistByNameAlphabetically(setlist) {
  const setlistCopy = [...setlist];
  return setlistCopy.sort((a, b) => a.name.localeCompare(b.name));
}

/**
 * Returns a set list sorted by artist name in alphabetical order.
 */
export function sortSetlistByArtistAlphabetically(setlist) {
  const setlistCopy = [...setlist];
  return setlistCopy.sort((a, b) => a.artist.name.localeCompare(b.artist.name));
}

export function getSimilarSongs(song, songList, limit) {
  const similarSongsList = [];

  for (let x = 0; x < songList.length; x++) {
    const diff = compareTunings(song.stringtunings, songList[x].stringtunings);

    if (diff <= limit) {
      similarSongsList.push(songList[x]);
    }
  }

  return similarSongsList;
}

/**
 * Returns an array of song to song tuning instructions for a setlist.
 */
export function getSetlistTuningInstructions(setlist) {
  return setlist.reduce((acc, song, index, setlist) => {
    if(index !== 0){
      acc.push({
        from: setlist[index - 1].name,
        to: song.name,
        instructions: getSongToSongTuningInstructions(setlist[index - 1], song),
      });
    }

    return acc;
  }, []);
}

/**
 * Returns text based instructions for tuning between two given songs
 */
export function getSongToSongTuningInstructions(song1, song2) {
  let instructions = [],
      totalSemitonesChanged = 0;

  for (let x = 0, y = 6; y > 0; x++, y--) {
    let diff = compareStrings(song1.stringtunings[x], song2.stringtunings[x], guitarstrings[x]);

    totalSemitonesChanged += diff;
    let postFix = 'th';

    if (y == 1) postFix = 'st';
    else if (y == 2) postFix = 'nd';
    else if (y == 3) postFix = 'rd';

    let direction = 'up';
    if (guitarstrings[x].noteRange.indexOf(song1.stringtunings[x]) > guitarstrings[x].noteRange.indexOf(song2.stringtunings[x])) {
      direction = 'down';
    }

    if (diff !== 0) {
      instructions.push(`${y + postFix} string: Tune from ${song1.stringtunings[x]} ${direction} to ${song2.stringtunings[x]}.`);
    }
  }

  if(totalSemitonesChanged === 0){
    instructions.push("Tuning remains the same.");
  }else{
    instructions.push(`Tuning difference: ${totalSemitonesChanged} semitones.`);
    instructions.splice(0, 0, `Tune from ${song1.stringtunings.join('')} to ${song2.stringtunings.join('')}`);
  }

  return instructions;
}

/**
 * Returns the note range for a given string.
 */
function getNoteRange(stringStandardNoteIndex, numberOfNotes) {
  const startIndexModifier = getNoteStartIndexModifier(numberOfNotes);

  function getNoteStartIndexModifier() {
    const twoThirdsRounded = Math.round((numberOfNotes / 3) * 2);

    return twoThirdsRounded;
  }

  const noteRange = [];
  const startIndex = (stringStandardNoteIndex - startIndexModifier + 1);

  for (let i = startIndex; i < startIndex + numberOfNotes; i++) {
    noteRange.push(chromaticScale[i]);
  }

  return noteRange;
}

/**
 * Returns a randomly sorted array from a given array.
 */
function fisherYatesShuffle(array) {
  const shuffledArray = [...array];
  let currentIndex = shuffledArray.length,
    temporaryValue,
    randomIndex;

  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = shuffledArray[currentIndex];
    shuffledArray[currentIndex] = shuffledArray[randomIndex];
    shuffledArray[randomIndex] = temporaryValue;
  }

  return shuffledArray;
}

/**
 * Returns an array containing all possible permutations of a given array.
 * Used for brute force sorting.
 */
function permute(input, permArr = [], usedChars = []) {
  let i,
    ch;

  for (i = 0; i < input.length; i++) {
    ch = input.splice(i, 1)[0];
    usedChars.push(ch);
    if (input.length == 0) {
      permArr.push(usedChars.slice());
    }
    permute(input, permArr, usedChars);
    input.splice(i, 0, ch);
    usedChars.pop();
  }
  return permArr;
}

/**
 * A functional mechinsim for looping a given number of times.
 * Simply to replace conventional loops for a known and fixed number of loops.
 */
function loop(loopCount) {
  return Array(loopCount).fill();
}
