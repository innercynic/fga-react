import { takeEvery } from 'redux-saga';

import { CREATE_, USER } from '../actions/ActionConstants';
import postDataAsync from './api/post';

export function* watchCreateUser() {
  yield takeEvery(CREATE_ + USER, postDataAsync);
}
