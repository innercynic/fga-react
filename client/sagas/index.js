import { watchRequestArtists, watchRequestArtist, watchCreateArtist } from './artists';
import { watchCreateSong, watchRequestSongs, watchRequestSong } from './songs';
import { watchCreateSetlist, watchRequestSetlists, watchRequestSetlist } from './setlists';
import { watchLoginAsync } from './login';
import { watchCreateUser } from './users';

export default function* rootSaga() {
  yield [
    watchRequestArtists(),
    watchRequestArtist(),
    watchCreateArtist(),
    watchCreateSong(),
    watchRequestSongs(),
    watchRequestSong(),
    watchCreateSetlist(),
    watchRequestSetlists(),
    watchRequestSetlist(),
    watchLoginAsync(),
    watchCreateUser(),
  ];
}
