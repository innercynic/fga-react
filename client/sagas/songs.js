import { takeEvery } from 'redux-saga';

import { REQUEST_, CREATE_, SONG, SONGS } from '../actions/ActionConstants';

import getDataAsync from './api/get';
import postDataAsync from './api/post';

export function* watchRequestSongs() {
  yield takeEvery(REQUEST_ + SONGS, getDataAsync);
}

export function* watchRequestSong() {
  yield takeEvery(REQUEST_ + SONG, getDataAsync);
}

export function* watchCreateSong() {
  yield takeEvery(CREATE_ + SONG, postDataAsync);
}
