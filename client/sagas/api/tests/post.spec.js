import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import sinon from 'sinon';

import postDataAsync from '../post';
import * as ApiUtils from '../../../utils/api/ApiUtils';
import { _SUCCESS } from '../../../actions/ActionConstants';

sinon.stub(ApiUtils, 'createUrlFromAction').returns("/mockurl");
const mockAction = { type: 'MOCK_ACTION', data: {} };
const mockResponse = { data: {}, status: 200 };

const generator = postDataAsync(mockAction);

test('it should yield the axios post call', () => {
  expect(JSON.stringify(generator.next().value)).toEqual(JSON.stringify(call(axios.post, "/mockurl", {}, {headers:{}})));
});

test('then it should yield a success action', () => {
  expect(generator.next(mockResponse).value).toEqual(put({ type: `${mockAction.type}${_SUCCESS}`, response: {} }));
});

test('finally it should be done', () => {
  expect(generator.next().done).toEqual(true);
});
