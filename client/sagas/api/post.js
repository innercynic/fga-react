import { call, put } from 'redux-saga/effects';
import axios from 'axios';

import { _SUCCESS, _FAIL } from '../../actions/ActionConstants';
import { createUrlFromAction, createFormData } from '../../utils/api/ApiUtils';


function createActionFromResponse(response, initialAction) {
  if (response.status === 401 || response.status === 409) {
    return {
      type: initialAction.type + _FAIL,
      message: response.data.message,
    };
  }
  return {
    type: initialAction.type + _SUCCESS,
    response: response.data,
  };
}

function getStatusCodeValidator() {
  return status => (status >= 200 && status < 300)
  || status === 401
  || status === 409;
}

export default function* postDataAsync(action) {
  try {
    const url = createUrlFromAction(action);
    let data = action.data;
    const config = {
      validateStatus: getStatusCodeValidator(),
      headers: {},
    };

    if (action.isMultiPart) {
      config.headers = { ...config.headers, 'content-type': 'multipart/form-data' };
      data = createFormData(action.data);
    }

    if (localStorage.auth_token) {
      config.headers = { ...config.headers, Authorization: localStorage.auth_token };
    }

    const response = yield call(axios.post, url, data, config);

    yield put(createActionFromResponse(response, action));
  } catch (e) {
    yield put({ type: action.type + _FAIL,
      response: {
        message: 'Unexpected server error, contact system administrator.',
      } });
    console.log(`Unexpected Error - ${e}`);
  }
}
