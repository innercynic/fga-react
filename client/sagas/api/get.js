import { call, put } from 'redux-saga/effects';
import axios from 'axios';

import { _SUCCESS, _FAIL } from '../../actions/ActionConstants';
import { createUrlFromAction } from '../../utils/api/ApiUtils';


function createActionFromResponse(response, initialAction) {
  if (response.status === 400 || response.status === 404) {
    return {
      type: initialAction.type + _FAIL,
      message: response ? response.data.message : {},
      statusCode: response ? response.status : {},
    };
  }
  return {
    type: initialAction.type + _SUCCESS,
    response: response ? response.data : {},
  };
}

function getStatusCodeValidator() {
  return status => (status >= 200 && status < 300)
  || status === 400
  || status === 404;
}

export default function* getDataAsync(action) {
  try {
    const url = createUrlFromAction(action);
    const config = {
      validateStatus: getStatusCodeValidator(),
      headers: {},
    };

    if (localStorage.auth_token) {
      config.headers = { ...config.headers, Authorization: localStorage.auth_token };
    }

    const response = yield call(axios.get, url, config);

    yield put(createActionFromResponse(response, action));
  } catch (e) {
    // TODO dispatch failure action
    console.log(`${action.type} failed`, e);
  }
}
