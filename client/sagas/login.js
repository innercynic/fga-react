import { takeEvery } from 'redux-saga';

import { LOGIN } from '../actions/user/auth';
import postDataAsync from './api/post';

export function* watchLoginAsync() {
  yield takeEvery(LOGIN, postDataAsync);
}
