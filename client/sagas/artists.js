import { takeEvery } from 'redux-saga';

import { REQUEST_, CREATE_, ARTIST, ARTISTS } from '../actions/ActionConstants';

import getDataAsync from './api/get';
import postDataAsync from './api/post';

export function* watchRequestArtists() {
  yield takeEvery(REQUEST_ + ARTISTS, getDataAsync);
}

export function* watchRequestArtist() {
  yield takeEvery(REQUEST_ + ARTIST, getDataAsync);
}

export function* watchCreateArtist() {
  yield takeEvery(CREATE_ + ARTIST, postDataAsync);
}
