import { takeEvery } from 'redux-saga';

import { REQUEST_, CREATE_, SETLIST, SETLISTS } from '../actions/ActionConstants';

import getDataAsync from './api/get';
import postDataAsync from './api/post';

export function* watchRequestSetlists() {
  yield takeEvery(REQUEST_ + SETLISTS, getDataAsync);
}

export function* watchRequestSetlist() {
  yield takeEvery(REQUEST_ + SETLIST, getDataAsync);
}

export function* watchCreateSetlist() {
  yield takeEvery(CREATE_ + SETLIST, postDataAsync);
}
