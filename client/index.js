import React from 'react';
import { render } from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import createHistory from 'history/createBrowserHistory';
import { Route } from 'react-router-dom';
import { reducer as formReducer } from 'redux-form';

import App from './components/app/App';
import applicationReducers from './reducers';
import rootSaga from './sagas';
import { songComparerMiddleware, themeChangeMiddleware } from './middleware';

// Global styles, css resets and font type declarations
import './styles/global';

const history = createHistory();
const historyMiddleware = routerMiddleware(history);
const sageMiddleware = createSagaMiddleware();
const middleware = [
  historyMiddleware, sageMiddleware,
  songComparerMiddleware, themeChangeMiddleware,
];

/* eslint-disable no-underscore-dangle */
const store = createStore(
  combineReducers({
    ...applicationReducers,
    router: routerReducer,
    form: formReducer,
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(...middleware),
);
/* eslint-enable */

sageMiddleware.run(rootSaga);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route path="/" component={App} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
