import setlistReducer from '../setlist';
import {
  addToSetlist,
  removeFromSetlist,
  repositionSetlistItem,
  isAddingSetlistName,
  setNewSetlist,
  setSetlistName,
  setSetlistSongs,
  setTuningInstructions,
} from '../../../actions/setlists/setlist';

test('adds an item to a setlist', () => {
  let selected = {
    name: "MockSetlistName",
    songs: [],
  };

  let state = setlistReducer({selected}, addToSetlist({name: "MockSongName"}));

  expect(state.selected.songs).toHaveLength(1);
});

test('removes an item from a setlist', () => {
  let selected = {
    name: "MockSetlistName",
    songs: [{name: "MockSongName"}],
  };

  let state = setlistReducer({selected}, removeFromSetlist(1));

  expect(state.selected.songs).toHaveLength(0);
});

test('sets a setlists song list', () => {
  let selected = {
    name: "MockSetlistName",
    songs: [],
  };

  let state = setlistReducer({selected}, setSetlistSongs([{name: "MockSong1"}, {name: "MockSong2"}]));

  expect(state.selected.songs).toHaveLength(2);
});

test('repositions a setlist item (song)', () => {
  let selected = {
    name: "MockSetlistName",
    songs: [{name: "MockSong1"}, {name: "MockSong2"}],
  };

  let state = setlistReducer({selected}, repositionSetlistItem(0, 1));

  expect(state.selected.songs[0].name).toEqual("MockSong2");
});

test('sets a new setlist', () => {
  let selected = {
    _id: "123",
    name: "MockSetlistName",
    songs: [{name: "MockSong1"}, {name: "MockSong2"}],
  };

  let state = setlistReducer({selected}, setNewSetlist());

  expect(state.selected.songs).toHaveLength(0);
  expect(state.selected._id).toBeFalsy();
});

test('toggles is adding setlist name state', () => {
  let state = setlistReducer(undefined, isAddingSetlistName(true));

  expect(state.isAddingSetlistName).toBeTruthy();
});

test('sets the selected setlists name', () => {
  let state = setlistReducer(undefined, setSetlistName("MockSetlistName"));

  expect(state.selected.name).toEqual("MockSetlistName");
});

test('sets the selected setlists tuning instructions', () => {
  let mockInstructions = {mockInstruction: "Do this."};
  let state = setlistReducer(undefined, setTuningInstructions(mockInstructions));

  expect(state.tuningInstructions).toEqual(mockInstructions);
});

