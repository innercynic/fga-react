import { createReducerForCollection } from '../factory/ReducerFactory';
import { SETLISTS } from '../../actions/ActionConstants';

export default createReducerForCollection({ type: SETLISTS });
