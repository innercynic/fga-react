import { arrayMove } from 'react-sortable-hoc';

import { createReducerForResource } from '../factory/ReducerFactory';
import { SELECT_, CREATE_, _SUCCESS, SETLIST } from '../../actions/ActionConstants';

import {
  ADD_TO_SETLIST,
  REMOVE_FROM_SETLIST,
  REPOSITION_SETLIST_ITEM,
  IS_ADDING_SETLIST_NAME,
  SET_NEW_SETLIST,
  SET_SETLIST_NAME,
  SET_SETLIST_SONGS,
  SET_TUNING_INSTRUCTIONS,
} from '../../actions/setlists/setlist';

const extendedHandlers = {};

extendedHandlers[IS_ADDING_SETLIST_NAME] = (state, action) => ({
  ...state,
  isAddingSetlistName: action.payload,
});

extendedHandlers[SET_SETLIST_NAME] = (state, action) => ({
  ...state,
  changed: true,
  selected: {
    ...state.selected,
    name: action.payload,
  },
});

extendedHandlers[SET_SETLIST_SONGS] = (state, action) => ({
  ...state,
  changed: true,
  selected: {
    ...state.selected,
    songs: action.payload,
  },
});

extendedHandlers[SET_TUNING_INSTRUCTIONS] = (state, action) => ({
  ...state,
  tuningInstructions: action.payload,
});

extendedHandlers[ADD_TO_SETLIST] = (state, action) => {
  const existingEntry = state.selected.songs.filter(song => song._id === action.payload._id);

  if (existingEntry.length > 0) {
    return state;
  }

  return {
    ...state,
    changed: true,
    selected: {
      ...state.selected,
      songs: [...state.selected.songs, action.payload],
    },
  };
};

extendedHandlers[REMOVE_FROM_SETLIST] = (state, action) => ({
  ...state,
  changed: true,
  selected: {
    ...state.selected,
    songs: state.selected.songs.filter(song => song._id !== action.payload._id),
  },
});

extendedHandlers[REPOSITION_SETLIST_ITEM] = (state, action) => {
  if (action.oldIndex === action.newIndex) return state;

  return {
    ...state,
    changed: true,
    selected: {
      ...state.selected,
      songs: arrayMove(state.selected.songs, action.oldIndex, action.newIndex),
    },
  };
};

extendedHandlers[SET_NEW_SETLIST] = state => ({
  ...state,
  changed: false,
  selected: {
    songs: [],
  },
});

extendedHandlers[SELECT_ + SETLIST] = (state, action) => ({
  ...state,
  selected: action.payload,
  changed: false,
});

extendedHandlers[CREATE_ + SETLIST + _SUCCESS] = (state, action) => ({
  ...state,
  isPostingData: false,
  selected: action.response,
  changed: false,
});

const extendedState = {
  isAddingSetlistName: false,
  changed: false,
  tuningInstructions: null,
};

export default createReducerForResource({
  type: SETLIST,
  extendedState,
  extendedHandlers,
});
