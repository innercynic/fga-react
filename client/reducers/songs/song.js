import { createReducerForResource } from '../factory/ReducerFactory';
import { SONG } from '../../actions/ActionConstants';

export default createReducerForResource({ type: SONG });
