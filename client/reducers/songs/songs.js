import { createReducerForCollection } from '../factory/ReducerFactory';
import { SONGS } from '../../actions/ActionConstants';

export default createReducerForCollection({ type: SONGS });
