import {
  REQUEST_,
  CLEAR_,

  _SUCCESS,
  _FAIL,
} from '../../actions/ActionConstants';

export const createInitialCollectionState = (extendedState = {}) => ({
  isFetching: false,
  data: [],
  hasMore: false,
  nextPage: 1,
  errorMessage: null,
  errorStatusCode: null,
  ...extendedState,
});

export const createHandlersForCollection = (type, extendedHandlers = {}) => {
  const handlers = {};

  handlers[REQUEST_ + type] = (state, action) => ({
    ...state,
    isFetching: true,
    currentQuery: action.query,
    nextPage: action.page,
  });

  handlers[REQUEST_ + type + _SUCCESS] = (state, action) => ({
    ...state,
    isFetching: false,
    hasMore: action.response.hasMore,
    data: state.nextPage > 1 ? [...state.data, ...action.response.data] : action.response.data,
    nextPage: action.response.hasMore ? state.nextPage + 1 : 1,
  });

  handlers[REQUEST_ + type + _FAIL] = (state, action) => ({
    ...state,
    isFetching: false,
    errorMessage: action.message,
    errorStatusCode: action.statusCode,
  });

  handlers[CLEAR_ + type] = state => ({
    ...state,
    isFetching: false,
    hasMore: false,
    data: [],
    nextPage: 1,
    errorMessage: '',
    errorStatusCode: '',
  });

  return {
    ...handlers,
    ...extendedHandlers,
  };
};
