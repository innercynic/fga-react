import {
  REQUEST_,
  CREATE_,
  SELECT_,
  CLEAR_,

  _SUCCESS,
  _FAIL,
} from '../../actions/ActionConstants';

export const createInitialResourceState = (extendedState = {}) => ({
  selected: null,
  isFetching: false,
  isPostingData: false,
  errorMessage: null,
  errorStatusCode: null,
  ...extendedState,
});

export const createHandlersForResource = (type, extendedHandlers = {}) => {
  const handlers = {};
  // RESOURCE SINGLE
  handlers[REQUEST_ + type] = state => ({
    ...state,
    isFetching: true,
  });

  handlers[REQUEST_ + type + _SUCCESS] = (state, action) => ({
    ...state,
    selected: action.response,
    isFetching: false,
  });

  handlers[REQUEST_ + type + _FAIL] = (state, action) => ({
    ...state,
    errorMessage: action.message,
    errorStatusCode: action.statusCode,
    isFetching: false,
  });

  handlers[CREATE_ + type] = state => ({
    ...state,
    isPostingData: true,
  });

  handlers[CREATE_ + type + _SUCCESS] = (state, action) => ({
    ...state,
    isPostingData: false,
    selected: action.response,
  });

  handlers[CREATE_ + type + _FAIL] = (state, action) => ({
    ...state,
    isPostingData: false,
    errorMessage: action.message,
    errorStatusCode: action.statusCode,
  });

  handlers[SELECT_ + type] = (state, action) => ({
    ...state,
    selected: action.payload,
  });

  handlers[CLEAR_ + type] = state => ({
    ...state,
    selected: null,
  });

  return {
    ...handlers,
    ...extendedHandlers,
  };
};
