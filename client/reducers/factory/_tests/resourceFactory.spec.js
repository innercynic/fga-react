import {
  CREATE_,
  REQUEST_,
  _SUCCESS,
  _FAIL,
} from '../../../actions/ActionConstants';

import { createReducerForResource } from '../ReducerFactory';
import { createResourceActionCreators }from '../../../actions/factory/ActionCreatorFactory';
import MockData from '../../../utils/test/MockData';

const MOCK = "MOCK";

const initialState = {
  selected: null,
  isFetching: false,
  isPostingData: false,
  errorMessage: '',
  errorStatusCode: '',
};

const {
  requestMock,
  createMock,
  selectMock,
  clearSelectedMock,
} = createResourceActionCreators(MOCK);

const mockResourceReducer = createReducerForResource({
  type: MOCK
});

describe('createReducerForResource', () => {
  test('isFetching should be true after dispatching request action', () => {
    const result = mockResourceReducer(initialState, requestMock({ id: 55 }));

    expect(result.isFetching).toBe(true);
  });

  test('state should contain data after dispatching request success action', () => {
    const result = mockResourceReducer(initialState, {
      type: REQUEST_ + MOCK + _SUCCESS,
      response: MockData.mockArtists[0]
    });

    expect(result.isFetching).toBe(false);
    expect(result.selected).toBe(MockData.mockArtists[0]);
  });

  test('state should contain error data after dispatching request fail action', () => {
    const result = mockResourceReducer(initialState, {
      type: REQUEST_ + MOCK + _FAIL,
      message: "Mock Request Error",
      statusCode: 999,
    });

    expect(result.isFetching).toBe(false);
    expect(result.errorStatusCode).toBe(999);
    expect(result.errorMessage).toBe("Mock Request Error");
  });

  test('isPostingData should be true after dispatching create action', () => {
    const result = mockResourceReducer(initialState, createMock({ name: "bob" }));

    expect(result.isPostingData).toBe(true);
  });

  test('state should contain mock data after dispatching create success action', () => {
    const result = mockResourceReducer(initialState, {
      type: CREATE_ + MOCK + _SUCCESS,
      response: MockData.mockArtists[0]
    });

    expect(result.isPostingData).toBe(false);
    expect(result.selected).toBe(MockData.mockArtists[0]);
  });

  test('state should contain error data after dispatching create fail action', () => {
    const result = mockResourceReducer(initialState, {
      type: CREATE_ + MOCK + _FAIL,
      message: "Mock Create Error",
      statusCode: 999,
    });

    expect(result.isPostingData).toBe(false);
    expect(result.errorStatusCode).toBe(999);
    expect(result.errorMessage).toBe("Mock Create Error");
  });

  test('selected artist should be set after dispatching select action', () => {
    const result = mockResourceReducer(initialState, selectMock(MockData.mockArtists[0]));

    expect(result.selected).toBe(MockData.mockArtists[0]);
  });

  test('selected artist should be null after dispatching clearSelected action', () => {
    const result = mockResourceReducer({...initialState,
      selected: MockData.mockArtists[0],
    }, clearSelectedMock());

    expect(result.selected).toBe(null);
  });
})