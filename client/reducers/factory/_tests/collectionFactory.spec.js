import {
  CREATE_,
  REQUEST_,
  _SUCCESS,
  _FAIL,
} from '../../../actions/ActionConstants';

import { createReducerForCollection } from '../ReducerFactory';
import { createCollectionActionCreators }from '../../../actions/factory/ActionCreatorFactory';
import MockData from '../../../utils/test/MockData';

const MOCKS = "MOCKS";

const initialState = {
  isFetching: false,
  data: [],
  hasMore: false,
  nextPage: 1,
  errorMessage: '',
  errorStatusCode: '',
};

const {
  requestMocks,
  clearMocks,
} = createCollectionActionCreators(MOCKS);

const mockCollectionReducer = createReducerForCollection({
  type: MOCKS
});


describe('createReducerForResource', () => {
  test('isFetching should be true after dispatching request action', () => {
    const result = mockCollectionReducer(initialState, requestMocks({}));

    expect(result.isFetching).toBe(true);
  });

  test('state should contain data after dispatching request success action', () => {
    const mockResponse = [MockData.mockArtists[0]];

    const result = mockCollectionReducer(initialState, {
      type: REQUEST_ + MOCKS + _SUCCESS,
      response: { data: mockResponse, hasMore: false },
    });

    expect(result.isFetching).toBe(false);
    expect(result.data).toEqual(mockResponse);
  });

  test('state should contain information about next page of data dispatching request success action', () => {
    const mockResponse = [MockData.mockArtists[0]];

    const result = mockCollectionReducer(initialState, {
      type: REQUEST_ + MOCKS + _SUCCESS,
      response: { data: mockResponse, hasMore: true },
    });

    expect(result.hasMore).toBe(true);
    expect(result.nextPage).toEqual(2);
  });

  test('state data should include next page of results after dispatching request success action', () => {
    const mockData = [MockData.mockArtists[0]];
    const mockNextPageResponse = [MockData.mockArtists[1]];

    const result = mockCollectionReducer({
      ...initialState,
      data: mockData,
      hasMore: true,
      nextPage: 2
      }, {
      type: REQUEST_ + MOCKS + _SUCCESS,
      response: { data: mockNextPageResponse, hasMore: false },
    });

    expect(result.data.includes(MockData.mockArtists[0])).toBeTruthy();
    expect(result.data.includes(MockData.mockArtists[1])).toBeTruthy();
  });

  test('state should contain error data after dispatching request fail action', () => {
    const result = mockCollectionReducer(initialState, {
      type: REQUEST_ + MOCKS + _FAIL,
      message: "Mock Request Error",
      statusCode: 999,
    });

    expect(result.isFetching).toBe(false);
    expect(result.errorStatusCode).toBe(999);
    expect(result.errorMessage).toBe("Mock Request Error");
  });

  test('state should be reset after dispatching clear action', () => {
    const result = mockCollectionReducer({...initialState,
      selected: MockData.mockArtists[0],
    }, clearMocks());

    expect(result.data).toEqual([]);
    expect(result.nextPage).toBe(1);
    expect(result.hasMore).toBe(false);
  });
})