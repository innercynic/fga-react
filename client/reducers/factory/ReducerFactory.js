import { createHandlersForResource, createInitialResourceState } from './resource';
import { createHandlersForCollection, createInitialCollectionState } from './collection';

export const createReducerForResource = ({ type, extendedState = {}, extendedHandlers = {} }) => {
  const initialState = createInitialResourceState(extendedState);
  const handlers = createHandlersForResource(type, extendedHandlers);

  return (state = initialState, action) => (
    handlers[action.type] ? handlers[action.type](state, action) : state
  );
};

export const createReducerForCollection = ({ type, extendedState = {}, extendedHandlers = {} }) => {
  const initialState = createInitialCollectionState(extendedState);
  const handlers = createHandlersForCollection(type, extendedHandlers);

  return (state = initialState, action) => (
    handlers[action.type] ? handlers[action.type](state, action) : state
  );
};

