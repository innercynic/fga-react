import songs from './songs/songs';
import song from './songs/song';

import artist from './artists/artist';
import artists from './artists/artists';

import setlist from './setlists/setlist';
import setlists from './setlists/setlists';

import auth from './user/auth';
import settings from './user/settings';
import user from './user/user';

const applicationReducers = {
  songs,
  song,
  artists,
  artist,
  setlist,
  setlists,
  auth,
  settings,
  user,
};

export default applicationReducers;
