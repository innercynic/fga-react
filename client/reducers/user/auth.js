import {
  OPEN_LOGIN,
  CLOSE_LOGIN,
  CLEAR_LOGIN_ERROR,
  LOGIN,
  LOGOUT,
} from '../../actions/user/auth';

import { _SUCCESS, _FAIL } from '../../actions/ActionConstants';

const auth = (state = {
  isLoggingIn: false,
  isLoginOpen: false,
  isLoggedIn: !!localStorage.auth_token,
  user: localStorage.user ? JSON.parse(localStorage.user) : null,
  token: localStorage.auth_token ? localStorage.auth_token : null,
  errorMessage: null,
  errorStatusCode: null,
}, action) => {
  switch (action.type) {
    case OPEN_LOGIN:
      return {
        ...state,
        isLoginOpen: true,
      };

    case CLOSE_LOGIN:
      return {
        ...state,
        isLoginOpen: false,
      };

    case CLEAR_LOGIN_ERROR:
      return {
        ...state,
        errorMessage: null,
        errorStatusCode: null,
      };

    case LOGIN:
      return {
        ...state,
        isLoggingIn: true,
      };

    case LOGIN + _SUCCESS:
      localStorage.setItem('auth_token', action.response.token);
      localStorage.setItem('user', JSON.stringify(action.response.user));

      return {
        ...state,
        token: action.response.token,
        user: action.response.user,
        isLoggingIn: false,
        isLoggedIn: true,
        isLoginOpen: false,
      };

    case LOGIN + _FAIL:
      return {
        ...state,
        errorMessage: action.message,
        errorStatusCode: action.statusCode,
        isLoggingIn: false,
      };

    case LOGOUT:
      localStorage.removeItem('auth_token');
      localStorage.removeItem('user');

      return {
        ...state,
        isLoggedIn: false,
        user: null,
        token: null,
      };

    default:
      return state;
  }
};

export default auth;
