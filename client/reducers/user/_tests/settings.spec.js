import sinon from 'sinon';

import settingsReducer from '../settings';
import { switchTheme, openSettings, closeSettings } from '../../../actions/user/settings';
import MockData from '../../../utils/test/MockData';

const themes = MockData.mockThemes;

const initialState = {
  currentTheme: 'mocktheme1',
  isSettingsOpen: false,
  themes,
};

test('state should contained changed theme after calling switchTheme', () => {
  const result = settingsReducer(initialState, switchTheme('mocktheme2'));

  expect(result.currentTheme).toBe("mocktheme2");
});

test('state should have isSettingOpen set to true after calling openSettings', () => {
  const result = settingsReducer(initialState, openSettings());

  expect(result.isSettingsOpen).toBe(true);
});

test('tate should have isSettingOpen set to false after calling closeSettings', () => {
  const result = settingsReducer({ ...initialState, isSettingsOpen: true }, closeSettings());

  expect(result.isSettingsOpen).toBe(false);
});
