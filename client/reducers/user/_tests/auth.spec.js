import sinon from 'sinon';

import authReducer from '../auth';
import MockData from '../../../utils/test/MockData';
import { _SUCCESS, _FAIL } from '../../../actions/ActionConstants';
import {
  login,
  logout,
  clearLoginError,
  openLogin,
  closeLogin,
  LOGIN,
} from '../../../actions/user/auth';

let stubbedLocalStorage = {};

beforeEach(() => {
  stubbedLocalStorage = {};
});

sinon.stub(localStorage, 'setItem').callsFake((itemName, itemValue) => {
  stubbedLocalStorage[itemName] = itemValue;
});

sinon.stub(localStorage, 'removeItem').callsFake((itemName) => {
  delete stubbedLocalStorage[itemName];
});


const initialState = {
  isLoggingIn: false,
  isLoginOpen: false,
  isLoggedIn: !!localStorage.auth_token,
  user: stubbedLocalStorage.user ? JSON.parse(stubbedLocalStorage.user) : null,
  token: stubbedLocalStorage.auth_token ? stubbedLocalStorage.auth_token : null,
  errorMessage: null,
  errorStatusCode: null,
};

test('should set isLoginOpen to true when dispatching openLogin action', () => {
  const result = authReducer(initialState, openLogin());

  expect(result.isLoginOpen).toBeTruthy();
});

test('should set isLoginOpen to false when dispatching closeLogin action', () => {
  const result = authReducer(initialState, closeLogin());

  expect(result.isLoginOpen).toBeFalsy();
});

test('should clear error data when dispatching clearLoginError action', () => {
  const result = authReducer({
    ...initialState,
    errorMessage: 'Mock error message',
    errorStatusCode: 999,
  }, clearLoginError());

  expect(result.errorMessage).toBeFalsy();
  expect(result.errorStatusCode).toBeFalsy();
});

test('state should have isLoggingIn set to true when calling login', () => {
  const result = authReducer(initialState, login({ email: "bob@bob.com", password: "password" }));

  expect(result.isLoggingIn).toBeTruthy();
});

test('state and localStorage should contain token and user data after dispatching login success action', () => {
  let mockToken = "kjlqk34598(*Olseasdf9(*FS0j3asd3wr#RFW@WFf|3n"
  const result = authReducer({ ...initialState, isLoggingIn: true }, {
    type: LOGIN + _SUCCESS,
    response: {
      token: mockToken,
      user: MockData.mockUsers[0],
    }
  });

  expect(result.isLoggingIn).toBeFalsy();
  expect(result.token).toBe(mockToken);
  expect(result.user).toBe(MockData.mockUsers[0]);

  expect(stubbedLocalStorage.auth_token).toEqual(mockToken);
  expect(stubbedLocalStorage.user).toEqual(JSON.stringify(MockData.mockUsers[0]));

});

test('state should have isLoggingIn set to false and have error when dispatching login failed action', () => {
  const result = authReducer({ ...initialState, isLoggingIn: true }, {
    type: LOGIN + _FAIL,
    message: "Mock login error",
    statusCode: 999,
  });

  expect(result.isLoggingIn).toBeFalsy();
  expect(result.errorMessage).toBe("Mock login error");
  expect(result.errorStatusCode).toBe(999);
});

test('state and localStorage should not contain token and user data after dispatching logout action', () => {
  let mockToken = "kjlqk34598(*Olseasdf9(*FS0j3asd3wr#RFW@WFf|3n";

  stubbedLocalStorage.auth_token = mockToken;
  stubbedLocalStorage.user = JSON.stringify(MockData.mockUsers[0]);

  const result = authReducer({
    ...initialState, 
    token: mockToken,
    user: MockData.mockUsers[0],
    isLoggedIn: true,
  }, logout());

  expect(result.isLoggedIn).toBeFalsy();
  expect(result.token).toBeFalsy();
  expect(result.user).toBeFalsy();

  expect(stubbedLocalStorage.auth_token).toBeFalsy();
  expect(stubbedLocalStorage.user).toBeFalsy();
});