import Modal from 'react-modal';

import {
  OPEN_SETTINGS,
  CLOSE_SETTINGS,
  SWITCH_THEME,
} from '../../actions/user/settings';
import themes from '../../themes';

const settings = (state = {
  currentTheme: 'darkfuture',
  isSettingsOpen: false,
  themes,
}, action) => {
  switch (action.type) {
    case SWITCH_THEME:
      return {
        ...state,
        currentTheme: action.payload,
      };

    case OPEN_SETTINGS:
      return {
        ...state,
        isSettingsOpen: true,
      };

    case CLOSE_SETTINGS:
      return {
        ...state,
        isSettingsOpen: false,
      };

    default:
      return state;
  }
};

export default settings;
