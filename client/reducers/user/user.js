import { createReducerForResource } from '../factory/ReducerFactory';
import { USER } from '../../actions/ActionConstants';

export default createReducerForResource({ type: USER });
