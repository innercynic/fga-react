import { createReducerForCollection } from '../factory/ReducerFactory';
import { ARTISTS } from '../../actions/ActionConstants';

export default createReducerForCollection({ type: ARTISTS });
