import artistReducer from '../artist';
import { IS_ADDING_ARTIST } from '../../../actions/artists/artist';

test('toggles is adding artist state', () => {
  let state = artistReducer(undefined, { type: IS_ADDING_ARTIST, payload: true });

  expect(state.isAddingArtist).toBeTruthy();
});
