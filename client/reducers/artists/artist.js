import { createReducerForResource } from '../factory/ReducerFactory';
import { ARTIST } from '../../actions/ActionConstants';
import { IS_ADDING_ARTIST } from '../../actions/artists/artist';

const extendedState = {
  isAddingArtist: false,
};

const extendedHandlers = {};

extendedHandlers[IS_ADDING_ARTIST] = (state, action) => ({
  ...state,
  isAddingArtist: action.payload,
});

export default createReducerForResource({
  type: ARTIST,
  extendedState,
  extendedHandlers,
});
