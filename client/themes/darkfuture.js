const theme = {

  themename: 'Dark Future',

  menu: {
    icons: {
      songdatabase: '',
      myspace: '',
      songsorter: '',
    },
  },

  backgrounds: {
    pageBackground: '/assets/images/dark1.jpg',
    mainOverlay: 'rgba(0, 0, 0, 0.45)',
    modalOverlay: 'rgba(0, 0, 0, 0.85)',
    container: 'rgba(0, 0, 0, 0.85)',
    container_alternate: 'rgba(17, 24, 40, 0.84)',
    input: {
      normal: 'rgba(26, 37, 60, 0.65)',
      hover: 'rgba(12, 43, 107, 0.7)',
      focus: 'rgba(12, 43, 107, 0.7)',
      readonly: 'rgba(17, 24, 40, 0.51)',
    },
  },

  svg: {
    guitarhead: {
      fill: 'rgba(26, 37, 60, 0.65)',
      stroke: 'rgba(255, 255, 255, 0.5);',
      strokeWidth: '11px',
    },
  },

  borders: {
    radiuses: {
      small: '5px',
      normal: '10px',
    },
    colors: {
      light: '#323e5c',
      normal: '#5e5ec0',
      dark: '#1c5266',
      shadow: '#1c5266',
    },
    sizes: {
      fine: '1px',
      medium: '1.5px',
      thick: '2px',
    },
  },

  fonts: {

    family: {
      title: 'aileronsregular',
      heading: "'Orbitron', sans-serif",
      subheading: "'Orbitron', sans-serif",
      text: "'OrkneyRegular'",
    },

    colors: {
      title: 'rgba(102, 159, 249, 1)',
      heading: '#74a4ff',
      subheading: 'rgba(148, 186, 255, 0.72)',
      text: 'rgba(148, 186, 255, 0.72)',
      error: 'rgba(255, 255, 255, 0.78)',
      input: {
        normal: 'rgba(148, 186, 255, 0.72)',
        hover: 'rgba(250, 250, 250, 0.85)',
        focus: 'rgba(250, 250, 250, 0.85)',
        readonly: 'rgba(148, 186, 255, 0.72)',
      },
    },

    sizes: {
      title: '3rem',
      heading: '1.5rem',
      subheading: '1.25rem',
      text: {
        tiny: '0.6rem',
        small: '0.8rem',
        normal: '1rem',
        large: '1.25rem',
        huge: '1.5rem',
      },
    },

  },

  webkit: {
    autoFillBoxShadow: '0 0 0 1000px rgba(12, 43, 107, 0.7) inset !important',
  },

};

export default theme;
