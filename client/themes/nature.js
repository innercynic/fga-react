const theme = {

  themename: 'Nature',

  menu: {
    icons: {
      songdatabase: '',
      myspace: '',
      songsorter: '',
    },
  },

  backgrounds: {
    pageBackground: '/assets/images/bg.jpg',
    mainOverlay: 'rgba(0, 0, 0, 0.45)',
    modalOverlay: 'rgba(0, 0, 0, 0.85)',
    container: 'rgba(0, 0, 0, 0.85)',
    container_alternate: 'rgba(42, 60, 26, 0.80)',
    input: {
      normal: 'rgba(42, 60, 26, 0.75)',
      hover: 'rgba(56, 113, 25, 0.76)',
      focus: 'rgba(56, 113, 25, 0.76)',
      readonly: 'rgba(18, 33, 12, 0.80)',
    },
  },

  svg: {
    guitarhead: {
      fill: 'rgba(26, 49, 14, 0.76)',
      stroke: 'rgba(75, 158, 34, 0.37);',
      strokeWidth: '11px',
    },
  },

  borders: {
    radiuses: {
      small: '5px',
      normal: '10px',
    },
    colors: {
      light: '#4ed116',
      normal: '#3d9a17',
      dark: '#286a0c',
      shadow: '#286a0c',
    },
    sizes: {
      fine: '1px',
      medium: '1.5px',
      thick: '2px',
    },
  },

  fonts: {

    family: {
      title: "'Patrick Hand SC', cursive",
      heading: "'Patrick Hand SC', cursive",
      subheading: "'Lato', sans-serif",
      text: "'Lato', sans-serif",
    },

    colors: {
      title: 'rgba(119, 232, 51, 1)',
      heading: 'rgba(119, 232, 51, 0.85)',
      subheading: 'rgba(81, 148, 40, 0.78)',
      text: 'rgba(81, 148, 40, 0.78)',
      error: 'rgba(255, 255, 255, 0.78)',
      input: {
        normal: 'rgba(119, 232, 51, 0.85)',
        hover: 'rgba(250, 250, 250, 0.85)',
        focus: 'rgba(250, 250, 250, 0.85)',
        readonly: 'rgba(119, 232, 51, 0.85)',
      },
    },

    sizes: {
      title: '3rem',
      heading: '1.5rem',
      subheading: '1.25rem',
      text: {
        tiny: '0.6rem',
        small: '0.8rem',
        normal: '1rem',
        large: '1.25rem',
        huge: '1.5rem',
      },
    },

  },

  webkit: {
    autoFillBoxShadow: '0 0 0 1000px rgba(12, 43, 107, 0.7) inset !important',
  },

};

export default theme;
