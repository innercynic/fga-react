import darkfuture from './darkfuture';
import nature from './nature';
import glass from './glass';

import { sizes, BASE_SIZE } from '../styles/media';

export function processThemes(themes) {
  const processedThemes = { ...themes };
  const { phone, tabletPortrait, tabletLandscape, desktop, hd } = sizes;
  const breakpoints = [phone, tabletPortrait, tabletLandscape, desktop, hd];

  const breakpointsInEms = breakpoints.map(breakpoint => breakpoint / BASE_SIZE);

  // Assign breakpoints and also ids to themes for rendering list keys in react
  let themeID = 0;

  Object.keys(processedThemes).forEach((key) => {
    processedThemes[key].breakpoints = breakpointsInEms;
    processedThemes[key].space = [0, 8, 16, 24, 32];
    processedThemes[key].themeid = themeID;
    themeID += 1;
  });

  return processedThemes;
}

const appThemes = {
  glass,
  darkfuture,
  nature,
};

export function getThemes() {
  return processThemes(appThemes);
}

export default processThemes(appThemes);
