import { generateTheme } from '../themegenerator';

const mockThemeVariables = {
  THEME_NAME: 'Mock',

  BG_IMAGE: '/assets/images/mock.jpg',
  BG_OVERLAY_COLOR: 'transparent',

  COLOR_BG_PRIMARY: 'blue',
  COLOR_BG_SECONDARY: 'red',
  COLOR_BG_ACTIVE: 'brown',

  COLOR_FONT_PRIMARY: 'blue',
  COLOR_FONT_SECONDARY: 'red',
  COLOR_FONT_ERROR: 'green',
  COLOR_FONT_TITLE: 'pink',

  COLOR_BORDER: 'transparent',

  FONT_FAMILY_TITLE: 'mockTitleFontName',
  FONT_FAMILY_HEADING: 'mockHeadingFontName',
  FONT_FAMILY_TEXT: "'mockTextFontName'",

  FONT_SIZE_TITLE: '3rem',
  FONT_SIZE_HEADING: '1.5rem',
  FONT_SIZE_TEXT: '1rem',

  BORDER_SIZE_BASE: '1.5px',

  BORDER_RADIUS_BASE: '10px',

  SVG_FILL: 'yellow',
  SVG_STROKE: 'transparent',
};

test('it generates theme from variables', () => {
  const theme = generateTheme(mockThemeVariables);

  expect(theme).toMatchSnapshot();
})