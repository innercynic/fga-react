import { generateTheme } from './themegenerator';

const variables = {
  THEME_NAME: 'Glass',

  BG_IMAGE: '/assets/images/base.jpg',
  BG_OVERLAY_COLOR: 'transparent',
  BG_MODAL_OVERLAY_COLOR: 'rgba(255, 255, 255, 0.50)',

  COLOR_BG_PRIMARY: 'rgba(255, 255, 255, 0.25)',
  COLOR_BG_SECONDARY: 'rgba(255, 255, 255, 0.50)',
  COLOR_BG_ACTIVE: 'rgba(255, 255, 255, 0.75)',

  COLOR_FONT_PRIMARY: '#6084a4',
  COLOR_FONT_SECONDARY: '#6188a9',
  COLOR_FONT_ERROR: '#4d51ef',
  COLOR_FONT_TITLE: 'rgba(255, 255, 255, 0.78)',

  COLOR_BORDER: 'rgba(255, 255, 255, 0)',

  FONT_FAMILY_TITLE: 'aileronsregular',
  FONT_FAMILY_HEADING: "'Orbitron', sans-serif",
  FONT_FAMILY_TEXT: "'OrkneyRegular'",

  FONT_SIZE_TITLE: '3rem',
  FONT_SIZE_HEADING: '1.5rem',
  FONT_SIZE_TEXT: '1rem',

  BORDER_SIZE_BASE: '1.5px',

  BORDER_RADIUS_BASE: '10px',

  SVG_FILL: 'rgba(255, 255, 255, 0.50)',
  SVG_STROKE: 'transparent',
};

export default generateTheme(variables);
