import Color from 'color';

import { increaseSizeByPercent, decreaseSizeByPercent } from '../utils/css/CSSUtils';

export const generateTheme = variables => ({
  themename: variables.THEME_NAME,

  backgrounds: {
    pageBackground: variables.BG_IMAGE,
    mainOverlay: variables.BG_OVERLAY_COLOR,
    modalOverlay: variables.BG_MODAL_OVERLAY_COLOR,
    container: variables.COLOR_BG_PRIMARY,
    container_alternate: variables.COLOR_BG_SECONDARY,
    input: {
      normal: variables.COLOR_BG_SECONDARY,
      hover: variables.COLOR_BG_ACTIVE,
      focus: variables.COLOR_BG_ACTIVE,
      readonly: variables.COLOR_BG_ACTIVE,
    },
  },

  svg: {
    guitarhead: {
      fill: variables.SVG_FILL,
      stroke: variables.SVG_STROKE,
    },
  },

  borders: {
    radiuses: {
      small: decreaseSizeByPercent(variables.BORDER_RADIUS_BASE, 0.5),
      normal: variables.BORDER_RADIUS_BASE,
    },
    colors: {
      light: Color(variables.COLOR_BORDER).lighten(0.1).string(),
      normal: variables.COLOR_BORDER,
      dark: Color(variables.COLOR_BORDER).darken(0.1).string(),
      shadow: variables.COLOR_BG_SECONDARY,
    },
    sizes: {
      fine: decreaseSizeByPercent(variables.BORDER_SIZE_BASE, 0.33),
      medium: variables.BORDER_SIZE_BASE,
      thick: increaseSizeByPercent(variables.BORDER_SIZE_BASE, 0.33),
    },
  },

  fonts: {

    family: {
      title: variables.FONT_FAMILY_TITLE,
      heading: variables.FONT_FAMILY_HEADING,
      subheading: variables.FONT_FAMILY_HEADING,
      text: variables.FONT_FAMILY_TEXT,
    },

    colors: {
      title: variables.COLOR_FONT_TITLE,
      heading: variables.COLOR_FONT_PRIMARY,
      subheading: variables.COLOR_FONT_PRIMARY,
      text: variables.COLOR_FONT_PRIMARY,
      error: variables.COLOR_FONT_ERROR,
      input: {
        normal: variables.COLOR_FONT_SECONDARY,
        hover: variables.COLOR_FONT_SECONDARY,
        focus: variables.COLOR_FONT_SECONDARY,
        readonly: variables.COLOR_FONT_SECONDARY,
      },
    },

    sizes: {
      title: variables.FONT_SIZE_TITLE,
      heading: variables.FONT_SIZE_HEADING,
      subheading: decreaseSizeByPercent(variables.FONT_SIZE_HEADING, 0.17),
      text: {
        tiny: decreaseSizeByPercent(variables.FONT_SIZE_TEXT, 0.40),
        small: decreaseSizeByPercent(variables.FONT_SIZE_TEXT, 0.20),
        normal: variables.FONT_SIZE_TEXT,
        large: increaseSizeByPercent(variables.FONT_SIZE_TEXT, 0.25),
        huge: increaseSizeByPercent(variables.FONT_SIZE_TEXT, 0.50),
      },
    },

  },

  webkit: {
    autoFillBoxShadow: '0 0 0 1000px transparent inset !important',
  },

});
