import React from 'react'
import 'jest-styled-components'

import { SearchForm } from '../SearchForm';

let props;

beforeEach(() => {
  props = {
    handleSubmit: jest.fn(),
  }
});

test('it renders', () => {

  const searchForm = shallow(
    <SearchForm {...props} />
  );

  expect(searchForm).toMatchSnapshot();
});

test('it calls prop when form is submitted', () => {
  const searchForm = shallow(
    <SearchForm {...props} />
  );

  searchForm.find('form').props().onSubmit();

  expect(props.handleSubmit).toHaveBeenCalled();
});