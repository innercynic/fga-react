import React from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';

import { RFFieldWithIcon } from '../form/reduxformfields';

export const SearchForm = ({ handleSubmit }) => (
  <form onSubmit={handleSubmit}>
    <RFFieldWithIcon name="searchValue" icon="search" />
  </form>
);

SearchForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'search' })(SearchForm);
