import styled from 'styled-components';

const PageBackground = styled.div`
  height: 100%;
  width: 100%;

  background: linear-gradient(
    ${props => props.theme.backgrounds.mainOverlay}, 
    ${props => props.theme.backgrounds.mainOverlay}
  ), url('${props => props.theme.backgrounds.pageBackground}');

  background-size: cover;
  background-attachment: fixed;
  background-position: center;
`;

export default PageBackground;
