import React from 'react';
import PageBackground from '../PageBackground';

test('should shallow render', () => {
  let themeData = { backgrounds: { pageBackground: "/bg.jpg" } };

  const pageBackground = mount(
      <PageBackground theme={themeData} />,
  );
  expect(pageBackground).toMatchSnapshot();
});