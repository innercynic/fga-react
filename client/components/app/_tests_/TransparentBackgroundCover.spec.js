import React from 'react';
import TransparentBackgroundCover from '../TransparentBackgroundCover';

test('should shallow render', () => {
  let themeData = { backgrounds: { mainOverlay: "rgba(0, 0, 0, 0.45)" } };

  const transparentBackgroundCover = mount(
      <TransparentBackgroundCover theme={themeData} />,
  );
  expect(transparentBackgroundCover).toMatchSnapshot();
});