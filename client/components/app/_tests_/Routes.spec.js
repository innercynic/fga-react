import React from 'react';

import Routes from '../Routes';

test('it renders', () => {
  const routes = shallow(
    <Routes />,
  );

  expect(routes).toMatchSnapshot();
});
