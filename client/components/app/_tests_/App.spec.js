import React from 'react';
import renderer from 'react-test-renderer';
import Shallow from 'react-test-renderer/shallow';
import { App } from '../App';

import MockData from '../../../utils/test/MockData';

let props = {
  themes: MockData.mockThemes,
  currentTheme: MockData.mockThemeName,
};

test('renders', () => {
  const app = shallow(
    <App {...props} />,
  );

  expect(app).toMatchSnapshot();
});
