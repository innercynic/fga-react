import React from 'react';
import PageContent from '../PageContent';

test('should shallow render', () => {
  const pageContent = mount(
      <PageContent />,
  );
  expect(pageContent).toMatchSnapshot();
});