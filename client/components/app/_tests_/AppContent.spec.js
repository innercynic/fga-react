import React from 'react';
import AppContent from '../AppContent';

test('it renders', () => {
  const appContent = shallow(
    <AppContent  />,
  );

  expect(appContent).toMatchSnapshot();
});
