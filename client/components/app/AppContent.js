import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

import PageContent from './PageContent';
import Routes from './Routes';
import Header from '../header/Header';

const AppContent = () => (
  <PageContent column align="center">
    <Header />

    <Scrollbars id="scroller">
      <Routes />
    </Scrollbars>

  </PageContent>
);

export default AppContent;
