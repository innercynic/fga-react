import { Flex } from 'grid-styled';

const PageContent = Flex.extend `
  width: 100%;
  height: 100%;
  z-index: 2;
`;

export default PageContent;
