import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Menu from '../menu/main/Menu';

import SongSearchMenu from '../song/search/searchmenu/SongSearchMenu';
import SongNameSearchContainer from '../song/search/namesearch/SongNameSearchContainer';
import SongTuningSearchContainer from '../song/search/tuningsearch/SongTuningSearchContainer';
import SongViewContainer from '../song/view/SongViewContainer';

import ChooseSongArtist from '../song/create/ChooseSongArtist';
import CreateSongContainer from '../song/create/CreateSongContainer';

import SignUpContainer from '../user/signup/SignUpContainer';

import CreateSetListContainer from '../setlist/create/CreateSetListContainer';
import SetlistTuningInstructionsContainer from '../setlist/tunings/SetlistTuningInstructionsContainer';
import SetlistList from '../setlist/list/SetlistList';

import { Container } from '../general';
import { requireAuthentication } from '../auth/RouteAuthenticationHOC';

const Routes = () => (
  <Container noBackground justify="center" p={[2, 2, 2, 3]}>
    <Route exact path="/" component={Menu} />


    <Route exact path="/search" component={SongSearchMenu} />
    <Route exact path="/search/name" component={SongNameSearchContainer} />
    <Route path="/search/name/:searchValue" component={SongNameSearchContainer} />
    <Route exact path="/search/tuning" component={SongTuningSearchContainer} />
    <Route path="/search/tuning/:tuning/:similaritylevel" component={SongTuningSearchContainer} />

    <Route exact path="/addsong" component={requireAuthentication(ChooseSongArtist)} />
    <Route path="/addsong/:id" component={requireAuthentication(CreateSongContainer)} />

    <Route path="/songs/:id" component={SongViewContainer} />

    <Route exact path="/setlists" component={requireAuthentication(SetlistList)} />

    <Switch>
      <Route exact path="/setlists/new" component={requireAuthentication(CreateSetListContainer)} />
      <Route exact path="/setlists/:id/tuninginstructions" component={requireAuthentication(SetlistTuningInstructionsContainer)} />
      <Route exact path="/setlists/:id" component={requireAuthentication(CreateSetListContainer)} />
    </Switch>

    <Route path="/signup" component={SignUpContainer} />
  </Container>
);

export default Routes;
