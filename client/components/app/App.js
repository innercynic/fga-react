import React from 'react';
import { ThemeProvider } from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import PageBackground from './PageBackground';

import AppContent from './AppContent';

export const App = ({ themes, currentTheme }) => (
  <ThemeProvider theme={themes[currentTheme]}>
    <PageBackground>
      <AppContent />
    </PageBackground>
  </ThemeProvider>
);

function mapStateToProps(state) {
  return {
    currentTheme: state.settings.currentTheme,
    themes: state.settings.themes,
  };
}

App.propTypes = {
  themes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  currentTheme: PropTypes.string.isRequired,
};

export default connect(mapStateToProps)(App);
