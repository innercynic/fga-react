import styled from 'styled-components';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

const OverlayModal = styled(Modal)`
  background: ${props => props.theme.backgrounds.mainOverlay};
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 90%;
  outline: none;
  max-width: ${props => (props.maxWidth ? `${props.maxWidth}px` : 'none')};
  border: ${props => `${props.theme.borders.sizes.medium} solid ${props.theme.borders.colors.normal}`};
  border-radius: ${props => props.theme.borders.radiuses.normal}; 
  color: ${props => props.theme.fonts.colors.text};
`;

OverlayModal.defaultProps = {
  maxWidth: null,
  contentLabel: '',
};

OverlayModal.propTypes = {
  maxWidth: PropTypes.number,
  contentLabel: PropTypes.string,
};

export default OverlayModal;
