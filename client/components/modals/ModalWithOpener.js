import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import OverlayModal from '../modals/OverlayModal';
import { Text, Icon } from '../general';

const ModalWithOpener = ({
  isOpen,
  open,
  close,
  title,
  icon,
  openerSize,
  modalMaxWidth,
  showLabel,
  children,
}) => (
  <Box>
    <OverlayModal
      isOpen={isOpen}
      contentLabel={title}
      onRequestClose={close}
      maxWidth={modalMaxWidth || null}
    >
      {children}
    </OverlayModal>

    <Flex id="modal-opener" onClick={open}>
      <Icon clickable size={openerSize} icon={icon} />
      {showLabel &&
        <Text size={openerSize}>{ title }</Text>
      }
    </Flex>
  </Box>
);

ModalWithOpener.defaultProps = {
  isOpen: false,
  showLabel: false,
  openerSize: 'normal',
};

ModalWithOpener.propTypes = {
  isOpen: PropTypes.bool,
  open: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  showLabel: PropTypes.bool,
  icon: PropTypes.string.isRequired,
  openerSize: PropTypes.string,
};

export default ModalWithOpener;
