import React from 'react';
import 'jest-styled-components'

import OverlayModal from '../OverlayModal';
import MockData from '../../../utils/test/MockData';

let props;

beforeEach(() => {
  props = {
    theme: MockData.mockThemes[MockData.mockThemeName],
  };
})

test('it renders', () => {
  const overlayModal = shallow(
    <OverlayModal {...props} />,
  );

  expect(overlayModal).toMatchSnapshot();
});

test('it configures max width', () => {
  const overlayModal = shallow(
    <OverlayModal {...props} maxWidth={400} />,
  );

  expect(overlayModal).toHaveStyleRule('max-width', '400px');
});