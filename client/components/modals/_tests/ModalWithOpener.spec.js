import React from 'react';

import ModalWithOpener from '../ModalWithOpener';
import { Text } from '../../general';

let props;

beforeEach(() => {
  props = {
    open: jest.fn(),
    close: jest.fn(),
    title: '',
    icon: "mockIconName",
  };
})

test('it renders', () => {
  const modalWithOpener = shallow(
    <ModalWithOpener {...props} />,
  );

  expect(modalWithOpener).toMatchSnapshot();
});

test('it displays title when showLabel is true', () => {
  const modalWithOpener = shallow(
    <ModalWithOpener {...props} showLabel />,
  );

  expect(modalWithOpener.find(Text)).toHaveLength(1);
});

test('it displays title when showLabel is true', () => {
  const modalWithOpener = shallow(
    <ModalWithOpener {...props} showLabel />,
  );

  modalWithOpener.find('#modal-opener').simulate('click');

  expect(props.open).toHaveBeenCalled();
});

