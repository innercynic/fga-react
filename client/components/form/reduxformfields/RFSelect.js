import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import { Input } from '../fields';

const InputField = Input.withComponent(Field);

export const RFSelect = props => (
  <InputField name={props.name} component="select">
    {props.children}
  </InputField>
);

RFSelect.propTypes = {
  name: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};
