
import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import { ImageDropzone } from '../fields';

export const RFImageDropzone = props => (
  <Field
    imageValue={props.imageValue}
    name={props.name}
    multiple={false}
    component={ImageDropzone}
  />
);

RFImageDropzone.defaultProps = {
  imageValue: { preview: '' },
};

RFImageDropzone.propTypes = {
  name: PropTypes.string.isRequired,
  imageValue: PropTypes.shape({
    preview: PropTypes.string,
  }),
};
