import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Field } from 'redux-form';

import { Input, TextArea } from '../fields';
import { FlexExtended, Icon } from '../../general';

const ExtendedTextArea = TextArea.withComponent(Field).extend`
  padding-left: 45px;
`;

const ExtendedInput = Input.withComponent(Field).extend`
  padding-left: 45px;
`;

const FieldIcon = styled(Icon)`
  position: absolute;
  margin-left: 17px;
  top: calc(50%);
  transform: translateY(-50%);
  z-index: 1;
`;

export const RFFieldWithIcon = props => (
  <FlexExtended position="relative" flex={1}>
    <FieldIcon icon={props.icon} />
    {props.isTextArea &&
    <ExtendedTextArea component="textarea" type={props.type} name={props.name} />
    }
    {!props.isTextArea &&
    <ExtendedInput component="input" type={props.type} name={props.name} />
    }
  </FlexExtended>
);

RFFieldWithIcon.defaultProps = {
  isTextArea: false,
  type: 'text',
};

RFFieldWithIcon.propTypes = {
  icon: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  isTextArea: PropTypes.bool,
};
