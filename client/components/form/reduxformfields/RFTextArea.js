import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import { TextArea } from '../fields';

const TextAreaField = TextArea.withComponent(Field);

export const RFTextArea = props => (
  <TextAreaField name={props.name} component="textarea" type="text" />
);

RFTextArea.propTypes = {
  name: PropTypes.string.isRequired,
};
