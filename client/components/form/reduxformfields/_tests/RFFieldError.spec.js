import React from 'react';

import { RFFieldError } from '../RFFieldError';

let props;

beforeEach(() => {
  props = {
    name: "MockFieldName",
  };
})

test('it renders', () => {
  const rfFieldError = shallow(
    <RFFieldError {...props} />,
  );

  expect(rfFieldError).toMatchSnapshot();
});
