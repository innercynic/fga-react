import React from 'react';

import { RFFieldWithIcon } from '../RFFieldWithIcon';

let props;

beforeEach(() => {
  props = {
    name: "MockFieldName",
    icon: "mockIconName"
  };
})

test('it renders', () => {
  const rfFieldWithIcon = shallow(
    <RFFieldWithIcon {...props} />,
  );

  expect(rfFieldWithIcon).toMatchSnapshot();
});

test('it renders as text area', () => {
  const rfFieldWithIcon = shallow(
    <RFFieldWithIcon {...props} isTextArea />,
  );

  expect(rfFieldWithIcon).toMatchSnapshot();
});