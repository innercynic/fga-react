import React from 'react';

import { RFSelect } from '../RFSelect';

let props;

beforeEach(() => {
  props = {
    name: "mockFieldName",
  };
})

test('it renders', () => {
  const rfSelect = shallow(
    <RFSelect {...props}>
      <option>Mock Option</option>
    </RFSelect>,
  );

  expect(rfSelect).toMatchSnapshot();
});
