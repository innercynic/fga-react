import React from 'react';

import { Label } from '../../fields';
import { RFCheckbox } from '../RFCheckbox';

let props;

beforeEach(() => {
  props = {
    name: "mockFieldName",
  };
})

test('it renders', () => {
  const rfCheckbox = shallow(
    <RFCheckbox {...props} />,
  );

  expect(rfCheckbox).toMatchSnapshot();
});

test('it renders a label', () => {
  const rfCheckbox = shallow(
    <RFCheckbox {...props} label="MockLabel" />,
  );

  expect(rfCheckbox).toMatchSnapshot();
});

