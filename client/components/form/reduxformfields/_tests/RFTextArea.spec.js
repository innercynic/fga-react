import React from 'react';

import { RFTextArea } from '../RFTextArea';

let props;

beforeEach(() => {
  props = {
    name: "mockFieldName",
  };
})

test('it renders', () => {
  const rfTextArea = shallow(
    <RFTextArea {...props} />,
  );

  expect(rfTextArea).toMatchSnapshot();
});
