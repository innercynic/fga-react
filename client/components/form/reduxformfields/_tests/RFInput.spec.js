import React from 'react';

import { RFInput } from '../RFInput';

let props;

beforeEach(() => {
  props = {
    name: "mockFieldName",
  };
})

test('it renders', () => {
  const rfInput = shallow(
    <RFInput {...props} />,
  );

  expect(rfInput).toMatchSnapshot();
});