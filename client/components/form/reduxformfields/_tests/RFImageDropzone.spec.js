import React from 'react';

import { RFImageDropzone } from '../RFImageDropzone';

let props;

beforeEach(() => {
  props = {
    name: "mockFieldName",
  };
})

test('it renders', () => {
  const rfImageDropzone = shallow(
    <RFImageDropzone {...props} />,
  );

  expect(rfImageDropzone).toMatchSnapshot();
});
