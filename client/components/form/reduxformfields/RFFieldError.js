import React from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';

import { ErrorText } from '../../general/index';

export const ReduxFormError = ({ meta: { touched, error } }) => (
  <span>
    {touched && (error && <ErrorText>{error}</ErrorText>)}
  </span>
);

ReduxFormError.defaultProps = {
  meta: {
    touched: false,
    error: '',
  },
};

ReduxFormError.propTypes = {
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
};

export const RFFieldError = ({ name }) => (
  <Field name={name} component={ReduxFormError} type="text" />
);

RFFieldError.propTypes = {
  name: PropTypes.string.isRequired,
};
