import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import { Input } from '../fields';

const InputField = Input.withComponent(Field);

export const RFInput = props => (
  <InputField name={props.name} type={props.type} component="input" />
);

RFInput.defaultProps = {
  type: 'text',
};

RFInput.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
};
