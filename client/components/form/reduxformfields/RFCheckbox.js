import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { Box } from 'grid-styled';

import { Checkbox, Label } from '../fields';

const CheckBoxField = Checkbox.withComponent(Field);

const CheckBoxLabel = Label.extend`
    padding-top: 3px;
`;

export const RFCheckbox = props => (
  <Box>
    <CheckBoxField id={`checkbox-${props.name}`} name={props.name} type="checkbox" component="input" />
    {props.label &&
      <CheckBoxLabel htmlFor={`checkbox-${props.name}`}>{props.label}</CheckBoxLabel>
    }
  </Box>
);

RFCheckbox.defaultProps = {
  label: '',
};

RFCheckbox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
};
