import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';

import { clearSelectedArtist } from '../../../actions/artists/artist';
import { requestArtists } from '../../../actions/artists/artists';
import { ArtistShape, InputShape } from '../../../proptypes';

import AutoCompleteContainer from './base/AutoCompleteContainer';

import { createUrlFromAction } from '../../../utils/api/ApiUtils';

export class ArtistAutoComplete extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchResults: [],
      isSearching: false,
    };

    this.searchArtists = this.searchArtists.bind(this);
    this.clearSearchResults = this.clearSearchResults.bind(this);
  }

  searchArtists(searchParams) {
    const requestArtistsUrl = createUrlFromAction(requestArtists(searchParams));
    this.setState({ isSearching: true });

    return axios.get(requestArtistsUrl).then((result) => {
      this.setState({
        searchResults: result.data.data,
        isSearching: false,
      });
    });
  }

  clearSearchResults() {
    this.setState({ searchResults: [] });
  }

  render() {
    return (
      <AutoCompleteContainer
        isSearching={this.state.isSearching}
        searchResults={this.state.searchResults}
        search={this.searchArtists}
        clearSearchResults={this.clearSearchResults}
        searchBy="name~"
        onSelectionMade={this.props.onSelectionMade}
        onNoSelectionMade={this.props.onNoSelectionMade}
        externalOnChange={this.props.externalOnChange}
        input={this.props.input}
        value={this.props.selectedArtist}
        clearValue={this.props.clearSelectedArtist}
      />
    );
  }
}

function mapStateToProps(state) {
  return { selectedArtist: state.artist.selected };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ clearSelectedArtist }, dispatch);
}

ArtistAutoComplete.defaultProps = {
  selectedArtist: null,
  onSelectionMade() {},
  onNoSelectionMade() {},
  externalOnChange() {},
};

ArtistAutoComplete.propTypes = {
  clearSelectedArtist: PropTypes.func.isRequired,
  onSelectionMade: PropTypes.func,
  onNoSelectionMade: PropTypes.func,
  externalOnChange: PropTypes.func,
  input: InputShape.isRequired,
  selectedArtist: ArtistShape,

};

export default connect(mapStateToProps, matchDispatchToProps)(ArtistAutoComplete);
