import { css } from 'styled-components';
import { Flex } from 'grid-styled';

export const OPTION_HEIGHT = 60;

const highlightOption = css`
  background-color: ${props => props.theme.backgrounds.input.hover};
  color: ${props => props.theme.fonts.colors.input.hover};
`;

export const AutoCompleteOption = Flex.extend`
  ${props => (props.isSelected ? highlightOption : '')}
  cursor: pointer;
  height: ${OPTION_HEIGHT}px;

  &:last-of-type{
    border-bottom-right-radius: ${props => props.theme.borders.radiuses.small}; 
    border-bottom-left-radius: ${props => props.theme.borders.radiuses.small};
  }

  &:hover {
    background-color: ${props => props.theme.backgrounds.input.hover};
    color: ${props => props.theme.fonts.colors.input.hover};
  }
`;
