import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { InputShape } from '../../../../proptypes';
import { AutoComplete } from './AutoComplete';
import { OPTION_HEIGHT } from './AutoCompleteOption';

class AutoCompleteContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.state.inputValue = '';
    this.state.isOpen = true;
    this.state.selectedItem = 0;

    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.setInputSelectedValue = this.setInputSelectedValue.bind(this);
    this.getSelectedItemIndex = this.getSelectedItemIndex.bind(this);
    this.setKeyPressListeners = this.setKeyPressListeners.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.setInputRef = this.setInputRef.bind(this);
    this.setScrollBarRef = this.setScrollBarRef.bind(this);
    this.search = this.search.bind(this);
  }

  componentWillMount() {
    if (this.props.value) {
      this.setInputSelectedValue(this.props.value);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value) {
      if (this.props.value !== nextProps.value) {
        this.setInputSelectedValue(nextProps.value);
      }
    }
  }

  /**
   * @param {DomElement} input  The dom element reference of the autocomplete input
   *    Needed for programatically blurring the input
   */
  setInputRef(input) {
    this.input = input;
  }

  /**
   * @param {DomElement} scrollbar  The dom element reference of the autocomplete options scrollbar
   *    Needed to programatically scroll options when user press up, down and tab keys.
   */
  setScrollBarRef(scrollbar) {
    this.scrollbar = scrollbar;
  }

  /**
   * @param item {object} The object to set as value for parent (redux form field)
   */
  setInputSelectedValue(item) {
    this.setState({ inputValue: item.name }, () => {
      this.props.input.onChange(item._id);
      this.setState({ isOpen: false });
      this.input.blur();
      this.props.onSelectionMade(item);
    });
  }

  /**
   * @param {bool} nextOption - If true, return next option, else return previous
   * @return {number} The index of the option to set as selected
   */
  getSelectedItemIndex(isGettingNextOption) {
    if (isGettingNextOption) {
      if (this.state.selectedItem < this.props.searchResults.length - 1) {
        return this.state.selectedItem + 1;
      }

      return 0;
    }
    if (this.state.selectedItem > 0) return this.state.selectedItem - 1;
    return this.props.searchResults.length - 1;
  }

  /**
   * @param {bool} enable - If true, create keypress listeners, else remove them
   */
  setKeyPressListeners(enable) {
    if (!enable) window.onkeydown = null;
    else {
      window.onkeydown = this.handleKeyDown;
    }
  }

  /**
   * @param {event} e - The keypress event
   */
  handleKeyDown(e) {
    const keyCode = e.keyCode ? e.keyCode : e.which;

    if (![9, 13, 38, 40].includes(keyCode)) return;

    let selectedItemIndex;
    e.preventDefault();

    switch (keyCode) {
      case 9: // Tab
        if (this.props.searchResults.length < 1) {
          this.props.onNoSelectionMade(this.state.inputValue);
          break;
        } // Otherwise fallthrough to case 40
      case 40: // Down
        if (this.props.searchResults.length > 1) {
          selectedItemIndex = this.getSelectedItemIndex(true);
          this.setState({ selectedItem: selectedItemIndex });
          this.scrollbar.scrollTop(OPTION_HEIGHT * selectedItemIndex);
        }
        break;
      case 38: // Up
        if (this.props.searchResults.length > 1) {
          selectedItemIndex = this.getSelectedItemIndex(false);
          this.setState({ selectedItem: selectedItemIndex });
          this.scrollbar.scrollTop(OPTION_HEIGHT * selectedItemIndex);
        }
        break;
      case 13: // Enter
        if (this.props.searchResults.length > 1) {
          if (this.props.searchResults[this.state.selectedItem]) {
            this.setInputSelectedValue(this.props.searchResults[this.state.selectedItem]);
          }
        }

        break;
      default:
    }
  }

  /**
   * @param  {object} event The change event object
   */
  handleChange({ target: { value } }) {
    this.setState({ inputValue: value });
    this.search(value);
    this.props.input.onChange(null);
    this.props.externalOnChange(value);
  }

  /**
   * @param  {object} event The blur event object
   */
  handleBlur(event) {
    this.setState({ isOpen: false });
    this.setKeyPressListeners(false);

    if (this.props.searchResults && this.props.searchResults.length === 1) {
      if (this.props.searchResults[0].name === event.target.value) {
        this.props.input.onChange(this.props.searchResults[0]._id);
      }
    // } else if (!this.props.searchResults || this.props.searchResults.length < 1) {
    } else {
      this.props.onNoSelectionMade(this.state.inputValue);
    }
  }

  handleFocus() {
    if (this.props.input.value) {
      this.props.clearSearchResults();
      this.search(this.state.inputValue);
    }

    this.setState({ isOpen: true });
    this.setKeyPressListeners(true);
  }

  /**
   * @param  {String} value - The value to search for
   */
  search(value) {
    this.setState({ isOpen: true });
    this.props.search({ query: this.props.searchBy + value });
  }

  render() {
    const { isSearching, searchResults } = this.props;
    const { isOpen, inputValue, selectedItem } = this.state;
    const {
      handleChange,
      handleBlur,
      handleFocus,
      setInputRef,
      setInputSelectedValue,
      setScrollBarRef,
    } = this;
    const propsToPass = {
      isSearching,
      searchResults,
      isOpen,
      inputValue,
      selectedItem,
      handleChange,
      handleBlur,
      handleFocus,
      setInputRef,
      setScrollBarRef,
      setInputSelectedValue,
    };

    return (
      <AutoComplete {...propsToPass} />
    );
  }
}

AutoCompleteContainer.defaultProps = {
  searchBy: 'name~',
  searchResults: [],
  value: null,
  onSelectionMade() {},
  onNoSelectionMade() {},
  externalOnChange() {},
};

AutoCompleteContainer.propTypes = {
  searchBy: PropTypes.string,
  searchResults: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    thumbnail: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  })),
  value: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),

  input: InputShape.isRequired,
  isSearching: PropTypes.bool.isRequired,
  onSelectionMade: PropTypes.func,
  onNoSelectionMade: PropTypes.func,
  externalOnChange: PropTypes.func,
  clearSearchResults: PropTypes.func.isRequired,
  search: PropTypes.func.isRequired,
};

export default AutoCompleteContainer;
