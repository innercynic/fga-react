import styled from 'styled-components';

export const AutoCompleteOptionIcon = styled.div`
  background: url("${props => (props.iconUrl ? props.iconUrl.split('\\').join('/') : '')}") center no-repeat;
  background-size: cover;
  width: 50px;
  height: 50px;
  border-radius: 50%;
}
`;
