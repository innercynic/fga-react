import { Flex } from 'grid-styled';
import Color from 'color';
import media from '../../../../styles/media';

export const AutoCompleteOptionsContainer = Flex.extend`
  background-color: ${props => Color(props.theme.backgrounds.input.normal).opaquer(0.5).string()};
  color: ${props => props.theme.fonts.colors.input.normal};
  position: absolute;
  top: 30px;

  ${media.tabletPortrait`
    top: 40px;
  `}  

  ${media.tabletLandscape`
    top: 50px;
  `}

  width: 100%;
  border-bottom-right-radius: ${props => props.theme.borders.radiuses.small}; 
  border-bottom-left-radius: ${props => props.theme.borders.radiuses.small};
  border: 1px solid ${props => props.theme.borders.colors.normal};
  border-top: 0;
`;
