import React from 'react'
import 'jest-styled-components'

import TestUtils from '../../../../../utils/test/TestUtils';
import { AutoCompleteOption } from '../AutoCompleteOption';

let ThemeProviderWrapper = TestUtils.getMockThemeProvider();

test('it renders without being highlighted', () => {
  const autoCompleteOption = mount(
    <ThemeProviderWrapper>
      <AutoCompleteOption />
    </ThemeProviderWrapper>,
  );

  expect(autoCompleteOption).toMatchSnapshot();
});

test('it renders with highlighted state', () => {
  const autoCompleteOption = mount(
    <ThemeProviderWrapper>
      <AutoCompleteOption isSelected />
    </ThemeProviderWrapper>,
  );

  expect(autoCompleteOption).toMatchSnapshot()
});