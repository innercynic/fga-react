import React from 'react';

import TestUtils from '../../../../../utils/test/TestUtils';
import MockData from '../../../../../utils/test/MockData';
import AutoCompleteContainer from '../AutoCompleteContainer';

describe('AutoCompleteContainer', () => {
  let ThemeProviderWrapper, props, mockInput;
  let mockSearchResults = MockData.mockArtists;

  beforeEach(() => {
    props = {
      input: {
        name: "mock",
        onBlur: jest.fn(),
        onChange: jest.fn(),
        onFocus: jest.fn(),
        onDragStart: jest.fn(),
        onDrop: jest.fn(),
        value: "",
      },
      isSearching: false,
      onNoSelectionMade: jest.fn(),
      externalOnChange: jest.fn(),
      clearSearchResults: jest.fn(),
      search: jest.fn(),
      value: null,
    };

    mockInput = {
      blur: jest.fn(),
    }

    ThemeProviderWrapper = TestUtils.getMockThemeProvider();
  });

  it('should set a selected item', () => {
    const autoComplete = shallow(
      <AutoCompleteContainer {...props} isOpen={true} />,
    );

    autoComplete.instance().setInputRef(mockInput);
    autoComplete.instance().setInputSelectedValue(mockSearchResults[0]);

    expect(autoComplete.instance().state.inputValue).toEqual(mockSearchResults[0].name);
    expect(autoComplete.instance().state.isOpen).toEqual(false);

    expect(props.input.onChange).toHaveBeenCalledWith(mockSearchResults[0]._id);
  });

  it('should set the selected item when new props are received', () => {
    const autoComplete = shallow(
      <AutoCompleteContainer {...props} isOpen={true} />,
    );

    let mock = jest.fn();
    autoComplete.instance().setInputSelectedValue = mock;
    autoComplete.setProps({ value: mockSearchResults[0] });


    expect(mock).toHaveBeenCalledWith(mockSearchResults[0]);
  });


  it('should get the next selected items index', () => {
    const autoComplete = shallow(
      <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults} />,
    );

    autoComplete.instance().setState({ selectedItem: 1 });
    expect(autoComplete.instance().getSelectedItemIndex(true)).toEqual(2);

    autoComplete.instance().setState({ selectedItem: 2 });
    expect(autoComplete.instance().getSelectedItemIndex(true)).toEqual(0);
  });

  it('should get the previous selected items index', () => {
    const autoComplete = shallow(
      <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults} />,
    );

    autoComplete.instance().setState({ selectedItem: 1 });
    expect(autoComplete.instance().getSelectedItemIndex(false)).toEqual(0);

    autoComplete.instance().setState({ selectedItem: 0 });
    expect(autoComplete.instance().getSelectedItemIndex(false)).toEqual(2);
  });

  describe("handleKeyDown", () => {
    describe("tab pressed", () => {
      it('should call onNoSelectionMade handler when searchResults is empty', () => {
        const autoComplete = shallow(
          <AutoCompleteContainer {...props} isOpen={true} />,
        );

        autoComplete.instance().handleKeyDown({ keyCode: 9, preventDefault: jest.fn() });

        expect(props.onNoSelectionMade).toHaveBeenCalled();
      });

      it('should highlight the next item when searchResults is not empty', () => {
        const autoComplete = shallow(
          <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults}/>,
        );
        const mockScrollTop = jest.fn();

        autoComplete.instance().setScrollBarRef({ scrollTop: mockScrollTop })
        autoComplete.instance().setState({ selectedItem: 1 });
        autoComplete.instance().handleKeyDown({ keyCode: 9, preventDefault: jest.fn() });

        expect(autoComplete.instance().state.selectedItem).toBe(2);
        expect(mockScrollTop).toHaveBeenCalled();
      });
    });

    describe("down key pressed", () => {
      it('should highlight then next item when last item is not selected', () => {
        const autoComplete = shallow(
          <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults}/>,
        );
        const mockScrollTop = jest.fn();

        autoComplete.instance().setScrollBarRef({ scrollTop: mockScrollTop })
        autoComplete.instance().setState({ selectedItem: 1 });
        autoComplete.instance().handleKeyDown({ keyCode: 40, preventDefault: jest.fn() });

        expect(autoComplete.instance().state.selectedItem).toBe(2);
        expect(mockScrollTop).toHaveBeenCalled();
      });

      it('should highlight then first item when last item is selected', () => {
        const autoComplete = shallow(
          <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults}/>,
        );
        const mockScrollTop = jest.fn();

        autoComplete.instance().setScrollBarRef({ scrollTop: mockScrollTop })
        autoComplete.instance().setState({ selectedItem: 2 });
        autoComplete.instance().handleKeyDown({ keyCode: 40, preventDefault: jest.fn() });

        expect(autoComplete.instance().state.selectedItem).toBe(0);
        expect(mockScrollTop).toHaveBeenCalled();
      });
    });

    describe("down up pressed", () => {
      it('should highlight then previous item when last first is not selected', () => {
        const autoComplete = shallow(
          <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults}/>,
        );
        const mockScrollTop = jest.fn();

        autoComplete.instance().setScrollBarRef({ scrollTop: mockScrollTop })
        autoComplete.instance().setState({ selectedItem: 1 });
        autoComplete.instance().handleKeyDown({ keyCode: 38, preventDefault: jest.fn() });

        expect(autoComplete.instance().state.selectedItem).toBe(0);
        expect(mockScrollTop).toHaveBeenCalled();
      });

      it('should highlight then last item when first item is selected', () => {
        const autoComplete = shallow(
          <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults}/>,
        );
        const mockScrollTop = jest.fn();

        autoComplete.instance().setScrollBarRef({ scrollTop: mockScrollTop })
        autoComplete.instance().setState({ selectedItem: 0 });
        autoComplete.instance().handleKeyDown({ keyCode: 38, preventDefault: jest.fn() });

        expect(autoComplete.instance().state.selectedItem).toBe(2);
        expect(mockScrollTop).toHaveBeenCalled();
      });
    });

    describe("enter key pressed", () => {
      it('should set currently highlighted open as selected when searchResults is not empty', () => {
        const autoComplete = shallow(
          <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults}/>,
        );
        const mockSetInputSelectedValue = jest.fn();

        autoComplete.instance().setInputSelectedValue = mockSetInputSelectedValue;
        autoComplete.instance().setInputRef(mockInput);
        autoComplete.instance().setState({ selectedItem: 1 });
        autoComplete.instance().handleKeyDown({ keyCode: 13, preventDefault: jest.fn() });

        expect(mockSetInputSelectedValue).toHaveBeenCalledWith(mockSearchResults[1]);
      });
    })
  });

  it('should start a search  when handle change event from input is received', () => {
    const autoComplete = shallow(
      <AutoCompleteContainer {...props} isOpen={true} searchResults={mockSearchResults}/>,
    );
    const searchString = "searchString";
    const mockSearch = jest.fn();

    autoComplete.instance().search = mockSearch;
    autoComplete.instance().handleChange({ target: { value: searchString } });

    expect(mockSearch).toHaveBeenCalledWith(searchString);
    expect(props.input.onChange).toHaveBeenCalledWith(null);
  });

  describe("handleBlur", () => {
    it('should call onNoResultsFound handler when searchResults is empty', () => {
      const autoComplete = shallow(
        <AutoCompleteContainer {...props} isOpen={true} />,
      );
      const mockValue = "Dave";
      const mockSetKeyPressListeners = jest.fn();
      autoComplete.instance().setKeyPressListeners = mockSetKeyPressListeners;
      autoComplete.instance().setState({ inputValue: mockValue });

      autoComplete.instance().handleBlur({ target: { value: mockValue } });

      
      expect(mockSetKeyPressListeners).toHaveBeenCalledWith(false);
      expect(autoComplete.instance().state.isOpen).toEqual(false);
      
      expect(props.onNoSelectionMade).toHaveBeenCalledWith(mockValue);
    });

    it('should fire input props onChange handler when event from input when searchResults length is 1 and input value matches that one results name', () => {
      const autoComplete = shallow(
        <AutoCompleteContainer {...props} isOpen={true} searchResults={[mockSearchResults[0]]} />,
      );
      const mockValue = mockSearchResults[0].name;
      const mockSetKeyPressListeners = jest.fn();

      autoComplete.instance().setKeyPressListeners = mockSetKeyPressListeners;
      autoComplete.instance().setState({ inputValue: mockValue });

      autoComplete.instance().handleBlur({ target: { value: mockValue } });

      expect(mockSetKeyPressListeners).toHaveBeenCalledWith(false);
      expect(props.input.onChange).toHaveBeenCalledWith(mockSearchResults[0]._id);
      expect(autoComplete.instance().state.isOpen).toEqual(false);
    });
  });

  it('should open options menu and enable keypress listeners when input is focused', () => {
    const autoComplete = shallow(
      <AutoCompleteContainer {...props} isOpen={false} searchResults={mockSearchResults}/>,
    );
    const mockSetKeyPressListeners = jest.fn();

    autoComplete.instance().setKeyPressListeners = mockSetKeyPressListeners;
    autoComplete.instance().handleFocus();

    expect(mockSetKeyPressListeners).toHaveBeenCalledWith(true);
    expect(autoComplete.instance().state.isOpen).toBe(true);
  });

  it("should call the search function from props when search function is called", () => {
    const autoComplete = shallow(
      <AutoCompleteContainer {...props} isOpen={false} searchResults={mockSearchResults}/>,
    );
    const searchString = "Dave";

    autoComplete.instance().search(searchString);

    expect(props.search).toHaveBeenCalledWith({"query": "name~Dave"});
  });

});