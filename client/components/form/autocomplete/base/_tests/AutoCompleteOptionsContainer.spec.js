import React from 'react'
import 'jest-styled-components'

import TestUtils from '../../../../../utils/test/TestUtils';
import { AutoCompleteOptionsContainer } from '../AutoCompleteOptionsContainer';

let ThemeProviderWrapper = TestUtils.getMockThemeProvider();

test('it renders correctly', () => {
  const autoCompleteOptionsContainer = mount(
    <ThemeProviderWrapper>
      <AutoCompleteOptionsContainer />
    </ThemeProviderWrapper>,
  );

  expect(autoCompleteOptionsContainer).toMatchSnapshot()
});