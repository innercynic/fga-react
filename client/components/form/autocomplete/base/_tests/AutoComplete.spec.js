import React from 'react';

import TestUtils from '../../../../../utils/test/TestUtils';
import { AutoComplete } from '../AutoComplete';
import { AutoCompleteInput } from '../AutoCompleteInput';
import { AutoCompleteOption } from '../AutoCompleteOption';

let store, storeData, ThemeProviderWrapper, props;

beforeEach(() => {
  props = {
    isSearching: false,
    isOpen: false,
    searchResults: [],
    setInputRef: jest.fn(),
    handleChange: jest.fn(),
    handleBlur: jest.fn(),
    handleFocus: jest.fn(),
    setInputSelectedValue: jest.fn(),
    setScrollBarRef: jest.fn(),
    inputValue: "",
  };

  ThemeProviderWrapper = TestUtils.getMockThemeProvider();
});

test('should render with default props and call setInputRef handling prop', () => {
  const autoComplete = mount(
    <ThemeProviderWrapper>
      <AutoComplete {...props} />
    </ThemeProviderWrapper>,
  );

  expect(props.setInputRef).toHaveBeenCalled();
  expect(autoComplete).toMatchSnapshot();
});

test('should call handleFocus and handleBlur props', () => {
  const autoComplete = mount(
    <ThemeProviderWrapper>
      <AutoComplete {...props} />
    </ThemeProviderWrapper>,
  );
  
  autoComplete.find(AutoCompleteInput).simulate('focus');
  expect(props.handleFocus).toHaveBeenCalled();

  autoComplete.find(AutoCompleteInput).simulate('blur');
  expect(props.handleBlur).toHaveBeenCalled();
});

test('should call handleChange prop', () => {
  const autoComplete = mount(
    <ThemeProviderWrapper>
      <AutoComplete {...props} />
    </ThemeProviderWrapper>,
  );

  autoComplete.find(AutoCompleteInput).simulate('change');
  expect(props.handleChange).toHaveBeenCalled();
});

test('should display options and call option mousedown event handling prop', () => {
  let result = {
    _id: "1",
    name: "Bob",
    info: "info",
    image: "image.jpg",
    thumbnail: "thumb.jpg"
  };

  props = {...props,
    isOpen: true,
    searchResults: [result],
  };

  const autoComplete = mount(
    <ThemeProviderWrapper>
      <AutoComplete {...props} />
    </ThemeProviderWrapper>,
  );

  expect(autoComplete).toMatchSnapshot();

  autoComplete.find(AutoCompleteOption).simulate('mousedown');
  expect(props.setInputSelectedValue).toHaveBeenCalledWith(result);
});
