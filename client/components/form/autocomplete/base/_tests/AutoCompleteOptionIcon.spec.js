import React from 'react'
import 'jest-styled-components'

import TestUtils from '../../../../../utils/test/TestUtils';
import { AutoCompleteOptionIcon } from '../AutoCompleteOptionIcon';

test('it renders with iconUrl missing (returns empty url)', () => {
  const autoCompleteOption = mount(
      <AutoCompleteOptionIcon />,
  );

  expect(autoCompleteOption).toMatchSnapshot()
  expect(autoCompleteOption).toHaveStyleRule('background', 'url("") center no-repeat');
});

test('it renders with corrent icon url set to background', () => {
  const autoCompleteOption = mount(
      <AutoCompleteOptionIcon iconUrl="\thumb\thumb.jpg" />,
  );

  expect(autoCompleteOption).toMatchSnapshot()
  expect(autoCompleteOption).toHaveStyleRule('background', 'url("/thumb/thumb.jpg") center no-repeat');
});