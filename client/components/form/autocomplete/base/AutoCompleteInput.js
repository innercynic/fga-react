import { Input } from '../../fields';

export const AutoCompleteInput = Input.extend`
  border-bottom-right-radius: ${props => (props.isHidingBottomBorderRadius ? 0 : props.theme.borders.radiuses.small)}; 
  border-bottom-left-radius: ${props => (props.isHidingBottomBorderRadius ? 0 : props.theme.borders.radiuses.small)};

  &:-webkit-autofill {
    -webkit-box-shadow: ${props => props.theme.webkit.autoFillBoxShadow};
  }

  &:focus {
    border-bottom: ${props => (props.isHidingBottomBorderRadius ? 0 : `${props.theme.borders.sizes.fine} solid ${props.theme.borders.colors.normal}`)};
    outline: none;
  }
`;
