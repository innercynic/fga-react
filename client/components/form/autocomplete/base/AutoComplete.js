import React from 'react';
import renderIf from 'render-if';
import styled from 'styled-components';
import { Flex } from 'grid-styled';
import { Scrollbars } from 'react-custom-scrollbars';
import PropTypes from 'prop-types';

import { FlexExtended, Loader } from '../../../general';
import { AutoCompleteInput } from './AutoCompleteInput';
import { AutoCompleteOptionsContainer } from './AutoCompleteOptionsContainer';
import { AutoCompleteOption, OPTION_HEIGHT } from './AutoCompleteOption';
import { AutoCompleteOptionIcon } from './AutoCompleteOptionIcon';

const LoaderPositioner = styled.div`
  position: absolute;
  right: 26px;
  top: 25px;
`;

const NUMBER_OF_OPTIONS = 5;

const AutoComplete = (props) => {
  const getContainerHeight = () => (
    props.searchResults.length * OPTION_HEIGHT <= OPTION_HEIGHT * NUMBER_OF_OPTIONS
      ? props.searchResults.length * OPTION_HEIGHT
      : OPTION_HEIGHT * NUMBER_OF_OPTIONS
  );

  return (
    <FlexExtended position="relative">
      <AutoCompleteInput
        innerRef={input => props.setInputRef(input)}
        isHidingBottomBorderRadius={props.isOpen && props.searchResults.length > 0}
        value={props.inputValue}
        onChange={props.handleChange}
        onBlur={props.handleBlur}
        onFocus={props.handleFocus}
      />

      <LoaderPositioner><Loader size="small" isLoading={props.isSearching} /></LoaderPositioner>

      {renderIf(props.isOpen && props.searchResults.length > 0)(() => (

        <AutoCompleteOptionsContainer column>
          <Scrollbars
            ref={(scrollbar) => { props.setScrollBarRef(scrollbar); }}
            style={{ height: getContainerHeight() }}
          >
            {props.searchResults.map((item, index) =>
              (<AutoCompleteOption
                align="center"
                p={5}
                key={item._id}
                onMouseDown={() => props.setInputSelectedValue(item)}
                isSelected={index === props.selectedItem}
              >
                <Flex ml={5} mr={15}>
                  <AutoCompleteOptionIcon iconUrl={item.thumbnail} />
                </Flex>
                <Flex align="center">
                  {item.name}
                </Flex>
              </AutoCompleteOption>),
            )}
          </Scrollbars>
        </AutoCompleteOptionsContainer>
      ))}

    </FlexExtended>
  );
};

AutoComplete.propTypes = {
  isSearching: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  searchResults: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    thumbnail: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  setInputRef: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleFocus: PropTypes.func.isRequired,
  inputValue: PropTypes.string.isRequired,
};

export { AutoComplete };
