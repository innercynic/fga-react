import React from 'react';
import sinon from 'sinon'
import axios from 'axios'

import MockData from '../../../../utils/test/MockData';

import { ArtistAutoComplete } from '../ArtistAutoComplete';

describe('ArtistAutoComplete', () => {
  let store, storeData, ThemeProviderWrapper, props;
  let mockSearchResults = MockData.mockArtists;

  beforeEach(() => {
    props = {
      clearSelectedArtist: jest.fn(),
      onNoResultsFound: jest.fn(),
      input: {
        name: "mock",
        onBlur: jest.fn(),
        onChange: jest.fn(),
        onFocus: jest.fn(),
        onDragStart: jest.fn(),
        onDrop: jest.fn(),
        value: "",
      },
      newArtist: null,
    };
  });

  it('renders', () => {
    const artistAutoComplete = shallow(
        <ArtistAutoComplete {...props} />,
    );

    expect(artistAutoComplete).toMatchSnapshot();
  })

  it('does fetch autocomplete searchresults', () => {
    const resolved = new Promise((r) => r({ data: { data: mockSearchResults } }));
    sinon.stub(axios, 'get').returns(resolved);

    const artistAutoComplete = shallow(
        <ArtistAutoComplete {...props} />,
    );

    var autoCompleteResultsPromise = artistAutoComplete.instance().searchArtists({"query": "name~Dave"});
    expect(artistAutoComplete.instance().state.isSearching).toBe(true);

    autoCompleteResultsPromise.then(() => {
      expect(artistAutoComplete.instance().state.isSearching).toBe(false);
      expect(artistAutoComplete.instance().state.searchResults).toBe(mockSearchResults);
    });
  });

});