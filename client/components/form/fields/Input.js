import styled from 'styled-components';

export const Input = styled.input`
  background: ${props => props.theme.backgrounds.input.normal};
  color: ${props => props.theme.fonts.colors.input.normal};
  border-radius: ${props => props.theme.borders.radiuses.small};  
  font-size: ${props => props.theme.fonts.sizes.text.normal};
  height: calc(${props => props.theme.fonts.sizes.text.normal} * 3);
  width: 100%;
  padding: 0 15px;
  border: none;
  display: block;

  &:-webkit-autofill {
    -webkit-box-shadow: ${props => props.theme.webkit.autoFillBoxShadow};
  }

  &:focus {
    background-color: ${props => props.theme.backgrounds.input.focus};
    color: ${props => props.theme.fonts.colors.input.focus};
    border: ${props => props.theme.borders.sizes.fine} solid ${props => props.theme.borders.colors.normal};
    outline: none;
  }

  &:hover {
    background-color: ${props => props.theme.backgrounds.input.hover};
    color: ${props => props.theme.fonts.colors.input.hover};
  }

  &:read-only {
    background-color: ${props => props.theme.backgrounds.input.readonly};
    color: ${props => props.theme.fonts.colors.input.readonly};
  }
`;
