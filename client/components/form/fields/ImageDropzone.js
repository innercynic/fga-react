import React from 'react';
import PropTypes from 'prop-types';
import ReactDropzone from 'react-dropzone';
import styled from 'styled-components';

import { InputShape } from '../../../proptypes';

export const StyledReactDropzone = styled(({ imageValue, ...rest }) => <ReactDropzone {...rest} />)`
  width: 200px;
  height: 200px;
  margin: 0 auto 10px auto;
  border-width: ${props => props.theme.borders.sizes.thick};
  border-color: ${props => props.theme.borders.colors.normal};
  border-style: dashed;
  border-radius: ${props => props.theme.borders.radiuses.small};
  background: ${props => `${(props.imageValue ? `url('${props.imageValue.preview}')` : 'transparent')} no-repeat center`};
  background-size: contain;

  &:hover {
    background-color: ${props => props.theme.backgrounds.input.hover};
    color: ${props => props.theme.fonts.colors.input.hover};
  }
`;

export class ImageDropzone extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(files) {
    // For Redux form
    if (this.props.input) {
      this.props.input.onChange(files[0]);
    } else if (this.props.onChange) {
      this.props.onChange(files[0]);
    } else {
      console.warn('redux-form-dropzone => Forgot to pass onChange props ?');
    }
  }

  render() {
    return (
      <StyledReactDropzone
        onDrop={this.onChange}
        multiple={this.props.multiple}
        imageValue={this.props.imageValue}
        accept="image/jpeg, image/png"
      />
    );
  }
}

ImageDropzone.defaultProps = {
  input: null,
  onChange: null,
  multiple: false,
  imageValue: { preview: '' },
};

ImageDropzone.propTypes = {
  input: InputShape,
  onChange: PropTypes.func,
  multiple: PropTypes.bool,
  imageValue: PropTypes.shape({
    preview: PropTypes.string,
  }),
};
