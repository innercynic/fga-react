import styled from 'styled-components';

export const Label = styled.label`
  color: ${props => props.theme.fonts.colors.subheading};
  font-family: ${props => props.theme.fonts.family.subheading};
  display: inline-block;
  font-size: ${props => props.theme.fonts.sizes.text.small};
`;
