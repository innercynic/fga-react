import { Input } from './Input';
import media from '../../../styles/media';

export const TextArea = Input.withComponent('textarea').extend`
  resize: none;
  height: 116px;
  padding-top: 10px;

  ${media.tabletPortrait`
    height: 116px;
  `}  

  ${media.tabletLandscape`
    height: 116px;
  `}
`;
