import React from 'react'
import 'jest-styled-components'

import MockData from '../../../../utils/test/MockData';
import { ImageDropzone, StyledReactDropzone } from '../ImageDropzone';

let onChange = jest.fn();
let input = {
  name: "mock",
  onBlur: jest.fn(),
  onChange: jest.fn(),
  onFocus: jest.fn(),
  onDragStart: jest.fn(),
  onDrop: jest.fn(),
  value: "",
};

test('it renders', () => {
  const imageDropzone = shallow(
    <ImageDropzone input={input}/>
  );

  expect(imageDropzone).toMatchSnapshot();
});

test('its styled ReactDropzone renders with bg image', () => {
  const styledReactDropzone = shallow(
    <StyledReactDropzone 
      imageValue={{ preview: "/mockImage.jpg" }}
      theme={MockData.mockThemes[MockData.mockThemeName]} 
    />
  );

  expect(styledReactDropzone).toHaveStyleRule('background', "url('/mockImage.jpg') no-repeat center");
  expect(styledReactDropzone).toMatchSnapshot();
});

test('it calls input props onChange method when available', () => {
  const imageDropzone = shallow(
      <ImageDropzone input={input} onChange={onChange} />
  );

  imageDropzone.instance().onChange([{}]);

  expect(onChange).not.toHaveBeenCalled();
  expect(input.onChange).toHaveBeenCalled();
});

test('it calls onChange prop when there is no input prop present', () => {
  const imageDropzone = shallow(
      <ImageDropzone onChange={onChange} />
  );

  imageDropzone.instance().onChange([{}]);

  expect(onChange).toHaveBeenCalled();
});