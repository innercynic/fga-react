import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { TextArea } from '../TextArea';

    
test('it renders', () => {
  const textArea = shallow(
    <TextArea theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(textArea).toMatchSnapshot();
});
