import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { Label } from '../Label';


test('it renders', () => {
  const label = shallow(
    <Label theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(label).toMatchSnapshot();
});
