import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { Input } from '../Input';

test('it renders', () => {
  const input = shallow(
    <Input theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(input).toMatchSnapshot();
});
