import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { Checkbox } from '../Checkbox';

test('it renders', () => {
  const checkbox = shallow(
    <Checkbox theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(checkbox).toMatchSnapshot();
});