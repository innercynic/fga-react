import styled from 'styled-components';
import { Flex } from 'grid-styled';

const HeaderContainer = styled(Flex)`
  width: 100%;
  background-color: ${props => props.theme.backgrounds.mainOverlay};
`;

export default HeaderContainer;
