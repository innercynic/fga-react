import styled from 'styled-components';
import { Box } from 'grid-styled';
import media from '../../styles/media';

const PageTitle = styled(Box)`
  font-family: ${props => props.theme.fonts.family.title};
  font-size: 1.8rem;
  color: ${props => props.theme.fonts.colors.title};

  ${media.tabletPortrait`
    font-size: 2.4rem;
  `}

  ${media.desktop`
    font-size: 2.8rem;
  `}

  ${media.hd`
    font-size: 3.2rem;
  `}
`;

export default PageTitle;
