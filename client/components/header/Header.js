import React from 'react';

import HeaderContainer from './HeaderContainer';
import PageTitle from '../header/PageTitle';
import SettingsContainer from '../user/settings/SettingsContainer';
import LoginContainer from '../user/login/LoginContainer';
import UserMenuContainer from '../user/menu/UserMenuContainer';

import { IfAuthenticated, IfNotAuthenticated } from '../auth/Authenticated';
import { LinkWithoutUnderline } from '../general';

const Header = () => (
  <HeaderContainer py={[1, 1, 1.5]} justify="center" align="center">
    <LinkWithoutUnderline to="/">
      <PageTitle m={[1, 1, 2, 3]}>Fingerstylist</PageTitle>
    </LinkWithoutUnderline>

    <IfNotAuthenticated m={[1, 1, 2, 3]}>
      <LoginContainer />
    </IfNotAuthenticated>

    <IfAuthenticated m={[1, 1, 2, 3]} >
      <UserMenuContainer />
      <SettingsContainer />
    </IfAuthenticated>

  </HeaderContainer>
);

export default Header;
