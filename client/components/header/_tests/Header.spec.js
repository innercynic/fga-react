import React from 'react';

import Header from '../Header';

test('it renders', () => {
  const header = shallow(
    <Header />,
  );

  expect(header).toMatchSnapshot();
});