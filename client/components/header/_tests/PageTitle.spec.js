import React from 'react';

import MockData from '../../../utils/test/MockData';
import PageTitle from '../PageTitle';

test('it renders', () => {
  const pageTitle = shallow(
    <PageTitle theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(pageTitle).toMatchSnapshot();
});