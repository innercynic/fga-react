import React from 'react';

import MockData from '../../../utils/test/MockData';
import HeaderContainer from '../HeaderContainer';

test('it renders', () => {
  const headerContainer = shallow(
    <HeaderContainer theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(headerContainer).toMatchSnapshot();
});