import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import UserMenu from './UserMenu';
import { logout } from '../../../actions/user/auth';
import { openSettings } from '../../../actions/user/settings';

export class UserMenuContainer extends Component {
  constructor(props) {
    super(props);

    this.toggleMenu = this.toggleMenu.bind(this);

    this.state = {
      menuIsOpen: false,
    };
  }

  toggleMenu() {
    this.setState({
      menuIsOpen: !this.state.menuIsOpen,
    });
  }

  render() {
    return (
      <UserMenu
        menuIsOpen={this.state.menuIsOpen}
        openSettings={this.props.openSettings}
        logout={this.props.logout}
        toggleMenu={this.toggleMenu}
        buttonImageUrl={this.props.userImageUrl}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    userImageUrl: state.auth.user.image,

  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ openSettings, logout }, dispatch);
}

UserMenuContainer.propTypes = {
  openSettings: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  userImageUrl: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, matchDispatchToProps)(UserMenuContainer);
