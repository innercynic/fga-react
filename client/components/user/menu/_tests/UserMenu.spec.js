import React from 'react'
import 'jest-styled-components'

import UserMenu from '../UserMenu';
import { ImgAsBackground } from '../../../general';

let props = {
  openSettings: jest.fn(),
  logout: jest.fn(),
  toggleMenu: jest.fn(),
  buttonImageUrl: "mockUserImage.jpg",
  menuIsOpen: true,
};

test('it renders', () => {
  const userMenu = shallow(
      <UserMenu {...props} />
  );

  expect(userMenu).toMatchSnapshot();
});

test('it opens settings', () => {
  const userMenu = shallow(
      <UserMenu {...props} />
  );

  userMenu.find(ImgAsBackground).simulate('click');

  expect(props.toggleMenu).toHaveBeenCalled();
});