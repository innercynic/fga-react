import React from 'react'
import 'jest-styled-components'

import { UserMenuContainer } from '../UserMenuContainer';
import UserMenu from '../UserMenu';
import { FlexExtended } from '../../../general';

let props = {
  openSettings: jest.fn(),
  logout: jest.fn(),
  userImageUrl: "mockUserImage.jpg",
};

test('it renders', () => {
  const userMenuContainer = shallow(
      <UserMenuContainer {...props} />
  );

  expect(userMenuContainer).toMatchSnapshot();
});

test('it changes menu is open state', () => {
  const contextMenuInstance = shallow(
      <UserMenuContainer {...props} />
  ).instance();

  contextMenuInstance.toggleMenu();
  expect(contextMenuInstance.state.menuIsOpen).toBeTruthy();
});
