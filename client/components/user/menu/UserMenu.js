import React from 'react';
import PropTypes from 'prop-types';
import { Box } from 'grid-styled';

import ContextMenu from '../../menu/context/ContextMenu';
import { FlexExtended, ImgAsBackground, Icon, Text } from '../../general';


const UserMenu = props => (
  <FlexExtended position="relative">
    <ImgAsBackground
      onClick={props.toggleMenu}
      width={props.buttonSize}
      height={props.buttonSize}
      asCircle
      clickable
      url={props.buttonImageUrl}
    />
    <ContextMenu
      isOpen={props.menuIsOpen}
      closeContextMenu={props.toggleMenu}
      offsetY={53}
      arrowHeight={7}
      arrowOffset={18}
      width={250}
    >
      <FlexExtended clickable menuItemClicked={props.openSettings} p={[2]}>
        <Box mr={[1]}><Icon clickable icon="settings" size="large" /></Box>
        <Text>Show Settings</Text>
      </FlexExtended>
      <FlexExtended clickable menuItemClicked={props.logout} p={[2]}>
        <Box mr={[1]}><Icon clickable icon="logout" size="large" /></Box>
        <Text>Logout</Text>
      </FlexExtended>
    </ContextMenu>
  </FlexExtended>
);

UserMenu.defaultProps = {
  buttonSize: 50,
};

UserMenu.propTypes = {
  buttonSize: PropTypes.number,
  buttonImageUrl: PropTypes.string.isRequired,
  menuIsOpen: PropTypes.bool.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  openSettings: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
};

export default UserMenu;

