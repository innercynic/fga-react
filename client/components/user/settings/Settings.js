import React from 'react';
import { Flex, Box } from 'grid-styled';

import ThemeSwitcherContainer from '../../themeswitcher/ThemeSwitcherContainer';

const Settings = () => (
  <Box>
    <Flex width={1} justify="center" align="center" p={[2, 2, 3, 3]}>
      <ThemeSwitcherContainer />
    </Flex>
  </Box>
);

export default Settings;
