import React from 'react';

import { SettingsContainer } from '../SettingsContainer';

let props = {
  closeSettings: jest.fn(),
};

test('it renders', () => {
  const settingsContainer = shallow(
    <SettingsContainer {...props} />,
  );

  expect(settingsContainer).toMatchSnapshot();
});