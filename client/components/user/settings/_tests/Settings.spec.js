import React from 'react';

import Settings  from '../Settings';

test('should shallow render', () => {
  const settings = shallow(
    <Settings />,
  );

  expect(settings).toMatchSnapshot();
});