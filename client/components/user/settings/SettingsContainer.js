import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { closeSettings } from '../../../actions/user/settings';
import Settings from './Settings';
import OverlayModal from '../../modals/OverlayModal';

export const SettingsContainer = props => (
  <OverlayModal
    isOpen={props.isSettingsOpen}
    contentLabel="Settings"
    onRequestClose={props.closeSettings}
    maxWidth={400}
  >
    <Settings />
  </OverlayModal>
);

function mapStateToProps(state) {
  return {
    isSettingsOpen: state.settings.isSettingsOpen,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ closeSettings }, dispatch);
}

SettingsContainer.defaultProps = {
  isSettingsOpen: false,
};

SettingsContainer.propTypes = {
  isSettingsOpen: PropTypes.bool,
  closeSettings: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, matchDispatchToProps)(SettingsContainer);
