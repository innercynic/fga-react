import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import { push } from 'react-router-redux';

import { openLogin } from '../../../actions/user/auth';
import { createUser, clearSelectedUser } from '../../../actions/user/user';
import { UserShape } from '../../../proptypes';

import SignUpForm from './SignUpForm';
import { Container, LoaderWithOverlay, Heading, Text, Button, ImgAsBackground } from '../../general';
import OverlayModal from '../../modals/OverlayModal';
import { IfAuthenticated, IfNotAuthenticated } from '../../auth/Authenticated';

export class SignUpContainer extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.redirectToLogin = this.redirectToLogin.bind(this);
  }

  handleSubmit(values) {
    this.props.createUser(values);
  }

  redirectToLogin() {
    this.closeModal();
    this.props.openLogin();
    this.props.push('/');
  }

  closeModal() {
    this.props.clearSelectedUser();
  }

  render() {
    return (
      <Container flex="1" maxWidth={700} justify="center">
        <LoaderWithOverlay isLoading={this.props.isPostingData} />

        <IfNotAuthenticated flex="1">
          <SignUpForm
            onSubmit={this.handleSubmit}
            createAccountError={this.props.createUserError}
          />
        </IfNotAuthenticated>

        <IfAuthenticated p={[1, 1, 2, 3]}>
          <Text>Please sign out if you wish to make a new account</Text>
        </IfAuthenticated>

        <OverlayModal
          isOpen={!!this.props.newUser}
          onRequestClose={this.closeModal}
          maxWidth={400}
        >
          {this.props.newUser &&
            <Flex column p={20} align="center">
              <Box my={5}>
                <Heading>Account created</Heading>
              </Box>
              <Box my={5}>
                <ImgAsBackground
                  asCircle
                  width={80}
                  height={80}
                  url={this.props.newUser.thumbnail}
                />
              </Box>
              <Box my={5}>
                <Text>Welcome to Fingerstylist, {this.props.newUser.name}.</Text>
              </Box>
              <Box my={5}>
                <Button
                  id="redirect-to-login"
                  width="150px"
                  onClick={this.redirectToLogin}
                >
                  Login
                </Button>
              </Box>
            </Flex>
          }
        </OverlayModal>

      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isPostingData: state.user.isPostingData,
    createUserError: state.user.errorMessage,
    newUser: state.user.selected,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ createUser, clearSelectedUser, openLogin, push }, dispatch);
}

SignUpContainer.defaultProps = {
  newUser: null,
  createUserError: null,
};

SignUpContainer.propTypes = {
  createUser: PropTypes.func.isRequired,
  clearSelectedUser: PropTypes.func.isRequired,
  openLogin: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  isPostingData: PropTypes.bool.isRequired,
  createUserError: PropTypes.string,
  newUser: PropTypes.shape(UserShape),
};

export default connect(mapStateToProps, matchDispatchToProps)(SignUpContainer);
