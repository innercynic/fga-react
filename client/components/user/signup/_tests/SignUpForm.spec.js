import React from 'react';

import { SignUpForm } from '../SignUpForm';
import { ErrorText } from '../../../general';


let props;

beforeEach(() => {
  props = {
    handleSubmit: jest.fn(),
    imageValue: {preview:''},
  };
})

test('it renders', () => {
  const signUpForm = shallow(
    <SignUpForm {...props} />,
  );

  expect(signUpForm).toMatchSnapshot();
});

test('it renders an error', () => {
  const mockErrorMessage = "Account already exists";
  const signUpForm = shallow(
    <SignUpForm {...props} createAccountError={mockErrorMessage} />,
  );

  expect(signUpForm.find(ErrorText).props().children).toBe(mockErrorMessage);
});

test('it calls prop when form is submitted', () => {
  const signUpForm = shallow(
    <SignUpForm {...props} />
  );

  signUpForm.find('form').props().onSubmit();

  expect(props.handleSubmit).toHaveBeenCalled();
});