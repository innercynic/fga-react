import React from 'react';

import { SignUpContainer } from '../SignUpContainer';
import MockData from '../../../../utils/test/MockData';

let props;

beforeEach(() => {
  props = {
    isPostingData: false,
    createUser: jest.fn(),
    clearNewUser: jest.fn(),
    openLogin: jest.fn(),
    push: jest.fn(),
  };
})

test('it renders', () => {
  const signUpContainer = shallow(
    <SignUpContainer {...props} />,
  );

  expect(signUpContainer).toMatchSnapshot();
});

test('it displays new user data after user is created', () => {
  let newUser = MockData.mockUsers[0];

  const signUpContainer = shallow(
      <SignUpContainer {...props} newUser={newUser} />
  );

  expect(signUpContainer).toMatchSnapshot();
});

test('it redirects homepage and displays login modal when user clicks button after creating account', () => {
  let newUser = MockData.mockUsers[0];

  const signUpContainer = shallow(
      <SignUpContainer {...props} newUser={newUser} />
  );

  signUpContainer.find("#redirect-to-login").simulate('click');

  expect(props.clearNewUser).toHaveBeenCalled();
  expect(props.openLogin).toHaveBeenCalled();
  expect(props.push).toHaveBeenCalled();
});

test('it calls create user when submitting information', () => {
  let userFormValues = MockData.mockUsers[0];

  const signUpContainer = shallow(
      <SignUpContainer {...props}/>
  );

  signUpContainer.instance().handleSubmit(userFormValues)

  expect(props.createUser).toHaveBeenCalled();
});