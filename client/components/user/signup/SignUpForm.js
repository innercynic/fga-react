import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import validate from '../../../validation/signup';

import { Button, ErrorText } from '../../general';
import { Label } from '../../form/fields';
import { RFInput, RFImageDropzone, RFFieldError } from '../../form/reduxformfields';

export const SignUpForm = props => (
  <form onSubmit={props.handleSubmit}>
    <Flex p={[3, 3, 4, 4]} wrap>

      <Flex column flex="1 1 300px">
        <Box>
          <Label htmlFor="image">Profile Picture</Label>
          <RFFieldError name="image" />
        </Box>
        <Flex flex="1" align="center" justify="center">
          <Box >
            <RFImageDropzone imageValue={props.imageValue} name="image" />
          </Box>
        </Flex>
      </Flex>

      <Flex column flex="1 1 300px">

        <Box>
          <Label htmlFor="name">Name</Label>
          <RFFieldError name="name" />
          <Flex flex={1}>
            <RFInput name="name" />
          </Flex>
        </Box>

        <Box>
          <Label htmlFor="email">Email</Label>
          <RFFieldError name="email" />
          {props.createAccountError &&
          <ErrorText>{props.createAccountError}</ErrorText>
          }
          <Flex flex={1}>
            <RFInput name="email" type="email" />
          </Flex>
        </Box>

        <Box>
          <Label htmlFor="password">Password</Label>
          <RFFieldError name="password" />
          <Flex flex={1}>
            <RFInput name="password" type="password" />
          </Flex>
        </Box>

        <Box>
          <Label htmlFor="password_verification">Repeat Password</Label>
          <RFFieldError name="password_verification" />
          <Flex flex={1}>
            <RFInput name="password_verification" type="password" />
          </Flex>
        </Box>
      </Flex>

      <Flex mt={10} flex="1 1 300px">
        <Button type="submit" width="100%">Signup</Button>
      </Flex>

    </Flex>
  </form>
);

SignUpForm.defaultProps = {
  createAccountError: null,
  imageValue: { preview: '' },
};

SignUpForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  createAccountError: PropTypes.string,
  imageValue: PropTypes.shape({
    preview: PropTypes.string,
  }),
};

const selector = formValueSelector('signup');

export default connect(state => ({ imageValue: selector(state, 'image') }))(
  reduxForm({
    form: 'signup',
    validate,
  })(SignUpForm));
