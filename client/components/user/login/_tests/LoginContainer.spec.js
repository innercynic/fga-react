import React from 'react'

import TestUtils from '../../../../utils/test/TestUtils';
import { LoginContainer } from '../LoginContainer';
import Login from '../Login';
import { Button } from '../../../general';

let props;

beforeEach(() => {
  props = {
    isLoggingIn: false,
    isLoginModalOpen: false,
    loginError: 'mockError',
    push: jest.fn(),
    login: jest.fn(),
    openLogin: jest.fn(),
    closeLogin: jest.fn(),
    clearLoginError: jest.fn(),
  };
})

test('it renders', () => {
  const loginContainer = shallow(
    <LoginContainer {...props} />
  );

  expect(loginContainer).toMatchSnapshot();
});

test('it opens login modal when login button is pressed', () => {
  const loginContainer = shallow(
      <LoginContainer {...props} />
  );

  loginContainer.find(Button).simulate('click');

  expect(props.openLogin).toHaveBeenCalled();
});

test('it handles submit by calling login prop', () => {
  const mockLoginDetails = { email: "mock@user.com", password: "mockpassword" };
  const loginContainer = shallow(
      <LoginContainer {...props} />
  );

  loginContainer.instance().handleSubmit(mockLoginDetails);

  expect(props.login).toHaveBeenCalledWith(mockLoginDetails);
  expect(props.clearLoginError).toHaveBeenCalled();
});

test('it redirects to signup by calling close login and push props', () => {
  const loginContainer = shallow(
      <LoginContainer {...props} />
  );

  loginContainer.instance().redirectToSignUp();

  expect(props.closeLogin).toHaveBeenCalled();
  expect(props.push).toHaveBeenCalledWith('/signup');
});