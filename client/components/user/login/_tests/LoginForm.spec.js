import React from 'react'
import 'jest-styled-components'

import { LoginForm } from '../LoginForm';
import { Button } from '../../../general';

let props;

beforeEach(() => {
  props = {
    handleSubmit: jest.fn(),
  };
})

test('it renders', () => {
  const loginForm = shallow(
    <LoginForm {...props} />
  );

  expect(loginForm).toMatchSnapshot();
});

test('it calls prop when form is submitted', () => {
  const loginForm = shallow(
    <LoginForm {...props} />
  );

  loginForm.find('form').props().onSubmit();

  expect(props.handleSubmit).toHaveBeenCalled();
});