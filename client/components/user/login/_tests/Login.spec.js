import React from 'react'
import 'jest-styled-components'

import Login from '../Login';
import { Button } from '../../../general';

let props;

beforeEach(() => {
  props = {
    isLoggingIn: false,
    loginError: null,
    handleSubmit: jest.fn(),
    redirectToSignUp: jest.fn(),
  };
})

test('it renders', () => {
  const login = shallow(
    <Login {...props} />
  );

  expect(login).toMatchSnapshot();
});

test('it renders login errors', () => {
  const login = shallow(
    <Login {...props} loginError="Login failed" />
  );

  expect(login).toMatchSnapshot();
});

test('it redirects to sign up by called prop', () => {
  const login = shallow(
    <Login {...props} />
  );

  login.find('#login-button').simulate('click');

  expect(props.redirectToSignUp).toHaveBeenCalled();
});