import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { Flex } from 'grid-styled';

import { login, openLogin, closeLogin, clearLoginError } from '../../../actions/user/auth';
import Login from './Login';
import { Text, Icon, Button } from '../../general';
import OverlayModal from '../../modals/OverlayModal';


export class LoginContainer extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.redirectToSignUp = this.redirectToSignUp.bind(this);
  }

  componentWillUnmount() {
    this.props.clearLoginError();
  }

  handleSubmit(val) {
    this.props.clearLoginError();
    this.props.login(val);
  }

  redirectToSignUp() {
    this.props.closeLogin();
    this.props.push('/signup');
  }

  render() {
    return (
      <Flex>
        <Button onClick={this.props.openLogin}>
          <Icon icon="login" />
          <Text>Login</Text>
        </Button>

        <OverlayModal
          isOpen={this.props.isLoginModalOpen}
          contentLabel="Settings"
          onRequestClose={this.props.closeLogin}
          maxWidth={400}
        >
          <Login
            handleSubmit={this.handleSubmit}
            loginError={this.props.loginError}
            redirectToSignUp={this.redirectToSignUp}
            isLoggingIn={this.props.isLoggingIn}
          />
        </OverlayModal>
      </Flex>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoggingIn: state.auth.isLoggingIn,
    isLoginModalOpen: state.auth.isLoginOpen,
    loginError: state.auth.errorMessage,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ login, openLogin, closeLogin, clearLoginError, push }, dispatch);
}

LoginContainer.propTypes = {
  login: PropTypes.func.isRequired,
  isLoggingIn: PropTypes.bool.isRequired,
  isLoginModalOpen: PropTypes.bool.isRequired,
  push: PropTypes.func.isRequired,
  openLogin: PropTypes.func.isRequired,
  closeLogin: PropTypes.func.isRequired,
  clearLoginError: PropTypes.func.isRequired,
  loginError: PropTypes.string.isRequired,

};

export default connect(mapStateToProps, matchDispatchToProps)(LoginContainer);
