import React from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import validate from '../../../validation/login';

import { Label } from '../../form/fields';
import { RFFieldWithIcon, RFFieldError } from '../../form/reduxformfields';
import { Button } from '../../general';

export const LoginForm = props => (
  <form onSubmit={props.handleSubmit}>
    <Flex p={[2, 2, 3, 3]} column>
      <Box mb={10}>
        <Label htmlFor="email">Email</Label>
        <RFFieldError name="email" />
        <RFFieldWithIcon name="email" icon="email" />
      </Box>
      <Box mb={10}>
        <Label htmlFor="password">Password</Label>
        <RFFieldError name="password" />
        <RFFieldWithIcon name="password" icon="password" type="password" />
      </Box>
      <Box>
        <Button type="submit" width="100%">Login</Button>
      </Box>
    </Flex>
  </form>
);

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'login',
  validate,
})(LoginForm);
