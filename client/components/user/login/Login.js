import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';

import LoginForm from './LoginForm';
import { LoaderWithOverlay, Text, TextItalics, Button } from '../../general';

const Login = props => (
  <Flex column>
    <LoaderWithOverlay isLoading={props.isLoggingIn} />

    {props.loginError &&
      <Flex justify="center">
        <Text>{props.loginError}</Text>
      </Flex>
    }

    <LoginForm onSubmit={props.handleSubmit} initialValues={{ email: 'jon@gomm.com', password: 'jonnyboy' }} />

    <Flex justify="center">
      <TextItalics>or</TextItalics>
    </Flex>

    <Flex p={20}>
      <Button id="login-button" onClick={props.redirectToSignUp} width="100%">Sign Up</Button>
    </Flex>
  </Flex>
);

Login.defaultProps = {
  loginError: '',
};

Login.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  redirectToSignUp: PropTypes.func.isRequired,
  loginError: PropTypes.string,
  isLoggingIn: PropTypes.bool.isRequired,
};

export default Login;
