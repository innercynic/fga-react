import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Flex, Box } from 'grid-styled';
import { Button, Text } from '../general';

class ThemeSwitcher extends Component {
  constructor(props) {
    super(props);
    this.createThemeButtons = this.createThemeButtons.bind(this);
  }

  createThemeButtons(themes) {
    const themeButtons = [];
    const selectedStyle = { border: '2px solid', borderRadius: '7px' };

    Object.keys(themes).forEach((key) => {
      themeButtons.push(
        <Box
          mx={5}
          key={themes[key].themeid}
          style={key === this.props.currentTheme ? selectedStyle : {}}
        >
          <Button
            id={`themebutton${themes[key].themeid}`}
            size="small"
            onClick={() => { this.props.switchTheme(key); }}
          >
            {themes[key].themename}
          </Button>
        </Box>,
      );
    });

    return themeButtons;
  }

  render() {
    return (
      <Flex>
        <Flex justify="center" align="center">
          <Text>Change Theme: </Text>
        </Flex>
        <Flex justify="center" align="center">
          {this.createThemeButtons(this.props.themes)}
        </Flex>
      </Flex>
    );
  }
}

ThemeSwitcher.propTypes = {
  switchTheme: PropTypes.func.isRequired,
  themes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  currentTheme: PropTypes.string.isRequired,
};

export default ThemeSwitcher;
