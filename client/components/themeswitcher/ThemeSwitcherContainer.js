import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { switchTheme } from '../../actions/user/settings';
import ThemeSwitcher from './ThemeSwitcher';

// eslint-disable-next-line no-shadow
export const ThemeSwitcherContainer = ({ switchTheme, themes, currentTheme }) => (
  <ThemeSwitcher
    switchTheme={switchTheme}
    themes={themes}
    currentTheme={currentTheme}
  />
);

function mapStateToProps(state) {
  return {
    currentTheme: state.settings.currentTheme,
    themes: state.settings.themes,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ switchTheme }, dispatch);
}

ThemeSwitcherContainer.propTypes = {
  currentTheme: PropTypes.string.isRequired,
  themes: PropTypes.objectOf(PropTypes.string).isRequired,
  switchTheme: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, matchDispatchToProps)(ThemeSwitcherContainer);
