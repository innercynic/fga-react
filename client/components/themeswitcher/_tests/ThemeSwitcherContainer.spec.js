import React from 'react';
import { ThemeSwitcherContainer } from '../ThemeSwitcherContainer';
import themes from '../../../themes';

let props;

beforeEach(() => {
  props = {
    currentTheme: "mock-theme-name",
    themes: {},
    switchTheme: jest.fn(),
  };
})

test('it renders', () => {
  const themeSwitcherContainer = shallow(
    <ThemeSwitcherContainer {...props} />,
  );
  expect(themeSwitcherContainer).toMatchSnapshot();
});