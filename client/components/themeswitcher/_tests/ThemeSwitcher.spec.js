import React from 'react';
import ThemeSwitcher from '../ThemeSwitcher';
import { Button } from '../../general';
import { Box } from 'grid-styled';

import MockData from '../../../utils/test/MockData';

let props;

beforeEach(() => {
  props = {
    switchTheme: jest.fn(),
    themes: MockData.mockThemes,
    currentTheme: MockData.mockThemeName,
  };
})

test('it renders', () => {
  let switchTheme = jest.fn();
  const themeSwitcherContainer = shallow(
    <ThemeSwitcher {...props} />,
  );
  expect(themeSwitcherContainer).toMatchSnapshot();
});

test('should call switchTheme when button is pressed', () => {
  let switchTheme = jest.fn();
  const themeSwitcherContainer = shallow(
    <ThemeSwitcher {...props} />,
  );

  themeSwitcherContainer.find('#themebutton' + MockData.mockThemes[MockData.mockThemeName].themeid)
  .simulate('click');

  expect(props.switchTheme).toHaveBeenCalledWith(MockData.mockThemeName);
});

test('should have a button for each theme', () => {
  let switchTheme = jest.fn();
  const themeSwitcherContainer = shallow(
    <ThemeSwitcher {...props} />,
  );

  expect(themeSwitcherContainer.find(Button)).toHaveLength(2);
});

test('selected theme (only) should have different style', () => {
  const themeSwitcherContainer = shallow(
    <ThemeSwitcher {...props} />,
  );

  const themeButtonBox1Style = themeSwitcherContainer.find(Box).first().props().style;
  const themeButtonBox2Style = themeSwitcherContainer.find(Box).last().props().style;

  expect(themeButtonBox1Style).toEqual({ border: '2px solid', borderRadius: '7px' });
  expect(themeButtonBox1Style).not.toEqual(themeButtonBox2Style);
});