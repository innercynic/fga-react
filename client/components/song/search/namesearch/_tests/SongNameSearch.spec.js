import React from 'react';

import SongNameSearch from '../SongNameSearch';

let props;

beforeEach(() => {
  props = {
    onSearch: jest.fn(),
  };
})

test('it renders', () => {
  const songNameSearch = shallow(
    <SongNameSearch {...props} />,
  );

  expect(songNameSearch).toMatchSnapshot();
});
