import React from 'react';

import { SongNameSearchContainer } from '../SongNameSearchContainer';

let props, propsWithSearchValue;

beforeEach(() => {
  props = {
    requestSongs: jest.fn(),
    clearSongs: jest.fn(),
    push: jest.fn(),
    changeFieldValue: jest.fn(),
    match: {
      path: "",
      url: "",
      params: {
        searchValue: ""
      },
    },
  };

  propsWithSearchValue = {
    ...props,
    match: {
      ...props.match,
      params: {
        searchValue: "searchValueMock",
      }
    }
  };
})

test('it renders', () => {
  const songNameSearchContainer = shallow(
    <SongNameSearchContainer {...props} />,
  );

  expect(songNameSearchContainer).toMatchSnapshot();
});

test('it fires initial search when route contains searchValue', () => {
  const songNameSearchContainer = shallow(
    <SongNameSearchContainer {...propsWithSearchValue} />
  );

  expect(propsWithSearchValue.requestSongs).toHaveBeenCalled();
});

test('it fires search when route is updated to contain searchValue', () => {
  const songNameSearchContainer = shallow(
    <SongNameSearchContainer {...props} />,
    { lifecycleExperimental: true }
  );

  expect(props.requestSongs).not.toHaveBeenCalled();

  songNameSearchContainer.setProps({match: propsWithSearchValue.match});

  expect(props.requestSongs).toHaveBeenCalled();
});

test('it clears search results on unmount', () => {
  const songNameSearchContainer = shallow(
    <SongNameSearchContainer {...propsWithSearchValue} />
  );

  songNameSearchContainer.unmount();

  expect(propsWithSearchValue.clearSongs).toHaveBeenCalled();
});

test('it updates url on search )', () => {
  const songNameSearchContainer = shallow(
    <SongNameSearchContainer {...props} />
  );

  songNameSearchContainer.instance().onSearch({ searchValue: 'mocksearch' });

  expect(propsWithSearchValue.push).toHaveBeenCalled();
});

test('it updates form field value and requests songs when searching songs', () => {
  const songNameSearchContainer = shallow(
    <SongNameSearchContainer {...props} />
  );

  songNameSearchContainer.instance().searchSongs('mocksearch');

  expect(propsWithSearchValue.changeFieldValue).toHaveBeenCalled();
  expect(propsWithSearchValue.requestSongs).toHaveBeenCalledWith({"query": "name~mocksearch"});
});

