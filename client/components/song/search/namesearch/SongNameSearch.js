import React from 'react';
import { Flex, Box } from 'grid-styled';
import PropTypes from 'prop-types';

import SearchForm from '../../../search/SearchForm';
import { Container } from '../../../general';

import SongSearchResultsContainer from '../searchresults/SongSearchResultsContainer';

export const SongNameSearch = props => (
  <Container flex="1" column noBackground width={1}>

    <Flex justify="center" m={20}>
      <SearchForm onSubmit={props.onSearch} />
    </Flex>

    <SongSearchResultsContainer />

  </Container>
);

SongNameSearch.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default SongNameSearch;
