import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { change as changeFieldValue } from 'redux-form';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';

import SongNameSearch from './SongNameSearch';
import { requestSongs, clearSongs } from '../../../../actions/songs/songs';

export class SongNameSearchContainer extends Component {
  constructor(props) {
    super(props);

    this.onSearch = this.onSearch.bind(this);
    this.searchSongs = this.searchSongs.bind(this);
  }

  componentWillMount() {
    if (!this.props.match) return;

    if (this.props.match.params.searchValue && this.props.match.params.searchValue !== '') {
      this.searchSongs(this.props.match.params.searchValue);
    }
  }

  componentDidUpdate(prevProps) {
    if (!this.props.match) return;

    if (this.props.match.params.searchValue
      && this.props.match.params.searchValue !== prevProps.match.params.searchValue) {
      this.searchSongs(this.props.match.params.searchValue);
    }
  }

  componentWillUnmount() {
    this.props.clearSongs();
  }

  onSearch(searchFormValues) {
    if (!this.props.match) return;

    if (searchFormValues.searchValue && searchFormValues.searchValue !== ''
     && searchFormValues.searchValue !== this.props.match.params.searchValue) {
      this.props.push(`/search/name/${searchFormValues.searchValue}`);
    }
  }

  searchSongs(searchValue) {
    this.props.clearSongs();
    this.props.changeFieldValue('search', 'searchValue', searchValue);
    this.props.requestSongs({ query: `name~${searchValue}` });
  }

  render() {
    return (
      <SongNameSearch
        onSearch={this.onSearch}
      />
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    requestSongs,
    clearSongs,
    push,
    changeFieldValue,
  }, dispatch);
}

SongNameSearchContainer.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
  requestSongs: PropTypes.func.isRequired,
  clearSongs: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  changeFieldValue: PropTypes.func.isRequired,
};

export default connect(null, matchDispatchToProps)(SongNameSearchContainer);
