import React from 'react';

import SongSearchMenu from '../SongSearchMenu';

test('it renders', () => {
  const songSearchMenu = shallow(
    <SongSearchMenu />,
  );

  expect(songSearchMenu).toMatchSnapshot();
});