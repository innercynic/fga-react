import React from 'react';

import TuningSVG from '../Tuning';

test('it renders', () => {
  const tuningSVG = shallow(
    <TuningSVG />,
  );

  expect(tuningSVG).toMatchSnapshot();
});