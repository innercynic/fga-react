import React from 'react';

import SongSVG from '../Song';

test('it renders', () => {
  const songSVG = shallow(
    <SongSVG />,
  );

  expect(songSVG).toMatchSnapshot();
});