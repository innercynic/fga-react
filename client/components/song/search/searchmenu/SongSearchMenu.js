import React from 'react';
import { Flex, Box } from 'grid-styled';

import TuningSVG from './svgs/Tuning';
import SongSVG from './svgs/Song';
import { Container, LinkWithoutUnderline, Heading, Button } from '../../../general';

import MenuItem from '../../../menu/main/MenuItem';

const SongSearchMenu = props => (
  <Container justify="center" align="center" wrap noBackground>
    <MenuItem flex="0 0 250px" column m={20}>
      <LinkWithoutUnderline to="/search/name">
        <Flex p={[3, 3, 4, 4]} column align="center">
          <Flex mb={20} flex={1}>
            <SongSVG />
          </Flex>
          <Heading>Song Name</Heading>
        </Flex>
      </LinkWithoutUnderline>
    </MenuItem>
    <MenuItem flex="0 0 250px" column m={20}>
      <LinkWithoutUnderline to="/search/tuning">

        <Flex p={[3, 3, 4, 4]} column align="center">
          <Flex mb={20} flex={1}>
            <TuningSVG />
          </Flex>
          <Heading>Song Tuning</Heading>
        </Flex>
      </LinkWithoutUnderline>
    </MenuItem>
  </Container>
);

export default SongSearchMenu;
