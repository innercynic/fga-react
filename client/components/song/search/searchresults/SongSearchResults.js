import React from 'react';
import { Flex, Box } from 'grid-styled';
import PropTypes from 'prop-types';

import { SongShape } from '../../../../proptypes';
import { Container, LoaderWithOverlay, Button } from '../../../general';

import List from '../../../list/List';
import SongGridItem from '../../list/SongGridItem';

export const SongSearchResults = props => (
  <Container flex="1" column noBackground width={1} minHeight={50}>

    <LoaderWithOverlay fullscreen isLoading={props.isLoading} />

    <Flex flex="1" justify="center" column>
      <Box width={1} flex={1}>
        <List
          items={props.songs}
          onItemClick={props.onSongClick}
          itemKey="_id"
          getItemContent={song => <SongGridItem song={song} />}
          itemBreakPoints={[1, 1, 1 / 2, 1 / 3, 1 / 4, 1 / 5]}
          displayAsGrid
        />
      </Box>

      {(props.hasMorePages && !props.isLoading) &&
        <Flex justify="center" my={[1, 1, 2, 3]}>
          <Button onClick={props.loadMoreSongs}>Load more songs</Button>
        </Flex>
      }

    </Flex>

  </Container>
);

SongSearchResults.defaultProps = {
  songs: [],
};

SongSearchResults.propTypes = {
  songs: PropTypes.arrayOf(SongShape),
  onSongClick: PropTypes.func.isRequired,
  loadMoreSongs: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  hasMorePages: PropTypes.bool.isRequired,
};

export default SongSearchResults;
