import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { change as changeFieldValue } from 'redux-form';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';

import SongSearchResults from './SongSearchResults';
import { selectSong } from '../../../../actions/songs/song';
import { requestSongs } from '../../../../actions/songs/songs';

export class SongSearchResultsContainer extends Component {
  constructor(props) {
    super(props);

    this.onSongClick = this.onSongClick.bind(this);
    this.loadMoreSongs = this.loadMoreSongs.bind(this);
  }

  onSongClick(song) {
    this.props.selectSong(song);
    this.props.push(`/songs/${song._id}`);
  }

  loadMoreSongs() {
    this.props.requestSongs({ query: this.props.currentQuery, page: this.props.nextPage });
  }

  render() {
    return (
      <SongSearchResults
        songs={this.props.songs}
        onSongClick={this.onSongClick}
        loadMoreSongs={this.loadMoreSongs}
        isLoading={this.props.isLoading}
        hasMorePages={this.props.hasMorePages}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    songs: state.songs.data,
    isLoading: state.songs.isFetching,
    nextPage: state.songs.nextPage,
    hasMorePages: state.songs.hasMore,
    currentQuery: state.songs.currentQuery,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    requestSongs,
    selectSong,
    push,
  }, dispatch);
}

SongSearchResultsContainer.defaultProps = { currentQuery: '' };

SongSearchResultsContainer.propTypes = {
  requestSongs: PropTypes.func.isRequired,
  selectSong: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  nextPage: PropTypes.number.isRequired,
  currentQuery: PropTypes.string,
};

export default connect(mapStateToProps, matchDispatchToProps)(SongSearchResultsContainer);
