import React from 'react';

import MockData from '../../../../../utils/test/MockData';
import { SongSearchResultsContainer } from '../SongSearchResultsContainer';

let props;

beforeEach(() => {
  props = {
    requestSongs: jest.fn(),
    selectSong: jest.fn(),
    push: jest.fn(),
    songs: [],
    isLoading: false,
    nextPage: 1,
    hasMorePages: false,
  };
})

test('it renders', () => {
  const songSearchResultsContainer = shallow(
    <SongSearchResultsContainer {...props} />,
  ); 

  expect(songSearchResultsContainer).toMatchSnapshot();
});

test('it selects song and changes url when child calls onSongClick', () => {
  const songSearchResultsContainer = shallow(
    <SongSearchResultsContainer {...props} />
  );

  const mockSong = MockData.mockSongs[0];
  songSearchResultsContainer.instance().onSongClick(mockSong);

  expect(props.selectSong).toHaveBeenCalledWith(mockSong);
  expect(props.push).toHaveBeenCalledWith(`/songs/${mockSong._id}`);
});

test('it calls request songs with next page config when load more songs is clicked', () => {
  const songSearchResultsContainer = shallow(
    <SongSearchResultsContainer {...props} nextPage={2} currentQuery="name~mocksearch" />
  );

  songSearchResultsContainer.instance().loadMoreSongs();

  expect(props.requestSongs)
  .toHaveBeenCalledWith({"query": "name~mocksearch", page:2});
});