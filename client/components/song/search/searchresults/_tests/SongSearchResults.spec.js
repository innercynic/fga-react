import React from 'react';

import SongSearchResults from '../SongSearchResults';
import { Button } from '../../../../general';

let props;

beforeEach(() => {
  props = {
    songs: [],
    onSongClick: jest.fn(),
    loadMoreSongs: jest.fn(),
    isLoading: false,
    hasMorePages: false,
  };
})

test('it renders', () => {
  const songSearchResults = shallow(
    <SongSearchResults {...props} />,
  );

  expect(songSearchResults).toMatchSnapshot();
});


test('it should show load more results button when there are more pages of results', () => {
  let propsWithMorePages = { ...props, hasMorePages: true };

  const songSearchResults = shallow(
    <SongSearchResults {...propsWithMorePages} />
  );

  let loadMoreSongsButton = songSearchResults.find(Button);
  expect(loadMoreSongsButton).toHaveLength(1);
});


test('it should call loadMoreSongs prop when click load more button', () => {
  let propsWithMorePages = { ...props, hasMorePages: true };

  const songSearchResults = shallow(
    <SongSearchResults {...propsWithMorePages} />
  );

  songSearchResults.find(Button).simulate('click');
  expect(props.loadMoreSongs).toHaveBeenCalled();
});

