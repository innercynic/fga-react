import React from 'react';
import { FieldArray, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import { Container, Button } from '../../../general';
import { Label } from '../../../form/fields';
import { RFSelect } from '../../../form/reduxformfields';
import StringSelector from '../../../stringselector/StringSelector';

export const TuningSelectForm = props => (
  <form onSubmit={props.handleSubmit}>
    <Container column p={[2, 2, 3, 3]} mb={10}>
      <FieldArray name="stringtunings" component={StringSelector} />
      <Box my={[1, 1, 2, 3]}>
        <Box mb={5}>
          <Label htmlFor="similaritylevel">Tuning difference maximum</Label>
        </Box>
        <RFSelect name="similaritylevel">
          <option value="0">Same Tuning</option>
          <option value="1">1 Semitone</option>
          <option value="2">2 Semitones</option>
          <option value="3">3 Semitones</option>
          <option value="4">4 Semitones</option>
          <option value="5">5 Semitones</option>
          <option value="6">6 Semitones</option>
        </RFSelect>
      </Box>

      <Box my={[1, 1, 2, 3]}>
        <Button type="submit" width="100%">Search</Button>
      </Box>
    </Container>
  </form>
);

export default reduxForm({ form: 'tuningsearch', destroyOnUnmount: false })(TuningSelectForm);
