import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { change as changeFieldValue } from 'redux-form';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';

import SongTuningSearch from './SongTuningSearch';
import { requestSongs, clearSongs } from '../../../../actions/songs/songs';

export class SongTuningSearchContainer extends Component {
  constructor(props) {
    super(props);

    this.onSearch = this.onSearch.bind(this);
    this.searchSongs = this.searchSongs.bind(this);
    this.updateFormValues = this.updateFormValues.bind(this);
    this.searchParamsAreValid = this.searchParamsAreValid.bind(this);
  }

  componentWillMount() {
    const { tuning, similaritylevel } = this.props.match.params;

    this.updateFormValues(tuning, similaritylevel);
    this.searchSongs(tuning, similaritylevel);
  }

  componentWillUpdate(nextProps) {
    const { tuning, similaritylevel } = this.props.match.params;
    const { tuning: nextTuning, similaritylevel: nextSimilaritylevel } = nextProps.match.params;

    if (nextTuning !== tuning || nextSimilaritylevel !== similaritylevel) {
      this.updateFormValues(nextTuning, nextSimilaritylevel);
      this.searchSongs(nextTuning, nextSimilaritylevel);
    }
  }

  componentWillUnmount() {
    this.props.clearSongs();
  }

  onSearch({ stringtunings, similaritylevel }) {
    if (!this.props.match) return;
    this.props.push(`/search/tuning/${stringtunings.join('')}/${similaritylevel}`);
  }

  searchSongs(tuning, similaritylevel) {
    if (!this.props.match) return;

    if (this.searchParamsAreValid(tuning, similaritylevel)) {
      this.props.clearSongs();
      this.props.requestSongs({
        query: `tuning=${tuning},similaritylevel=${similaritylevel}`,
      });
    }
  }

  searchParamsAreValid(tuning, similaritylevel) {
    if (typeof tuning === 'undefined' || typeof similaritylevel === 'undefined') return false;
    if (tuning.match(/[ABCDEFG]#?/g).length !== 6) return false;
    const levelNumber = parseInt(similaritylevel);
    if (isNaN(levelNumber)) return false;
    if (levelNumber > 6 || levelNumber < 0) return false;

    return true;
  }

  updateFormValues(tuning = 'EADGBE', similaritylevel = 0) {
    if (this.searchParamsAreValid(tuning, similaritylevel)) {
      const stringtunings = tuning.match(/[ABCDEFG]#?/g);

      stringtunings.map((stringtuning, index) => {
        this.props.changeFieldValue('tuningsearch', `stringtunings[${index}]`, stringtuning);
      });

      this.props.changeFieldValue('tuningsearch', 'similaritylevel', similaritylevel);
    }
  }

  render() {
    return (
      <SongTuningSearch
        onSearch={this.onSearch}
      />
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    requestSongs,
    clearSongs,
    push,
    changeFieldValue,
  }, dispatch);
}

SongTuningSearchContainer.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
  requestSongs: PropTypes.func.isRequired,
  clearSongs: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  changeFieldValue: PropTypes.func.isRequired,
};

export default connect(null, matchDispatchToProps)(SongTuningSearchContainer);
