import React from 'react';
import { Flex, Box } from 'grid-styled';
import PropTypes from 'prop-types';

import { SongShape } from '../../../../proptypes';
import SearchForm from '../../../search/SearchForm';
import { Container, LoaderWithOverlay, Button } from '../../../general';

import List from '../../../list/List';
import SongGridItem from '../../list/SongGridItem';
import SongSearchResultsContainer from '../searchresults/SongSearchResultsContainer';
import TuningSelectForm from './TuningSelectForm';

export const SongTuningSearch = props => (
  <Container flex="1" column noBackground width={1}>

    <Flex justify="center" mb={20}>
      <TuningSelectForm onSubmit={props.onSearch} />
    </Flex>

    <SongSearchResultsContainer />

  </Container>
);

SongTuningSearch.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default SongTuningSearch;
