import React from 'react';

import { SongTuningSearchContainer } from '../SongTuningSearchContainer';

let props, propsWithSearchValue;

beforeEach(() => {
  props = {
    requestSongs: jest.fn(),
    clearSongs: jest.fn(),
    push: jest.fn(),
    changeFieldValue: jest.fn(),
    match: {
      path: "",
      url: "",
      params: {},
    },
  };

  propsWithSearchValue = {
    ...props,
    match: {
      ...props.match,
      params: {
        tuning: "EADGBE",
        similaritylevel: 0,
      }
    }
  };
})

test('it renders', () => {
  const songTuningSearchContainer = shallow(
    <SongTuningSearchContainer {...props} />,
  );

  expect(songTuningSearchContainer).toMatchSnapshot();
});

test('it fires initial search when route contains params', () => {
  const songTuningSearchContainer = shallow(
    <SongTuningSearchContainer {...propsWithSearchValue} />
  );

  expect(propsWithSearchValue.requestSongs).toHaveBeenCalled();
});

test('it fires search when route is updated to contain params', () => {
  const songTuningSearchContainer = shallow(
    <SongTuningSearchContainer {...props} />,
    { lifecycleExperimental: true }
  );

  expect(props.requestSongs).not.toHaveBeenCalled();

  songTuningSearchContainer.setProps({match: propsWithSearchValue.match});

  expect(props.requestSongs).toHaveBeenCalled();
});

test('it clears search results on unmount', () => {
  const songTuningSearchContainer = shallow(
    <SongTuningSearchContainer {...propsWithSearchValue} />
  );

  songTuningSearchContainer.unmount();

  expect(propsWithSearchValue.clearSongs).toHaveBeenCalled();
});

test('it updates url on search )', () => {
  const songTuningSearchContainer = shallow(
    <SongTuningSearchContainer {...props} />
  );

  songTuningSearchContainer.instance().onSearch({
    stringtunings: ["D", "A", "D", "G", "B", "E"],
    similaritylevel: 0
  });

  expect(propsWithSearchValue.push).toHaveBeenCalled();
});

test('it updates form field value when searching songs', () => {
  const songTuningSearchContainer = shallow(
    <SongTuningSearchContainer {...props} />
  );

  songTuningSearchContainer.instance().searchSongs({
    stringtunings: ["D", "A", "D", "G", "B", "E"],
    similaritylevel: 0
  });

  expect(propsWithSearchValue.changeFieldValue).toHaveBeenCalled();
});

test('it verifys search params are valid', () => {
  const songTuningSearchContainer = shallow(
    <SongTuningSearchContainer {...propsWithSearchValue} />
  );

  const validTuning = "DADGBE";
  const validTuning2 = "DADG#BE";
  const invalidTuning = "DADGBX";
  const invalidTuning2 = "DADGBEE";
  const validSimlilarityLevel = 3;
  const invalidSimlilarityLevel = 8;
  const invalidSimlilarityLevel2 = "five";

  let isValid = songTuningSearchContainer.instance().searchParamsAreValid(validTuning, validSimlilarityLevel);
  expect(isValid).toBeTruthy();

  isValid = songTuningSearchContainer.instance().searchParamsAreValid(validTuning2, validSimlilarityLevel);
  expect(isValid).toBeTruthy();
  
  isValid = songTuningSearchContainer.instance().searchParamsAreValid(invalidTuning, validSimlilarityLevel);
  expect(isValid).toBeFalsy();

  isValid = songTuningSearchContainer.instance().searchParamsAreValid(invalidTuning2, validSimlilarityLevel);
  expect(isValid).toBeFalsy();

  isValid = songTuningSearchContainer.instance().searchParamsAreValid(validTuning, invalidSimlilarityLevel);
  expect(isValid).toBeFalsy();

  isValid = songTuningSearchContainer.instance().searchParamsAreValid(validTuning, invalidSimlilarityLevel2);
  expect(isValid).toBeFalsy();

  isValid = songTuningSearchContainer.instance().searchParamsAreValid(undefined, undefined);
  expect(isValid).toBeFalsy();
});