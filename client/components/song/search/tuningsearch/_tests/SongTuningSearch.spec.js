import React from 'react';

import SongTuningSearch from '../SongTuningSearch';

let props;

beforeEach(() => {
  props = {
    onSearch: jest.fn(),
  };
})

test('it renders', () => {
  const songTuningSearch = shallow(
    <SongTuningSearch {...props} />,
  );

  expect(songTuningSearch).toMatchSnapshot();
});