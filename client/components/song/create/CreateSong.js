import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';

import { LoaderWithOverlay } from '../../general';

import CreateSongPageContainer from './CreateSongPageContainer';
import CreateSongPageOne from './CreateSongPageOne';
import CreateSongPageTwo from './CreateSongPageTwo';

export class CreateSong extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.state = { page: 1 };
  }

  nextPage() {
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 });
  }

  render() {
    return (
      <Flex column width={1}>
        <LoaderWithOverlay isLoading={this.props.isPostingData} />

        <Flex width={1}>

          {this.state.page === 1 &&
          <CreateSongPageContainer
            submitButtonText="Next"
            onSubmit={this.nextPage}
          >
            <CreateSongPageOne />
          </CreateSongPageContainer>}

          {this.state.page === 2 &&
          <CreateSongPageContainer
            onBack={this.previousPage}
            submitButtonText="Submit"
            onSubmit={this.props.onSubmit}
          >
            <CreateSongPageTwo />
          </CreateSongPageContainer>}

        </Flex>

      </Flex>
    );
  }
}

CreateSong.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isPostingData: PropTypes.bool.isRequired,
};

export default CreateSong;
