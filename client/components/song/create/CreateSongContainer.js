import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { reset, initialize } from 'redux-form';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';
import { Flex, Box } from 'grid-styled';

import { ArtistShape, SongShape } from '../../../proptypes';

import { createSong, clearSelectedSong } from '../../../actions/songs/song';
import { requestArtist, clearSelectedArtist } from '../../../actions/artists/artist';

import CreateSong from './CreateSong';
import OverlayModal from '../../modals/OverlayModal';
import { Container, Loader, Text, Media, ImgAsBackground, Heading, Button, HTTPErrorMessage } from '../../general';

export class CreateSongContainer extends Component {
  constructor(props) {
    super(props);

    this.onCreateSong = this.onCreateSong.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.redirectToCreateSong = this.redirectToCreateSong.bind(this);
    this.redirectToSongView = this.redirectToSongView.bind(this);
  }

  componentWillMount() {
    if (!this.props.match) return;

    this.props.clearSelectedSong();

    const artistId = this.props.match.params.id;
    this.props.initialize('song', { artist: artistId });

    if (!this.props.selectedArtist || this.props.selectedArtist._id !== artistId) {
      this.props.clearSelectedArtist();
      this.props.requestArtist({ id: artistId });
    }
  }

  onCreateSong(songData) {
    this.props.createSong(songData);
  }

  get isShowingHttpError() {
    return !this.props.isFetchingArtist
    && !this.props.selectedArtist
    && this.props.errorStatusCode;
  }

  redirectToCreateSong() {
    this.props.push('/addsong');
    this.closeModal();
  }

  redirectToSongView() {
    this.props.push(`/songs/${this.props.newSong._id}`);
    this.closeModal();
  }

  closeModal() {
    this.props.reset('song');
    // this.props.clearSelectedSong();
  }

  render() {
    return (
      <Container column flex={1} width={1} maxWidth={600} p={[3, 3, 4, 4]}>
        <Flex>
          {this.props.isFetchingArtist &&
          <Loader isLoading={this.props.isFetchingArtist} />
          }

          {this.isShowingHttpError &&
            <HTTPErrorMessage
              statusCode={this.props.errorStatusCode}
              messages={{
                404: 'Could not find artist with this id!',
                400: 'The artist id provided in the url has an invalid format.',
              }}
            />
          }

          {this.props.selectedArtist &&
          <Flex flex="1" justify="center" align="center" mb={[1, 1, 2, 3]}>
            <Media imageUrl={this.props.selectedArtist.image}>
              <Text size="large">Song By {this.props.selectedArtist.name}</Text>
            </Media>
          </Flex>
          }

        </Flex>

        {this.props.selectedArtist &&
        <CreateSong
          isPostingData={this.props.isPostingData}
          onArtistNotFound={this.onArtistNotFound}
          onSubmit={this.onCreateSong}
        />
        }
        <OverlayModal isOpen={!!this.props.newSong} onRequestClose={this.closeModal} maxWidth={400}>
          {this.props.newSong &&
            <Flex column p={[3, 3, 4, 4]} align="center">
              <Box my={5}>
                <Heading>Song created</Heading>
              </Box>
              <Box my={5}>
                <ImgAsBackground
                  asCircle
                  width={80}
                  height={80}
                  url={this.props.selectedArtist.thumbnail}
                />
              </Box>
              <Box my={5}>
                <Text>{this.props.newSong.name} by {this.props.selectedArtist.name}</Text>
              </Box>
              <Flex my={5}>
                <Box px={5}>
                  <Button
                    id="create-another"
                    width="100%"
                    onClick={this.redirectToCreateSong}
                  >
                    Create Another
                  </Button>
                </Box>
                <Box px={5}>
                  <Button
                    id="view-song"
                    width="100%"
                    onClick={this.redirectToSongView}
                  >
                    View Song
                  </Button>
                </Box>
              </Flex>
            </Flex>
          }
        </OverlayModal>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isPostingData: state.song.isPostingData,
    selectedArtist: state.artist.selected,
    isFetchingArtist: state.artist.isFetching,
    errorStatusCode: state.artist.errorStatusCode,
    newSong: state.song.selected,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    createSong,
    clearSelectedSong,
    requestArtist,
    clearSelectedArtist,
    push,
    reset,
    initialize,
  }, dispatch);
}

CreateSongContainer.defaultProps = {
  selectedArtist: null,
  newSong: null,
  match: null,
  errorStatusCode: null,
};

CreateSongContainer.propTypes = {
  createSong: PropTypes.func.isRequired,
  clearSelectedSong: PropTypes.func.isRequired,
  requestArtist: PropTypes.func.isRequired,
  clearSelectedArtist: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  initialize: PropTypes.func.isRequired,
  isPostingData: PropTypes.bool.isRequired,
  isFetchingArtist: PropTypes.bool.isRequired,
  errorStatusCode: PropTypes.number,
  selectedArtist: ArtistShape,
  newSong: SongShape,
  match: ReactRouterPropTypes.match,
};

export default connect(mapStateToProps, matchDispatchToProps)(CreateSongContainer);
