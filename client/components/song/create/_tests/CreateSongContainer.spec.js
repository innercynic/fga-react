import React from 'react';

import MockData from '../../../../utils/test/MockData';
import { CreateSongContainer } from '../CreateSongContainer';
import { HTTPErrorMessage, Loader } from '../../../general';
import CreateSong from '../CreateSong';


let props;

beforeEach(() => {
  props = {
    createSong: jest.fn(),
    clearSelectedSong: jest.fn(),
    requestArtist: jest.fn(),
    clearSelectedArtist: jest.fn(),
    push: jest.fn(),
    reset: jest.fn(),
    initialize: jest.fn(),
    isPostingData: false,
    isFetchingArtist: false,
    match: {
      params: {
        id: MockData.mockArtists[0]._id,
      },
      path: "",
      url: "",
    },
  };
})

test('it renders', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} />,
  );

  expect(createSongContainer).toMatchSnapshot();
});

test('it initializes the song form with artist id when component mounts', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} />,
    { lifecycleExperimental: true }
  );

  expect(props.initialize).toHaveBeenCalledWith("song", {"artist": props.match.params.id});
});

test('it requests the artist if not passed as prop', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} />,
    { lifecycleExperimental: true }
  );

  expect(props.requestArtist).toHaveBeenCalledWith({id: MockData.mockArtists[0]._id});
});

test('it requests the artist if artist passed as prop has different id to path param', () => {
  const artistWithDifferntId = {
    ...MockData.mockArtists[0],
    _id: 123,
  };

  const createSongContainer = shallow(
    <CreateSongContainer {...props} selectedArtist={artistWithDifferntId}/>,
    { lifecycleExperimental: true }
  );

  expect(props.requestArtist).toHaveBeenCalledWith({id: MockData.mockArtists[0]._id});
});

test('it shows Loader component if isFetchingArtist', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} isFetchingArtist={true} />,
  );

  expect(createSongContainer.find(Loader)).toHaveLength(1);
});

test('it shows http error if is no artist or request in process for one, and errorStatusCode is available', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} isFetchingArtist={false} errorStatusCode={404} />,
  );

  expect(createSongContainer.find(HTTPErrorMessage)).toHaveLength(1);
});

test('it shows CreateSong component if artist is selected', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} selectedArtist={MockData.mockArtists[0]} />,
  );

  expect(createSongContainer.find(CreateSong)).toHaveLength(1);
});

test('it shows CreateSong and artist Media components if artist is selected', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} selectedArtist={MockData.mockArtists[0]} />,
  );

  expect(createSongContainer).toMatchSnapshot();
});

test('it shows modal with song details if new song was successfully created', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} selectedArtist={MockData.mockArtists[0]} newSong={MockData.mockSongs[0]} />,
  );

  expect(createSongContainer).toMatchSnapshot();
});

test('it redirects to song view when clicking View Song button after song is created', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} selectedArtist={MockData.mockArtists[0]} newSong={MockData.mockSongs[0]} />,
  );

  createSongContainer.find('#view-song').simulate('click');

  expect(props.push).toHaveBeenCalledWith(`/songs/${MockData.mockSongs[0]._id}`);
});

test('it redirects to create new song view when clicking Create Another button after song is created', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} selectedArtist={MockData.mockArtists[0]} newSong={MockData.mockSongs[0]} />,
  );

  createSongContainer.find('#create-another').simulate('click');

  expect(props.push).toHaveBeenCalledWith("/addsong");
});

test('it resets song form and clears new song when new artist modal is closed', () => {
  const createSongContainer = shallow(
    <CreateSongContainer {...props} selectedArtist={MockData.mockArtists[0]} newSong={MockData.mockSongs[0]} />,
  );

  createSongContainer.instance().closeModal();

  expect(props.reset).toHaveBeenCalledWith("song");
  expect(props.clearSelectedSong).toHaveBeenCalled();
});





