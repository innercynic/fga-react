import React from 'react';

import CreateSongPageTwo from '../CreateSongPageTwo';

test('it renders', () => {
  const createSongPageTwo = shallow(
    <CreateSongPageTwo />,
  );

  expect(createSongPageTwo).toMatchSnapshot();
});