import React from 'react'
import 'jest-styled-components'

import TestUtils from '../../../../utils/test/TestUtils';
import { CreateSong } from '../CreateSong';
import CreateSongPageOne from '../CreateSongPageOne';
import CreateSongPageTwo from '../CreateSongPageTwo';

let props;

beforeEach(() => {
  props = {
    onSubmit: jest.fn(),
    isPostingData: false,
  };
})

test('it renders', () => {
  const createSong = shallow(
    <CreateSong {...props} />
  );

  expect(createSong).toMatchSnapshot();
});


test('it renders CreateSongPageOne when current page is 1', () => {
  const createSong = shallow(
    <CreateSong {...props} />
  );

  expect(createSong.find(CreateSongPageOne)).toHaveLength(1);
});

test('it renders CreateSongPageTwo when current page is 2', () => {
  const createSong = shallow(
    <CreateSong {...props} />
  );
  createSong.instance().setState({page:2});
  
  expect(createSong.find(CreateSongPageTwo)).toHaveLength(1);
});

test('it should change page', () => {
  const createSong = shallow(
    <CreateSong {...props} />
  );

  createSong.instance().nextPage();
  expect(createSong.instance().state.page).toEqual(2);
  createSong.instance().nextPage();
  expect(createSong.instance().state.page).toEqual(3);
  createSong.instance().previousPage();
  expect(createSong.instance().state.page).toEqual(2);
});