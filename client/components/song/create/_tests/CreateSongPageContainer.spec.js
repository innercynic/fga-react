import React from 'react';

import { CreateSongPageContainer } from '../CreateSongPageContainer';

let props;

beforeEach(() => {
  props = {
    children: [],
    handleSubmit: jest.fn(),
    submitButtonText: "Submit",
  };
})

test('it renders', () => {
  const createSongPageContainer = shallow(
    <CreateSongPageContainer {...props} />,
  );

  expect(createSongPageContainer).toMatchSnapshot();
});

test('it renders its children', () => {
  const createSongPageContainer = shallow(
    <CreateSongPageContainer {...props}>
      <div id="child" />
    </CreateSongPageContainer>
  );

  expect(createSongPageContainer.find('#child')).toHaveLength(1);
});


test('it shows back button when onBack function prop is passed', () => {
  const mockOnBack = jest.fn();
  const createSongPageContainer = shallow(
    <CreateSongPageContainer {...props} onBack={mockOnBack} />,
  );

  expect(createSongPageContainer.find('#create-song-page-container-back')).toHaveLength(1);
});

test('it calls onBack function prop when Back button is clicked', () => {
  const mockOnBack = jest.fn();
  const createSongPageContainer = shallow(
    <CreateSongPageContainer {...props} onBack={mockOnBack} />,
  );

  createSongPageContainer.find('#create-song-page-container-back').simulate('click');

  expect(mockOnBack).toHaveBeenCalled();
});


test('it calls prop when form is submitted', () => {
  const createSongPageContainer = shallow(
    <CreateSongPageContainer {...props} />,
  );

  createSongPageContainer.find('form').props().onSubmit();

  expect(props.handleSubmit).toHaveBeenCalled();
});