import React from 'react';

import CreateSongPageOne from '../CreateSongPageOne';

test('it renders', () => {
  const createSongPageOne = shallow(
    <CreateSongPageOne />,
  );

  expect(createSongPageOne).toMatchSnapshot();
});