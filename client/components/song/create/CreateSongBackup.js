import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import { Heading, LoaderWithOverlay } from '../../general';

import validate from '../../../validation/song';

import CreateSongPageContainer from './CreateSongPageContainer';
import CreateSongPageOne from './CreateSongPageOne';
import CreateSongPageTwo from './CreateSongPageTwo';
import CreateSongPageThree from './CreateSongPageThree';
import CreateArtistContainer from '../../artist/create/CreateArtistContainer';

export class CreateSong extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.state = { page: 1 };
  }

  nextPage() {
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 });
  }

  render() {
    return (
      <Flex column width={1}>
        <LoaderWithOverlay isLoading={this.props.isPostingData} />

        <Box m={[1, 1, 2, 3]}>
          <Heading>Create Song</Heading>
        </Box>

        <Flex width={1}>
          {this.state.page === 1 &&
            <CreateSongPageContainer submitButtonText="Next" onSubmit={this.nextPage}>
              <CreateSongPageOne />
            </CreateSongPageContainer>}
          {
            this.state.page === 2 &&
            <Box>
              <CreateSongPageContainer
                onBack={this.previousPage}
                submitButtonText="Next"
                onSubmit={this.nextPage}
              >
                <CreateSongPageTwo onArtistNotFound={this.props.onArtistNotFound} />
              </CreateSongPageContainer>

              <CreateArtistContainer />
            </Box>
          }
          {this.state.page === 3 &&
            <CreateSongPageContainer
              onBack={this.previousPage}
              submitButtonText="Submit"
              onSubmit={this.props.onSubmit}
            >
              <CreateSongPageThree />
            </CreateSongPageContainer>}
        </Flex>

      </Flex>
    );
  }
}

CreateSong.defaultProps = {
  onArtistNotFound() {},
};

CreateSong.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onArtistNotFound: PropTypes.func,
  isPostingData: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'song',
  validate,
})(CreateSong);
