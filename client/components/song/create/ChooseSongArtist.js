import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Flex } from 'grid-styled';
import PropTypes from 'prop-types';

import { ArtistShape } from '../../../proptypes';
import ChooseOrCreateArtist from '../../artist/find/ChooseOrCreateArtist';
import { Container, Text, Media } from '../../general';


class ChooseSongArtist extends Component {
  constructor(props) {
    super(props);

    this.loadCreateSongPage = this.loadCreateSongPage.bind(this);
  }

  loadCreateSongPage(data) {
    this.props.push(`/addsong/${data._id}`);
  }

  render() {
    return (
      <Container column flex={1} maxWidth={600} p={[3, 3, 4, 4]}>
        <ChooseOrCreateArtist onArtistChosen={this.loadCreateSongPage} />

        {this.props.selectedArtist &&
        <Flex flex="1" justify="center" align="center" mt={[1, 1, 2, 3]}>
          <Media imageUrl={this.props.selectedArtist.image}>
            <Text size="large">Song By {this.props.selectedArtist.name}</Text>
          </Media>
        </Flex>
        }
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  selectedArtist: state.artist.selected,
});

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ push }, dispatch);
}

ChooseSongArtist.defaultProps = {
  selectedArtist: null,
};

ChooseSongArtist.propTypes = {
  push: PropTypes.func.isRequired,
  selectedArtist: ArtistShape,
};

export default connect(mapStateToProps, matchDispatchToProps)(ChooseSongArtist);
