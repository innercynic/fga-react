import React from 'react';
import { Box } from 'grid-styled';

import { Label } from '../../form/fields';
import { RFInput, RFFieldError } from '../../form/reduxformfields';

const CreateSongPageOne = () => (
  <Box flex="1">
    <Box mb={10}>
      <Box mb={5}><Label htmlFor="name">Name</Label></Box>
      <RFFieldError name="name" />
      <RFInput name="name" />
    </Box>

    <Box mb={10}>
      <Box mb={5}><Label htmlFor="album">Album</Label></Box>
      <RFFieldError name="album" />
      <RFInput name="album" />
    </Box>

    <Box mb={10}>
      <Box mb={5}><Label htmlFor="video">Video</Label></Box>
      <RFInput name="video" />
    </Box>

    <Box mb={10}>
      <Box mb={5}><Label htmlFor="tags">Techniques</Label></Box>
      <RFInput name="tags" />
    </Box>
  </Box>
);

export default CreateSongPageOne;
