import React from 'react';
import { Flex, Box } from 'grid-styled';
import { FieldArray } from 'redux-form';

import { Label } from '../../form/fields';
import { RFInput, RFCheckbox } from '../../form/reduxformfields';

import StringSelector from '../../stringselector/StringSelector';

const CreateSongPageTwo = () => (
  <Box flex="1">
    <Flex>
      <Box mb={10} width={3 / 4}>
        <Box mb={5}><Label htmlFor="tab">Tab</Label></Box>
        <RFInput name="tab" />
      </Box>
      <Flex width={1 / 4} align="center" justify="center" pt={20}>
        <RFCheckbox name="paid" label="Paid" />
      </Flex>
    </Flex>

    <Label htmlFor="name">Tuning</Label>
    <Flex justify="center">
      <FieldArray name="stringtunings" component={StringSelector} />
    </Flex>

  </Box>
);

export default CreateSongPageTwo;
