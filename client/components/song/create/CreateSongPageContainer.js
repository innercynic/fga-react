import React from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import { Button } from '../../general';
import validate from '../../../validation/song';

export const CreateSongPageContainer = ({ children, handleSubmit, submitButtonText, onBack }) => (
  <Box width={1}>
    <form onSubmit={handleSubmit}>
      <Flex>
        {children}
      </Flex>

      {onBack &&
        <Box my={[1, 1, 2, 3]}>
          <Button id="create-song-page-container-back" onClick={onBack} width="100%">Back</Button>
        </Box>}

      <Box my={[1, 1, 2, 3]}>
        <Button type="submit" width="100%">{submitButtonText}</Button>
      </Box>
    </form>
  </Box>
);

CreateSongPageContainer.defaultProps = {
  onBack: false,
};

CreateSongPageContainer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  onBack: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};

export default reduxForm({
  form: 'song',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
})(CreateSongPageContainer);
