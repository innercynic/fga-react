import React from 'react';
import { Box } from 'grid-styled';

import { SongShape } from '../../../proptypes';
import { Heading, Text, TextItalics, Media } from '../../general';
import { songTuningToString } from '../helpers';

const SongGridItem = ({ song }) => (
  <Box my={[1, 1, 2, 3]} mx={[0, 1, 2, 3]}>
    <Media clickable imageUrl={song.artist.thumbnail}>
      <Heading>{song.name}</Heading>
      <Text>{song.album}</Text>
      <TextItalics>By {song.artist.name}</TextItalics>
      <Text>{songTuningToString(song.stringtunings)}</Text>
    </Media>
  </Box>
);

SongGridItem.propTypes = {
  song: SongShape.isRequired,
};

export default SongGridItem;

