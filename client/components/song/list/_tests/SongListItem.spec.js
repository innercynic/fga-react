import React from 'react';

import MockData from '../../../../utils/test/MockData';
import SongListItem from '../SongListItem';

let props;

beforeEach(() => {
  props = {
    song: MockData.mockSongs[0],
  };
})

test('it renders', () => {
  const songListItem = shallow(
    <SongListItem {...props} />,
  );

  expect(songListItem).toMatchSnapshot();
});