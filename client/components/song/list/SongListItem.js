import React from 'react';
import { Flex } from 'grid-styled';

import { SongShape } from '../../../proptypes';
import { Text, TextItalics, ImgAsBackground, Container } from '../../general';

const SongListItem = ({ song }) => (
  <Container forContentItem clickable mb={10} p={5}>
    <Flex ml={5} mr={15}>
      <ImgAsBackground url={song.artist.thumbnail} width={50} height={50} asCircle />
    </Flex>
    <Flex column justify="center">
      <Text>{song.name}</Text>
      <TextItalics size="tiny">{song.artist.name}</TextItalics>
    </Flex>
  </Container>
);

SongListItem.propTypes = {
  song: SongShape.isRequired,
};

export default SongListItem;

