import { sizes } from '../../styles/media';

export function songTuningToString(stringTunings) {
  return stringTunings.join('');
}

export function breakpointIsLessThan(screensizeName) {
  if (window.innerWidth < sizes[screensizeName]) return true;
  return false;
}

export function breakpointIsAtLeast(screensizeName) {
  if (window.innerWidth >= sizes[screensizeName]) return true;
  return false;
}

export function heightIsLessThan(height) {
  if (window.innerHeight <= height) return true;
  return false;
}

export function heightIsAtLeast(height) {
  if (window.innerHeight >= height) return true;
  return false;
}

