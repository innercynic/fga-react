import React from 'react';

import { SongViewContainer } from '../SongViewContainer';
import MockData from '../../../../utils/test/MockData';

let props;

beforeEach(() => {
  props = {
    isFetchingSong: false,
    match: {
      params: {
        id: MockData.mockSongs[0]._id,
      },
      path: "",
      url: "",
    },
    requestSong: jest.fn(),
    clearSelectedSong: jest.fn(),
  };
});

test('it renders only loader with default props', () => {
  const songViewContainer = shallow(
    <SongViewContainer {...props} />,
  );

  expect(songViewContainer).toMatchSnapshot();
});

test('it renders songView when passing a song', () => {
  const songViewContainer = shallow(
    <SongViewContainer {...props} selectedSong={MockData.mockSongs[0]} />,
  );

  expect(songViewContainer).toMatchSnapshot();
});

test('it requests the song if not passed as prop', () => {
  const songViewContainer = shallow(
    <SongViewContainer {...props} />,
    { lifecycleExperimental: true }
  );

  expect(props.requestSong).toHaveBeenCalled();
});

test('it requests the song if song passed as prop has different id to path param', () => {
  const songWithDifferntId = {
    ...MockData.mockSongs[0],
    _id: 123,
  };

  const songViewContainer = shallow(
    <SongViewContainer {...props} selectedSong={songWithDifferntId}/>,
    { lifecycleExperimental: true }
  );

  expect(props.requestSong).toHaveBeenCalled();
});