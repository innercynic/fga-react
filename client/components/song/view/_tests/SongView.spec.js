import React from 'react'
import 'jest-styled-components'

import TestUtils from '../../../../utils/test/TestUtils';
import MockData from '../../../../utils/test/MockData';
import SongView from '../SongView';

test('it renders', () => {
  const wrappedSongView = shallow(
    <SongView song={MockData.mockSongs[0]} />
  );

  expect(wrappedSongView.find(SongView)).toMatchSnapshot();
});