import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';

import { SongShape } from '../../../proptypes';
import { requestSong, clearSelectedSong } from '../../../actions/songs/song';

import SongView from './SongView';
import { LoaderWithOverlay, FlexExtended } from '../../general';

export class SongViewContainer extends Component {
  componentWillMount() {
    if (!this.props.match) return;

    const songId = this.props.match.params.id;

    if (!this.props.selectedSong || this.props.selectedSong._id !== songId) {
      this.props.clearSelectedSong();
      this.props.requestSong({ id: songId });
    }
  }

  render() {
    if (this.props.isFetchingSong || !this.props.selectedSong) {
      return (
        <FlexExtended height={250}>
          <LoaderWithOverlay isLoading={this.props.isFetchingSong} />
        </FlexExtended>
      );
    }
    return (
      <SongView song={this.props.selectedSong} />
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedSong: state.song.selected,
    isFetchingSong: state.song.isFetching,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ requestSong, clearSelectedSong }, dispatch);
}

SongViewContainer.defaultProps = {
  selectedSong: null,
};

SongViewContainer.propTypes = {
  selectedSong: SongShape,
  isFetchingSong: PropTypes.bool.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
  requestSong: PropTypes.func.isRequired,
  clearSelectedSong: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, matchDispatchToProps)(SongViewContainer);
