import React from 'react';
import { Flex, Box } from 'grid-styled';

import { SongShape } from '../../../proptypes';
import { songTuningToString } from '../helpers';
import { Heading, Text, TextItalics, Container, ImgAsBackground } from '../../general';

const SongView = ({ song }) => (
  <Container flex="1" maxWidth={600} wrap justify="center" align="center" p={[3, 3, 4, 4]}>
    <Flex column flex="1 1 180px" align="center">
      <Heading>{song.name}</Heading>
      <Text>{song.album}</Text>
      <Text>{songTuningToString(song.stringtunings)}</Text>
    </Flex>

    <Flex column flex="1 1 180px" align="center">
      <Box mb={5}>
        <ImgAsBackground asCircle width={100} height={100} url={song.artist.thumbnail} />
      </Box>
      <TextItalics>By {song.artist.name}</TextItalics>
    </Flex>

  </Container>
);

SongView.propTypes = { song: SongShape.isRequired };

export default SongView;

