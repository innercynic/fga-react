import styled from 'styled-components';
import PropTypes from 'prop-types';

import { clickable } from '../../../styles/mixins';

const Button = styled(styled.button``).attrs({
  border: props => (!props.noBorder),
  size: props => (props.size ? props.size : 'normal'),
  width: props => (props.width ? props.width : 'auto'),
  padding: (props) => {
    switch (props.size) {
      case 'large':
        return '1rem';
      case 'small':
        return '0.4rem';
      default:
        return '0.6rem';
    }
  },
  fontSize: (props) => {
    switch (props.size) {
      case 'large':
        return props.theme.fonts.sizes.text.large;
      case 'small':
        return props.theme.fonts.sizes.text.tiny;
      default:
        return props.theme.fonts.sizes.text.small;
    }
  },
})`
  border: ${props => (props.border ? `${props.theme.borders.sizes.fine} solid ${props.theme.borders.colors.normal}` : 0)};
  border-radius: ${props => props.theme.borders.radiuses.small}; 
  width: ${props => props.width};
  padding: ${props => props.padding};
  font-size: ${props => props.fontSize};
  background: ${props => props.theme.backgrounds.input.normal};
  color: ${props => props.theme.fonts.colors.input.normal};
  
  ${clickable}

  &:focus {
    background-color: ${props => props.theme.backgrounds.input.focus};
    color: ${props => props.theme.fonts.colors.input.focus};
    outline: none;
  }
`;

Button.defaultProps = {
  noBorder: false,
  size: null,
  width: null,
};

Button.proptypes = {
  noBorder: PropTypes.bool,
  size: PropTypes.string,
  width: PropTypes.string,
};

export { Button };
