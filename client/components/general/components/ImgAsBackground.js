import styled from 'styled-components';
import PropTypes from 'prop-types';

import { clickable } from '../../../styles/mixins';

const ImgAsBackground = styled.div`
  ${props => (props.clickable ? clickable : '')}
  
  height: ${props => (props.height ? `${props.height}px` : '100%')};
  width: ${props => (props.width ? `${props.width}px` : '100%')};
  border-radius: ${props => (props.asCircle ? '50%' : 0)};
  background: url('${props => props.url}') center no-repeat;
  background-size: cover;
`;

ImgAsBackground.defaultProps = {
  height: null,
  width: null,
  asCircle: false,
  clickable: false,
};

ImgAsBackground.propTypes = {
  url: PropTypes.string.isRequired,
  height: PropTypes.number,
  width: PropTypes.number,
  asCircle: PropTypes.bool,
  clickable: PropTypes.bool,
};

export { ImgAsBackground };
