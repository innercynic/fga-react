import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const LinkWithoutUnderline = styled(Link)`
  text-decoration: none;
`;

LinkWithoutUnderline.propTypes = {
  to: PropTypes.string.isRequired,
};

export { LinkWithoutUnderline };
