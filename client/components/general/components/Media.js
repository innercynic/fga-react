import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';

import { Container, ImgAsBackground } from '../../general';

const Media = props => (
  <Container
    forContentItem
    flex={1}
    clickable={props.clickable}
    overflow="hidden"
    height={100}
    align="center"
    justify="center"
  >

    <Flex width={'100px'} flex={1} px={10} align="center" justify="center" column>
      {props.children}
    </Flex>

    <Flex flex={'0 0 100px'} ml={10} align="center" justify="flex-end">
      <ImgAsBackground width={100} height={100} url={props.imageUrl} />
    </Flex>

  </Container>
);

Media.defaultProps = {
  clickable: false,
};

Media.propTypes = {
  imageUrl: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  clickable: PropTypes.bool,
};

export { Media };
