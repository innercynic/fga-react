import React from 'react';
import ReactLoader from 'react-loader';
import { withTheme } from 'styled-components';
import PropTypes from 'prop-types';

const normalOptions = {
  lines: 9,
  length: 0,
  width: 32,
  radius: 36,
  scale: 0.6,
  corners: 1,
  color: '#000',
  opacity: 0.35,
  rotate: 39,
  direction: 1,
  speed: 1,
  trail: 60,
  fps: 40,
};

const smallOptions = {
  ...normalOptions,
  width: 8,
  radius: 23,
  scale: 0.32,
};

const Loader = withTheme(({ isLoading, theme, size }) => {
  const options = { ...(size === 'small' ? smallOptions : normalOptions), color: theme.fonts.colors.input.normal };

  return <ReactLoader loaded={!isLoading} options={options} />;
});

Loader.defaultProps = {
  size: 'normal',
};

Loader.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  size: PropTypes.string,
};


export { Loader };
