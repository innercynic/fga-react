import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import { Icon, TextItalics } from '../../general';

const HTTPErrorMessage = ({ messages, statusCode }) => (
  <Flex flex="1" justify="center" align="center">
    <Box mr={10}>
      <Icon icon="error" />
    </Box>
    <Box mr={10}>
      <TextItalics>
        {messages[statusCode] || 'Unknown error'}
      </TextItalics>
    </Box>
  </Flex>
);

HTTPErrorMessage.propTypes = {
  messages: PropTypes.objectOf(PropTypes.string).isRequired,
  statusCode: PropTypes.number.isRequired,
};

export { HTTPErrorMessage };
