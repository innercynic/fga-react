import styled from 'styled-components';
import PropTypes from 'prop-types';

import { getIcons } from '../../../styles/icons';
import { clickable } from '../../../styles/mixins';

const Icon = styled.span`
  ${props => (props.clickable ? clickable : '')}

  color: ${props => props.theme.fonts.colors.input.normal};

  &:before {
    font-family: icons;
    font-size: ${props => (props.theme.fonts.sizes.text[props.size]
    ? props.theme.fonts.sizes.text[props.size]
    : props.theme.fonts.sizes.text.normal)};
    content: "${props => (getIcons()[props.icon] ? getIcons()[props.icon] : '')}";
  }
`;

Icon.defaultProps = {
  size: 'normal',
  clickable: false,
};

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  size: PropTypes.string,
  clickable: PropTypes.bool,
};

export { Icon };
