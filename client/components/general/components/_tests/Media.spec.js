import React from 'react';

import { Media } from '../Media';

let props;

beforeEach(() => {
  props = {
    imageUrl: "/mockImage.jpg",
    children: <div id="child">CHILD</div>,
  };
})

test('it renders', () => {
  const media = shallow(
    <Media {...props} />,
  );

  expect(media.find('#child')).toHaveLength(1);
  expect(media).toMatchSnapshot();
});
