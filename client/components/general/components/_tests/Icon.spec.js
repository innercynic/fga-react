import React from 'react';
import 'jest-styled-components';
import sinon from 'sinon';

import MockData from '../../../../utils/test/MockData';
import { Icon } from '../Icon';

import * as Icons from '../../../../styles/icons';

sinon.stub(Icons, 'getIcons').callsFake(() => {
  return {mockIconName: 'mockIconCode'};
});

let props;

beforeEach(() => {
  props = {
    icon: "mockIconName",
  };
})

test('it renders', () => {
  const icon = shallow(
    <Icon {...props} theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(icon).toMatchSnapshot();

  expect(icon).toHaveStyleRule('content', '\"mockIconCode\"', { modifier: ':before' });
});

test('it renders as clickable', () => {
  const icon = shallow(
    <Icon {...props} clickable theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(icon).toMatchSnapshot();
});
