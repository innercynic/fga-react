import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { HTTPErrorMessage } from '../HTTPErrorMessage';

let props;

beforeEach(() => {
  props = {
    messages: {
      404: "Mock resource not found error",
      400: 'Mock bad request error',
    },
    statusCode: 404,
  };
})

test('it renders', () => {
  const httpErrorMessage = shallow(
    <HTTPErrorMessage {...props} theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(httpErrorMessage).toMatchSnapshot();
});

test('it displays "Unknown Error" if no message matches status code', () => {
  const httpErrorMessage = shallow(
    <HTTPErrorMessage {...props} statusCode={500} theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(httpErrorMessage).toMatchSnapshot();
});

