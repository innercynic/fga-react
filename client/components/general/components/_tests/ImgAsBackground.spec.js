import React from 'react'
import 'jest-styled-components'

import MockData from '../../../../utils/test/MockData';
import { ImgAsBackground } from '../ImgAsBackground';

let props;

beforeEach(() => {
  props = {
    url: "/image.jpg",
  };
});

test('it renders', () => {
  const imgAsBackground = shallow(
   <ImgAsBackground {...props} theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(imgAsBackground)
  .toHaveStyleRule('background', 'url(\'/image.jpg\') center no-repeat');
  expect(imgAsBackground).toMatchSnapshot();
});

test('it renders as clickable', () => {
  const imgAsBackground = shallow(
   <ImgAsBackground {...props} clickable theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(imgAsBackground).toMatchSnapshot();
});

test('it renders as circle (borders 50%)', () => {
  const imgAsBackground = shallow(
   <ImgAsBackground {...props} asCircle theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(imgAsBackground)
  .toHaveStyleRule('border-radius', '50%');
});

test('it renders with give width and height', () => {
  const imgAsBackground = shallow(
   <ImgAsBackground {...props} width={10} height={10} theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(imgAsBackground)
  .toHaveStyleRule('height', '10px');

  expect(imgAsBackground)
  .toHaveStyleRule('width', '10px');
});