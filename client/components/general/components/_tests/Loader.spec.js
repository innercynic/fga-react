import React from 'react';
import * as StyledComponents from 'styled-components';
import sinon from 'sinon';

import TestUtils from '../../../../utils/test/TestUtils';
import MockData from '../../../../utils/test/MockData';
import { Loader } from '../Loader';

let props;

beforeEach(() => {
  props = {
    isLoading: false,
  };
})


let ThemeProviderWrapper = TestUtils.getMockThemeProvider();


test('it renders', () => {
  const loader = mount(
    <ThemeProviderWrapper>
      <Loader {...props} />
    </ThemeProviderWrapper>,
  );

  expect(loader).toMatchSnapshot();
});

test('it renders with small options and is loading set to true', () => {
  const loader = mount(
    <ThemeProviderWrapper>
      <Loader {...props} size="small" isLoading />
    </ThemeProviderWrapper>,
  );

  expect(loader).toMatchSnapshot();
});