import React from 'react'
import 'jest-styled-components'

import MockData from '../../../../utils/test/MockData';
import { LoaderWithOverlay, LoaderOverlay } from '../LoaderWithOverlay';


test('it renders', () => {
  const loaderWithOverlay = shallow(
    <LoaderWithOverlay isLoading={true} />
  );

  expect(loaderWithOverlay).toMatchSnapshot();
});

test('its overlay renders', () => {
  const loaderOverlay = shallow(
    <LoaderOverlay isLoading={true} theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(loaderOverlay).toMatchSnapshot();
});

test('its overlay is hidden when is loading is false', () => {
  const loaderOverlay = shallow(
    <LoaderOverlay isLoading={false} theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(loaderOverlay).toHaveStyleRule('display', 'none');
});

test('its overlay renders full screen', () => {
  const loaderOverlay = shallow(
    <LoaderOverlay isLoading={true} fullscreen theme={MockData.mockThemes[MockData.mockThemeName]}/>
  );

 expect(loaderOverlay).toHaveStyleRule('position', 'fixed');
});
