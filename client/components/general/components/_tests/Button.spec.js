import React from 'react'
import 'jest-styled-components'

import MockData from '../../../../utils/test/MockData';
import { Button } from '../Button';

test('it renders', () => {
  const button = shallow(
    <Button theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(button).toMatchSnapshot();
});

test('it renders without border', () => {
  const button = shallow(
    <Button noBorder theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(button).toHaveStyleRule('border', '0');
});

test('it renders using a size prop', () => {
  const button = shallow(
    <Button size="large" theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(button).toHaveStyleRule('padding', '1rem');
});

test('it renders using a width prop', () => {
  const button = shallow(
    <Button width="100px" theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(button).toHaveStyleRule('width', '100px');
});