import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { LinkWithoutUnderline } from '../LinkWithoutUnderline';

test('it renders', () => {
  const linkWithoutUnderline = shallow(
    <LinkWithoutUnderline to="/mockroute" theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(linkWithoutUnderline).toMatchSnapshot();
});