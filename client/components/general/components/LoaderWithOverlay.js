import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Loader } from './Loader';

const LoaderOverlay = styled.div `
  position: ${props => (props.fullscreen ? 'fixed' : 'absolute')};
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background: ${props => props.theme.backgrounds.mainOverlay};
  border-radius: ${props => props.theme.borders.radiuses.normal};
  display: ${props => (props.isLoading ? 'inherit' : 'none')};
`;

LoaderOverlay.defaultProps = {
  fullscreen: false,
};

LoaderOverlay.propTypes = {
  fullscreen: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
};

const LoaderWithOverlay = ({ isLoading, loaderSize = 'normal', fullscreen }) => (
  <LoaderOverlay fullscreen={fullscreen} isLoading={isLoading}>
    <Loader size={loaderSize} isLoading={isLoading} />
  </LoaderOverlay>
);

LoaderWithOverlay.defaultProps = {
  fullscreen: false,
  loaderSize: 'normal',
};

LoaderWithOverlay.propTypes = {
  fullscreen: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
  loaderSize: PropTypes.string,
};

export { LoaderWithOverlay };
export { LoaderOverlay };
