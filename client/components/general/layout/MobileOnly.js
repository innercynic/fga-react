import { Flex } from 'grid-styled';
import media from '../../../styles/media';

const MobileOnly = Flex.extend`
  display: none;
  ${media.phone`
    width: 135px;
    height: 210px;
  `}  
`;

export { MobileOnly };
