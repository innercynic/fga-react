import { Flex } from 'grid-styled';
import PropTypes from 'prop-types';

import { clickable } from '../../../styles/mixins';

const FlexExtended = Flex.extend`
  height: ${props => (props.height !== 'auto' ? `${props.height}px` : props.height)};
  position: ${props => props.position};
  ${props => (props.clickable ? clickable : '')}
`;

FlexExtended.defaultProps = {
  height: 'auto',
  position: 'static',
  clickable: false,
};

FlexExtended.propTypes = {
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  position: PropTypes.string,
  clickable: PropTypes.bool,
};

export { FlexExtended };
