import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';

import { clickable } from '../../../styles/mixins';

const Container = Flex.extend.attrs({
  bg: (props) => {
    if (props.noBackground) return 'transparent';
    if (props.forContentItem) return props.theme.backgrounds.container_alternate;
    return props.theme.backgrounds.container;
  },
})`
  background: ${props => props.bg};
  border-radius: ${props => props.theme.borders.radiuses.normal};
  height: ${props => (props.height !== 'auto' ? `${props.height}px` : props.height)};

  max-width: ${props => (props.maxWidth !== 'none' ? `${props.maxWidth}px` : props.maxWidth)};
  max-height: ${props => (props.maxHeight !== 'none' ? `${props.maxHeight}px` : props.maxHeight)};
  min-width: ${props => (props.minWidth !== 'auto' ? `${props.minWidth}px` : props.minWidth)};
  min-height: ${props => (props.minHeight !== 'auto' ? `${props.minHeight}px` : props.minHeight)};

  vertical-align: middle;
  position: relative;
  overflow: ${props => props.overflow};
  cursor: ${props => (props.pointer ? 'pointer' : 'auto')};

  ${props => (props.clickable ? clickable : '')}
`;

Container.defaultProps = {
  pointer: false,
  noBackground: false,
  height: 'auto',
  overflow: 'visible',
  maxWidth: 'none',
  maxHeight: 'none',
  minWidth: 'auto',
  minHeight: 'auto',
};

Container.propTypes = {
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  maxWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  maxHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  minWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  minHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  noBackground: PropTypes.bool,
  pointer: PropTypes.bool,
  overflow: PropTypes.string,
};

export { Container };
