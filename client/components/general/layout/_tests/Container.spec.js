import React from 'react'
import 'jest-styled-components'

import MockData from '../../../../utils/test/MockData';
import { Container } from '../Container';


test('it renders', () => {
  const container = shallow(
    <Container theme={MockData.mockThemes[MockData.mockThemeName]} />
  );

  expect(container).toMatchSnapshot();
});

test('it renders with transparent background', () => {
  const container = shallow(
    <Container noBackground theme={MockData.mockThemes[MockData.mockThemeName]} />
  );


  expect(container).toHaveStyleRule('background', 'transparent');
});

test('it renders using given height prop', () => {
  const container = shallow(
    <Container height={10} theme={MockData.mockThemes[MockData.mockThemeName]} />
  );


  expect(container).toHaveStyleRule('height', '10px');
});

test('it renders using given max width prop', () => {
  const container = shallow(
    <Container maxWidth={10} theme={MockData.mockThemes[MockData.mockThemeName]} />
  );


  expect(container).toHaveStyleRule('max-width', '10px');
});

test('it renders using given overflow prop', () => {
  const container = shallow(
    <Container overflow="hidden" theme={MockData.mockThemes[MockData.mockThemeName]} />
  );


  expect(container).toHaveStyleRule('overflow', 'hidden');
});

test('it renders using pointer prop to change cursor style rule to "pointer"', () => {
  const container = shallow(
    <Container pointer theme={MockData.mockThemes[MockData.mockThemeName]} />
  );


  expect(container).toHaveStyleRule('cursor', 'pointer');
});