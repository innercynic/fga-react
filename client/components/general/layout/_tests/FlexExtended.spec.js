import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { FlexExtended } from '../FlexExtended';

test('it renders', () => {
  const flexExtended = shallow(
    <FlexExtended theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(flexExtended).toMatchSnapshot();
});

test('it renders with clickable mixin', () => {
  const flexExtended = shallow(
    <FlexExtended clickable theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(flexExtended).toMatchSnapshot();
});

test('it renders with given position prop', () => {
  const flexExtended = shallow(
    <FlexExtended position="relative" theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(flexExtended).toHaveStyleRule('position', 'relative');
});

test('it renders with given height prop', () => {
  const flexExtended = shallow(
    <FlexExtended height={200} theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(flexExtended).toHaveStyleRule('height', '200px');
});



