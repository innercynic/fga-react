import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { MobileOnly } from '../MobileOnly';

test('it renders', () => {
  const mobileOnly = shallow(
    <MobileOnly theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(mobileOnly).toMatchSnapshot();
});
