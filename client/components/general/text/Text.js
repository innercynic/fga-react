import styled from 'styled-components';
import { Box } from 'grid-styled';

export const Text = styled(Box).attrs({
  textSize: props => ((props.size && props.theme.fonts.sizes.text[props.size]) ? props.size : 'normal'),
})`
  font-family: ${props => props.theme.fonts.family.text};
  font-size: ${props => props.theme.fonts.sizes.text[props.textSize]};
  color: ${props => props.theme.fonts.colors.text};
`;
