import React from 'react';
import 'jest-styled-components'

import MockData from '../../../../utils/test/MockData';
import { Text } from '../Text';

const sizes = MockData.mockThemes[MockData.mockThemeName].fonts.sizes.text;

test('it renders', () => {
  const text = shallow(
    <Text theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(text).toMatchSnapshot();
});

test('it changes text based on size prop', () => {
  const text = shallow(
    <Text size="large" theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(text).toHaveStyleRule('font-size', sizes.large);
});

test('it defaults to "normal" size if size prop is unrecognised', () => {
  const text = shallow(
    <Text size="giant" theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(text).toHaveStyleRule('font-size', sizes.normal);
});