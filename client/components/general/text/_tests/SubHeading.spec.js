import React from 'react';

import MockData from '../../../../utils/test/MockData';
import { SubHeading } from '../SubHeading';

test('it renders', () => {
  const subHeading = shallow(
    <SubHeading theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(subHeading).toMatchSnapshot();
});