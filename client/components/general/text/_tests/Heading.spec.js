import React from 'react';

import MockData from '../../../../utils/test/MockData';
import { Heading } from '../Heading';

test('it renders', () => {
  const heading = shallow(
    <Heading theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(heading).toMatchSnapshot();
});