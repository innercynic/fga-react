import React from 'react';

import MockData from '../../../../utils/test/MockData';
import { TextItalics } from '../TextItalics';

test('it renders', () => {
  const textItalics = shallow(
    <TextItalics theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(textItalics).toMatchSnapshot();
});