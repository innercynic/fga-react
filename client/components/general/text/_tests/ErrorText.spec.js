import React from 'react';
import 'jest-styled-components';

import MockData from '../../../../utils/test/MockData';
import { ErrorText } from '../ErrorText';

test('it renders', () => {
  const errorText = shallow(
    <ErrorText theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(errorText).toMatchSnapshot();
});
