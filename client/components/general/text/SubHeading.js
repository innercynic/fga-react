import styled from 'styled-components';
import { Box } from 'grid-styled';

export const SubHeading = styled(Box)`
  font-family: ${props => props.theme.fonts.family.subheading};
  font-size: ${props => props.theme.fonts.sizes.subheading};
  color: ${props => props.theme.fonts.colors.subheading};
`;
