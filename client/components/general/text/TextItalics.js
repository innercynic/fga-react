import { Text } from './Text';

export const TextItalics = Text.extend`
  font-style: italic;
`;
