import styled from 'styled-components';

export const ErrorText = styled.span`
  color: ${props => props.theme.fonts.colors.error};
  font-style: italic;
  font-size: ${props => props.theme.fonts.sizes.text.tiny};
  margin-left: 7px;
`;
