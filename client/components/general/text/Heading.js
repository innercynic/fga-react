import styled from 'styled-components';
import { Box } from 'grid-styled';

export const Heading = styled(Box)`
  font-family: ${props => props.theme.fonts.family.heading};
  font-size: ${props => props.theme.fonts.sizes.heading};
  color: ${props => props.theme.fonts.colors.heading};
`;
