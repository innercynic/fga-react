import React from 'react';

import { CreateArtist } from '../CreateArtist';

let props;

beforeEach(() => {
  props = {
    handleSubmit: jest.fn(),
    isPostingData: false,
    imageValue: {preview:''},
  };
});

test('it renders', () => {
  const createArtist = shallow(
    <CreateArtist {...props} />,
  );

  expect(createArtist).toMatchSnapshot();
});

test('it renders with name field set to read only', () => {
  const createArtist = shallow(
    <CreateArtist {...props} nameIsReadOnly />,
  );

  let nameFieldProps = createArtist.find('#name-field').props();


  expect(nameFieldProps.readOnly).toBeTruthy();
});


test('it calls prop when form is submitted', () => {
  const createArtist = shallow(
    <CreateArtist {...props} />
  );

  createArtist.find('form').props().onSubmit();

  expect(props.handleSubmit).toHaveBeenCalled();
});