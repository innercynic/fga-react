import React from 'react';

import MockData from '../../../../utils/test/MockData';
import { CreateArtistContainer } from '../CreateArtistContainer';

let mockNewArtist = MockData.mockArtists[0];
let props;

beforeEach(() => {
  props = {
    onArtistCreated: jest.fn(),
    createArtist: jest.fn(),
    isPostingData: false,
  };
});


test('it renders', () => {
  const createArtistContainer = shallow(
    <CreateArtistContainer {...props} />,
  );

  expect(createArtistContainer).toMatchSnapshot();
});