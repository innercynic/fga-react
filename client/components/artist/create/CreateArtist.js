import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import { Heading, Button, LoaderWithOverlay } from '../../general';
import { Label } from '../../form/fields';
import { RFInput, RFTextArea, RFFieldError, RFImageDropzone } from '../../form/reduxformfields';

import validate from '../../../validation/artist';

export const CreateArtist = props => (
  <Box width="100%" flex="1 1 auto">
    <form onSubmit={props.handleSubmit}>
      <LoaderWithOverlay isLoading={props.isPostingData} />

      <Box>
        <Heading>Create Artist</Heading>
      </Box>

      <Flex wrap>
        <Box flex="1 1 250px" width={[1, 1, 1 / 2]} mb={5}>
          <Label htmlFor="image">Profile Picture</Label>
          <RFFieldError name="image" />
          <RFImageDropzone imageValue={props.imageValue} name="image" />
        </Box>

        <Box flex="1 1 250px" width={[1, 1, 1 / 2]} mb={5}>
          <Label htmlFor="name">Name</Label>
          <RFFieldError name="name" />
          <RFInput id="name-field" name="name" readOnly={props.nameIsReadOnly} />

          <Label htmlFor="info">Info</Label>
          <RFTextArea name="info" />
        </Box>
      </Flex>

      <Flex>
        <Button type="submit" width="100%">Submit</Button>
      </Flex>

    </form>
  </Box>
);

CreateArtist.defaultProps = {
  nameIsReadOnly: false,
  imageValue: { preview: '' },
};

CreateArtist.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isPostingData: PropTypes.bool.isRequired,
  nameIsReadOnly: PropTypes.bool,
  imageValue: PropTypes.shape({
    preview: PropTypes.string,
  }),
};

const selector = formValueSelector('artist');

export default connect(state => ({ imageValue: selector(state, 'image') }))(reduxForm({
  form: 'artist',
  validate,
})(CreateArtist));
