import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { createArtist } from '../../../actions/artists/artist';

import CreateArtist from './CreateArtist';

export const CreateArtistContainer = props => (
  <CreateArtist
    isPostingData={props.isPostingData}
    onSubmit={props.createArtist}
    initialValues={props.initialValues}
    nameIsReadOnly={props.nameIsReadOnly}
  />
);

function mapStateToProps(state) {
  return {
    isPostingData: state.artist.isPostingData,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ createArtist }, dispatch);
}

CreateArtistContainer.defaultProps = {
  initialValues: {},
  nameIsReadOnly: false,
};

CreateArtistContainer.propTypes = {
  createArtist: PropTypes.func.isRequired,
  isPostingData: PropTypes.bool.isRequired,
  initialValues: PropTypes.objectOf(PropTypes.string),
  nameIsReadOnly: PropTypes.bool,
};

export default connect(mapStateToProps, matchDispatchToProps)(CreateArtistContainer);
