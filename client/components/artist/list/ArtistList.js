import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import renderIf from 'render-if';
import PropTypes from 'prop-types';

import List from '../list/List';
import { ArtistShape } from '../../proptypes';
import { requestArtists } from '../../actions/artists/artists';
import { Button } from '../general';

class ArtistList extends Component {
  static getListItemContent(item) {
    return (
      <div>
        <div>{item.name}</div>
      </div>
    );
  }

  constructor(props) {
    super(props);

    this.loadMoreArtists = this.loadMoreArtists.bind(this);
    this.getListItemContent = this.getListItemContent.bind(this);
  }

  componentDidMount() {
    if (this.props.artists.length === 0) {
      this.props.requestArtists(1, 'name');
    }
  }

  loadMoreArtists() {
    this.props.requestArtists(this.props.nextPage, 'name');
  }

  render() {
    return (
      <div>
        {renderIf(this.props.artists)(() => (
          <List
            items={this.props.artists}
            itemKey="_id"
            onItemClick={this.props.selectArtist}
            selectedItem={this.props.selectedArtist}
            getItemContent={this.getListItemContent}
          />
        ))}

        {renderIf(this.props.nextPage !== -1)(() => (
          <Button onClick={this.loadMoreArtists}>Load more artists</Button>
        ))}

        {renderIf(this.props.isLoading)(() => (
          <div>Loading artists...</div>
        ))}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    artists: state.artists.data,
    isLoading: state.artists.isFetching,
    nextPage: state.artists.nextPage,
    selectedArtist: state.artist.selected,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ requestArtists }, dispatch);
}

ArtistList.defaultProps = {
  nextPage: 1,
  selectedArtist: null,
};

ArtistList.propTypes = {
  selectArtist: PropTypes.func.isRequired,
  requestArtists: PropTypes.func.isRequired,
  artists: PropTypes.arrayOf(ArtistShape).isRequired,
  isLoading: PropTypes.bool.isRequired,
  nextPage: PropTypes.number,
  selectedArtist: PropTypes.shape(ArtistShape),
};

export default connect(mapStateToProps, matchDispatchToProps)(ArtistList);
