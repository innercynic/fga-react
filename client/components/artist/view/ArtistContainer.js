import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import renderIf from 'render-if';

import Artist from './Artist';
import { requestArtist } from '../../actions/artist';
import { ArtistShape } from '../../proptypes';

class ArtistContainer extends Component {
  componentDidMount() {
    this.props.requestArtist({ id: this.props.match.params.artist });
  }

  render() {
    return (
      <div>
        {renderIf(this.props.selectedArtist)(() => (
          <Artist {...this.props.selectedArtist} />
        ))}

        {renderIf(this.props.isLoading)(() => (
          <div>Loading artist...</div>
        ))}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.artist.isFetching,
    selectedArtist: state.artist.selected,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ requestArtist }, dispatch);
}

ArtistContainer.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  selectedArtist: ArtistShape.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
  requestArtist: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, matchDispatchToProps)(ArtistContainer);
