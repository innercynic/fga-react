import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { Flex, Box } from 'grid-styled';

import ArtistContainer from './ArtistContainer';
import ArtistList from './ArtistList';
import { Container } from '../general';

class ArtistView extends Component {
  constructor(props) {
    super(props);

    this.selectArtist = this.selectArtist.bind(this);
  }

  selectArtist(item) {
    console.log(item);
    this.props.history.push(`/songdatabase/${item._id}`);// eslint-disable-line no-underscore-dangle
  }

  render() {
    return (
      <Container width={600}>
        <Flex>
          <Box width={2 / 10}>
            <ArtistList selectArtist={this.selectArtist} />
          </Box>
          <Flex width={8 / 10}>
            <Route path={`${this.props.match.url}/:artist`} component={ArtistContainer} />
          </Flex>
        </Flex>
      </Container>
    );
  }
}

ArtistView.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
  history: ReactRouterPropTypes.history.isRequired,
};

export default ArtistView;
