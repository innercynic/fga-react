import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';
import { ImgAsBackground, SubHeading, Text } from '../../general';

const Artist = ({ name, info, image }) => (
  <Flex align="center">
    <SubHeading>{name}</SubHeading>
    <Flex m={10} justify="flex-end">
      <ImgAsBackground asCircle width={80} height={80} url={image} />
    </Flex>
  </Flex>
);

Artist.propTypes = {
  name: PropTypes.string.isRequired,
  info: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default Artist;
