import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import { selectArtist, clearSelectedArtist, setIsAddingArtist } from '../../../actions/artists/artist';
import { ArtistShape } from '../../../proptypes';

import FindArtist from './FindArtist';
import CreateArtistContainer from '../create/CreateArtistContainer';

import { Heading, Button, Text } from '../../general';
import OverlayModal from '../../modals/OverlayModal';

export class ChooseOrCreateArtist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      isModalOpen: false,
    };

    this.onArtistInputChange = this.onArtistInputChange.bind(this);
    this.setInputValue = this.setInputValue.bind(this);

    this.showAddArtistForm = this.showAddArtistForm.bind(this);
    this.hideAddArtistForm = this.hideAddArtistForm.bind(this);

    this.onSubmit = this.onSubmit.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillMount() {
    if (this.props.selectedArtist) {
      this.props.clearSelectedArtist();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedArtist) {
      this.hideAddArtistForm();
    }
  }

  onArtistInputChange(value) {
    this.setInputValue(value);
    this.props.clearSelectedArtist();
  }

  onSubmit(data) {
    if (!data || !data._id) {
      if (this.state.inputValue !== '') {
        this.openModal();
      }
    } else {
      this.props.onArtistChosen(data);
    }
  }

  get initialValues() {
    return {
      name: this.state.inputValue,
    };
  }

  setInputValue(value) {
    this.setState({ inputValue: value });
  }

  showAddArtistForm() {
    this.closeModal();
    this.props.setIsAddingArtist(true);
  }

  hideAddArtistForm() {
    this.props.setIsAddingArtist(false);
    this.setInputValue('');
  }

  openModal() {
    this.setState({
      isModalOpen: true,
    });
  }

  closeModal() {
    this.setState({
      isModalOpen: false,
    });
  }

  render() {
    return (
      <Flex column flex={1}>
        {!this.props.isAddingArtist &&
        <Flex column flex={1}>
          <Box mb={[1, 1, 2, 3]}>
            <Heading>Choose Artist</Heading>
          </Box>
          <FindArtist
            onSubmit={this.onSubmit}
            onSelectionMade={this.props.selectArtist}
            onArtistInputChange={this.onArtistInputChange}
          />
        </Flex>
        }

        {this.props.isAddingArtist &&
        <Flex column flex={1}>
          <Box mb={[1]} flex={1}>
            <CreateArtistContainer
              initialValues={this.initialValues}
              onArtistCreated={this.onArtistCreated}
              nameIsReadOnly
            />
          </Box>
          <Button id="artist-form-back" onClick={this.hideAddArtistForm} width="100%">Back</Button>
        </Flex>
        }

        <OverlayModal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.closeModal}
          maxWidth={400}
        >
          <Flex column m={20}>
            <Heading>Arist not found</Heading>
            <Text>We could not find &apos;{this.state.inputValue}&apos; in the database,
            have you typed the name correctly?</Text>
            <Text>Use the button below to create this artist now.</Text>
            <Box width={1} my={5}>
              <Button id="create-artist" width="100%" onClick={this.showAddArtistForm}>
                Create Artist
              </Button>
            </Box>
            <Box width={1} my={5}>
              <Button id="modal-close" width="100%" onClick={this.closeModal}>
                Close
              </Button>
            </Box>

          </Flex>
        </OverlayModal>

      </Flex>
    );
  }
}

function mapStateToProps(state) {
  return {
    isPostingData: state.artist.isPostingData,
    selectedArtist: state.artist.selected,
    isAddingArtist: state.artist.isAddingArtist,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    selectArtist,
    clearSelectedArtist,
    setIsAddingArtist,
  }, dispatch);
}

ChooseOrCreateArtist.defaultProps = {
  selectedArtist: null,
};

ChooseOrCreateArtist.propTypes = {
  selectArtist: PropTypes.func.isRequired,
  setIsAddingArtist: PropTypes.func.isRequired,
  isAddingArtist: PropTypes.bool.isRequired,
  clearSelectedArtist: PropTypes.func.isRequired,
  selectedArtist: ArtistShape,
  onArtistChosen: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, matchDispatchToProps)(ChooseOrCreateArtist);
