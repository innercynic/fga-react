import React from 'react';

import MockData from '../../../../utils/test/MockData';
import CreateArtistContainer from '../../create/CreateArtistContainer';
import FindArtist from '../FindArtist';

import { ChooseOrCreateArtist } from '../ChooseOrCreateArtist';

let props;
const mockArtist = MockData.mockArtists[0];

beforeEach(() => {
  props = {
    isAddingArtist: false,
    clearSelectedArtist: jest.fn(),
    selectArtist: jest.fn(),
    setIsAddingArtist: jest.fn(),
    clearCreatedArtist: jest.fn(),
    onArtistChosen: jest.fn(),
  };
});

test('it renders find artist form by default', () => {
  const chooseOrCreateArtist = shallow(
    <ChooseOrCreateArtist {...props} />,
  );

  expect(chooseOrCreateArtist.find(FindArtist)).toHaveLength(1);
  expect(chooseOrCreateArtist).toMatchSnapshot();
});

test('it renders create artist form and hides find artist when adding artist', () => {
  const chooseOrCreateArtist = shallow(
    <ChooseOrCreateArtist {...props} isAddingArtist />,
  );

  expect(chooseOrCreateArtist.find(FindArtist)).toHaveLength(0);
  expect(chooseOrCreateArtist.find(CreateArtistContainer)).toHaveLength(1);

});

test('it clears selected artist when mounting', () => {
  const chooseOrCreateArtist = shallow(
    <ChooseOrCreateArtist {...props} selectedArtist={mockArtist}  />,
    {lifecyclExperimental: true}
  );

  expect(props.clearSelectedArtist).toHaveBeenCalled();
});

test('it sets inputValue state and clears new or selected artist when Find Artist input changes', () => {
  const chooseOrCreateArtist = shallow(
    <ChooseOrCreateArtist {...props} isAddingArtist />,
  );

  chooseOrCreateArtist.instance().onArtistInputChange('mockValue');

  expect(props.clearSelectedArtist).toHaveBeenCalled();
  expect(props.clearSelectedArtist).toHaveBeenCalled();
  expect(chooseOrCreateArtist.instance().state.inputValue).toEqual('mockValue');

});

test('it sets adding artist to false and clears find artist input when back button on artist form is pressed', () => {
  const chooseOrCreateArtist = shallow(
    <ChooseOrCreateArtist {...props} isAddingArtist />,
  );

  chooseOrCreateArtist.find('#artist-form-back').simulate('click');

  expect(props.setIsAddingArtist).toHaveBeenCalledWith(false);
  expect(chooseOrCreateArtist.instance().state.isModalOpen).toBeFalsy();

});

test('it shows add artist form when user clicks Create Artist button in artist not found modal', () => {
  const chooseOrCreateArtist = shallow(
    <ChooseOrCreateArtist {...props} />,
  );

  chooseOrCreateArtist.find('#create-artist').simulate('click');

  expect(props.setIsAddingArtist).toHaveBeenCalledWith(true);
});

test('it sets modal state to closed when artist not found modals go back button is pressed', () => {
  const chooseOrCreateArtist = shallow(
    <ChooseOrCreateArtist {...props} />,
  );
  chooseOrCreateArtist.instance().setState({ isModalOpen: true });
  chooseOrCreateArtist.find('#modal-close').simulate('click');

  expect(chooseOrCreateArtist.instance().state.isModalOpen).toBeFalsy();
});

describe('onSubmit method', () => {
  test('it opens artist not found modal if artist was not chosen using Find Artist and find artist autocomplete contains a value', () => {
    const chooseOrCreateArtist = shallow(
      <ChooseOrCreateArtist {...props} />,
    );
    chooseOrCreateArtist.instance().setState({ inputValue: 'Unknown Artist Name' });
    chooseOrCreateArtist.instance().onSubmit();


    expect(chooseOrCreateArtist.instance().state.isModalOpen).toBeTruthy();
  });

    test('it does not open artist not found modal if artist was not chosen using Find Artist and find artist autocomplete contains no value', () => {
    const chooseOrCreateArtist = shallow(
      <ChooseOrCreateArtist {...props} />,
    );
    chooseOrCreateArtist.instance().setState({ inputValue: '' });
    chooseOrCreateArtist.instance().onSubmit();


    expect(chooseOrCreateArtist.instance().state.isModalOpen).toBeFalsy();
  });

    test('it calls onArtistChosen if artist was selected using Find Artist', () => {
    const chooseOrCreateArtist = shallow(
      <ChooseOrCreateArtist {...props} />,
    );

    chooseOrCreateArtist.instance().setState({ inputValue: '' });
    chooseOrCreateArtist.instance().onSubmit(mockArtist);

    expect(props.onArtistChosen).toHaveBeenCalledWith(mockArtist);
  });
});