import React from 'react';

import { FindArtist } from '../FindArtist';

let props;

beforeEach(() => {
  props = {
    handleSubmit: jest.fn(),
    onSelectionMade: jest.fn(),
    externalOnChange: jest.fn(),
  };
})

test('it renders', () => {
  const findArtist = shallow(
    <FindArtist {...props} />,
  );

  expect(findArtist).toMatchSnapshot();
});

test('it calls prop when form is submitted', () => {
  const findArtist = shallow(
    <FindArtist {...props} />
  );

  findArtist.find('form').props().onSubmit();

  expect(props.handleSubmit).toHaveBeenCalled();
});