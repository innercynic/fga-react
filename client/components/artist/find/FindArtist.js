import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';
import { RFFieldError } from '../../form/reduxformfields';
import { Button } from '../../general';

import ArtistAutoComplete from '../../form/autocomplete/ArtistAutoComplete';

export const FindArtist = props => (
  <form onSubmit={props.handleSubmit}>
    <RFFieldError name="_id" />
    <Field
      name="_id"
      component={ArtistAutoComplete}
      onSelectionMade={props.onSelectionMade}
      externalOnChange={props.onArtistInputChange}
    />

    <Flex my={[1, 1, 2, 3]}>
      <Button type="submit" width="100%">Next</Button>
    </Flex>
  </form>
);

FindArtist.defaultProps = {
  onArtistInputChange: null,
};

FindArtist.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSelectionMade: PropTypes.func.isRequired,
  onArtistInputChange: PropTypes.func,
};

export default reduxForm({ form: 'findartist' })(FindArtist);
