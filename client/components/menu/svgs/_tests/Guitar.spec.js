import React from 'react';

import Guitar from '../Guitar';

test('it renders', () => {
  const guitarSVG = shallow(
    <Guitar />,
  );

  expect(guitarSVG).toMatchSnapshot();
});