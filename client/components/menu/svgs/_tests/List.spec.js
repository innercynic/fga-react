import React from 'react';

import List from '../List';

test('it renders', () => {
  const listSVG = shallow(
    <List />,
  );

  expect(listSVG).toMatchSnapshot();
});