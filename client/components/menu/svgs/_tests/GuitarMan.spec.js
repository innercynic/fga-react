import React from 'react';

import GuitarMan from '../GuitarMan';

test('it renders', () => {
  const guitarManSVG = shallow(
    <GuitarMan  />,
  );

  expect(guitarManSVG).toMatchSnapshot();
});