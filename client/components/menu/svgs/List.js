import React from 'react';
import styled from 'styled-components';

import media from '../../../styles/media';


const SVGStyle = styled.svg`
  width: 65pt;

  ${media.tabletPortrait`
    width: 80pt;
  `}

  ${media.tabletLandscape`
    width: 80pt;
  `}

  ${media.desktop`
    width: 190pt;
  `}

  ${media.hd`
    width: 230pt;
  `}

  > g, path {
    fill: ${props => props.theme.svg.guitarhead.fill};
  }
`;

const ListSVG = () => (
  <SVGStyle
    viewBox="0 0 144.000000 90.000000"
    preserveAspectRatio="xMidYMid meet"
  >
    <g
      transform="translate(0.000000,90.000000) scale(0.100000,-0.100000)"
      fill="#000000"
      stroke="none"
    >
      <path d="M360 810 l0 -90 420 0 420 0 0 90 0 90 -420 0 -420 0 0 -90z" />
      <path d="M300 536 l0 -53 -74 -5 c-95 -6 -154 -38 -193 -106 -76 -129 5 -285
    160 -307 l47 -7 0 31 c0 29 -2 31 -37 31 -146 0 -196 203 -71 280 24 15 51 20
    100 20 l67 0 3 -56 3 -57 80 67 c44 37 80 71 80 76 0 8 -151 140 -161 140 -2
    0 -4 -24 -4 -54z"
      />
      <path d="M600 450 l0 -90 420 0 420 0 0 90 0 90 -420 0 -420 0 0 -90z" />
      <path d="M360 90 l0 -90 420 0 420 0 0 90 0 90 -420 0 -420 0 0 -90z" />
    </g>
  </SVGStyle>
);

export default ListSVG;
