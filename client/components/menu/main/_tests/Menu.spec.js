import React from 'react';

import Menu from '../Menu';

test('it renders', () => {
  const menu = shallow(
    <Menu />,
  );

  expect(menu).toMatchSnapshot();
});