import { Flex, Box } from 'grid-styled';
import styled from 'styled-components';

const MenuItem = styled(Flex)`
  background: ${props => props.theme.backgrounds.container};
  border-radius: ${props => props.theme.borders.radiuses.normal};

  &:hover {
    box-shadow: 0 0 3pt 2pt ${props => props.theme.borders.colors.shadow};
  }
`;

export default MenuItem;
