import React from 'react';
import { Flex, Box } from 'grid-styled';
import styled from 'styled-components';

import ListSVG from '../svgs/List';
import GuitarSVG from '../svgs/Guitar';
import GuitarManSVG from '../svgs/GuitarMan';

import media from '../../../styles/media';
import { LinkWithoutUnderline, Heading } from '../../general';

import MenuItem from './MenuItem';

const MainMenuItem = MenuItem.extend`
  height: 152px;

  ${media.tabletPortrait`
    height: 240px;
  `}

  ${media.tabletLandscape`
    height: 350px;
  `}

  ${media.desktop`
    height: 425px;
  `}

  ${media.hd`
    height: 480px;
  `}
`;

const MenuItemContainer = styled(Flex)`
  flex-direction: column;

  ${media.tabletPortrait`
    flex-direction: row;
  `}
`;

const Menu = () => (
  <MenuItemContainer width={1}>
    <Box flex="1" mx={20} mb={10}>
      <LinkWithoutUnderline to="/search">
        <MainMenuItem width={1} p={20} column align="center" justify="center">
          <Flex mb={20} flex={1}>
            <GuitarSVG />
          </Flex>
          <Box>
            <Heading>Search</Heading>
          </Box>
        </MainMenuItem>
      </LinkWithoutUnderline>
    </Box>

    <Box flex="1" mx={20} mb={10}>
      <LinkWithoutUnderline to="/addsong">
        <MainMenuItem width={1} p={20} column align="center" justify="center">
          <Flex mb={20} flex={1}>
            <GuitarManSVG />
          </Flex>
          <Box>
            <Heading>Create</Heading>
          </Box>
        </MainMenuItem>
      </LinkWithoutUnderline>
    </Box>

    <Box flex="1" mx={20} mb={10}>
      <LinkWithoutUnderline to="/setlists">
        <MainMenuItem width={1} p={20} column align="center" justify="center">
          <Flex mb={20} flex={1}>
            <ListSVG />
          </Flex>
          <Box>
            <Heading>Setlists</Heading>
          </Box>
        </MainMenuItem>
      </LinkWithoutUnderline>
    </Box>


    {/* 
    <Box width={1}><MenuLink bg="/assets/images/mainmenu/sdb" to="/search" /></Box>
    <Box width={1}><MenuLink bg="/assets/images/mainmenu/ms" to="/addsong" /></Box>
    <Box width={1}><MenuLink bg="/assets/images/mainmenu/ss" to="/setlists" /></Box>
    */}


  </MenuItemContainer>
);

export default Menu;
