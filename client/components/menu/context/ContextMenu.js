import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box } from 'grid-styled';

import ContextOverlay from './ContextOverlay';
import ContextMenuOuter from './ContextMenuOuter';
import ContextPanel from './ContextPanel';

class ContextMenu extends Component {
  constructor(props) {
    super(props);

    this.processChildren = this.processChildren.bind(this);
    this.getMenuItemClickHander = this.getMenuItemClickHander.bind(this);
  }

  getMenuItemClickHander(menuItemClicked) {
    return () => {
      menuItemClicked();
      this.props.closeContextMenu();
    };
  }

  /* eslint-disable no-plusplus */
  processChildren(children) {
    let keyCounter = 0;
    const childrenArray = children.constructor === Array ? children : [children];

    return childrenArray.filter(child => !!child.props.menuItemClicked)
      .map(child => (
        <Box
          flex="1"
          key={keyCounter++}
          onClick={this.getMenuItemClickHander(child.props.menuItemClicked)}
        >
          {child}
        </Box>
      ));
  }
  /* eslint-enable no-plusplus */

  render() {
    const {
      arrowHeight,
      arrowOffset,
      offsetX,
      offsetY,
      width,
      isPositionedAbove,
      isPositionedRight,
      asRow,
      rounded,
    } = this.props;

    if (this.props.isOpen) {
      return (
        <ContextMenuOuter
          isPositionedAbove={isPositionedAbove}
          isPositionedRight={isPositionedRight}
          offsetY={offsetY}
          offsetX={offsetX}
          width={width}
          arrowHeight={arrowHeight}
          arrowOffset={arrowOffset}
        >

          <ContextOverlay onClick={this.props.closeContextMenu} />

          <ContextPanel asRow={asRow} rounded={rounded}>
            {this.props.children &&
              this.processChildren(this.props.children)
            }
          </ContextPanel>

        </ContextMenuOuter>
      );
    }
    return null;
  }
}

ContextMenu.defaultProps = {
  width: 200,
  isPositionedRight: false,
  isPositionedAbove: false,
  offsetX: 0,
  offsetY: 0,
  arrowHeight: 5,
  arrowOffset: 0,
  children: [],
  asRow: false,
  rounded: false,
};

ContextMenu.propTypes = {
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  isPositionedRight: PropTypes.bool,
  isPositionedAbove: PropTypes.bool,
  asRow: PropTypes.bool,
  rounded: PropTypes.bool,
  offsetX: PropTypes.number,
  offsetY: PropTypes.number,
  arrowHeight: PropTypes.number,
  arrowOffset: PropTypes.number,

  closeContextMenu: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default ContextMenu;


// <ContextArrow 
//   isPositionedRight={isPositionedRight} 
//   isPositionedAbove={isPositionedAbove} 
//   offsetY={offsetY} 
//   arrowOffset={arrowOffset} 
//   arrowHeight={arrowHeight} 
// />
