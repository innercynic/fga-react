import styled from 'styled-components';
import { Flex } from 'grid-styled';

const ContextPanel = styled(Flex)`
  flex-direction: ${props => (props.asRow ? 'row' : 'column')};
  border: ${props => `${props.theme.borders.sizes.medium} solid ${props.theme.borders.colors.normal}`};
  border-radius: ${props => (props.rounded ? props.theme.borders.radiuses.normal : 0)};
  background-color: ${props => props.theme.backgrounds.container_alternate};
`;

export default ContextPanel;
