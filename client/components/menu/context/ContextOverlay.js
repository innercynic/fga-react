import styled from 'styled-components';

const ContextOverlay = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: -1;
  background-color: transparent;
`;

export default ContextOverlay;
