import React from 'react'
import 'jest-styled-components'

import ContextMenu from '../ContextMenu';
import ContextOverlay from '../ContextOverlay';

let props;

beforeEach(() => {
  props  = {
    isOpen: true,
    closeContextMenu: jest.fn(),
  };
})

test('it does not render if not open', () => {
  const contextMenu = shallow(
      <ContextMenu {...props} isOpen={false} />
  );

  expect(contextMenu).toMatchSnapshot();
});

test('it renders when open', () => {
  const contextMenu = shallow(
      <ContextMenu {...props} />
  );

  expect(contextMenu).toMatchSnapshot();
});

test('it calls closeContextMenu prop when overlay is clicked', () => {
  const contextMenu = shallow(
      <ContextMenu {...props} />
  );

  contextMenu.find(ContextOverlay).simulate('click');

  expect(props.closeContextMenu).toHaveBeenCalled();
});

describe('handleMenuItemClicked method', () => {
  test('it is called when menu item is clicked', () => {
    let mock = jest.fn();
    const contextMenu = shallow(
        <ContextMenu {...props}>
          <div id='mockItem' menuItemClicked={mock}>MENUITEM</div>
        </ContextMenu>
    );

    contextMenu.find("#mockItem").parent().simulate('click');

    expect(props.closeContextMenu).toHaveBeenCalled();
    expect(mock).toHaveBeenCalled();
  });
});

describe('processChildren method', () => {
  test('it processes one child', () => {
    let mock = jest.fn();
    const contextMenuInstance = shallow(
        <ContextMenu {...props}>
          <div menuItemClicked={mock}>MENUITEM</div>
        </ContextMenu>
    ).instance();

    var processedChild = contextMenuInstance.processChildren(contextMenuInstance.props.children);

    expect(processedChild).toHaveLength(1);
  });

  test('it processes more than one child', () => {
    let mock = jest.fn();
    const contextMenuInstance = shallow(
        <ContextMenu {...props}>
          <div menuItemClicked={mock}>MENUITEM1</div>
          <div menuItemClicked={mock}>MENUITEM2</div>
        </ContextMenu>
    ).instance();

    var processedChild = contextMenuInstance.processChildren(contextMenuInstance.props.children);

    expect(processedChild).toHaveLength(2);
  });

  test('it removes children that don\'t have menuItemClicked prop', () => {
    let mock = jest.fn();
    const contextMenuInstance = shallow(
        <ContextMenu {...props}>
          <div menuItemClicked={mock}>MENUITEM1</div>
          <div menuItemClicked={mock}>MENUITEM2</div>
          <div>MENUITEM3</div>
        </ContextMenu>
    ).instance();

    var processedChild = contextMenuInstance.processChildren(contextMenuInstance.props.children);

    expect(processedChild).toHaveLength(2);
  });
});


