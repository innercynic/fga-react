import React from 'react'
import 'jest-styled-components'

import ContextMenuOuter from '../ContextMenuOuter';
import MockData from '../../../../utils/test/MockData';

let props;

beforeEach(() => {
  props = {
    theme: MockData.mockThemes[MockData.mockThemeName],
    width: 200,
    isPositionedRight: false,
    isPositionedAbove: false,
    offsetX: 0,
    offsetY: 0,
    arrowHeight: 5,
    arrowOffset: 0,
  };
});

test('it renders with defaults', () => {
  const contextMenu = shallow(
      <ContextMenuOuter {...props} />
  );

  expect(contextMenu).toMatchSnapshot();
});

test('it renders with offsets in opposite position and different arrow height/offset', () => {
  const contextMenu = shallow(
      <ContextMenuOuter 
        {...props} 
        isPositionedRight 
        isPositionedAbove 
        arrowHeight={8}
        arrowOffset={15}
        offsetX={30}
        offsetY={20}
      />
  );

  expect(contextMenu).toMatchSnapshot();
});



  // expect(overlayModal).toHaveStyleRule('max-width', '400px');