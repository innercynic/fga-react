import styled from 'styled-components';
import PropTypes from 'prop-types';

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

const ContextMenuOuter = styled.div`
  position: absolute;
  left: ${props => (props.isPositionedRight ? `${props.offsetX}px` : 'auto')};
  right: ${props => (props.isPositionedRight ? 'auto' : `${props.offsetX}px`)};
  top: ${props => (props.isPositionedAbove ? 'auto' : `${props.offsetY + props.arrowHeight}px`)};
  bottom: ${props => (props.isPositionedAbove ? `${props.offsetY + props.arrowHeight}px` : 'auto')};

  width: ${props => (isNumber(props.width) ? `${props.width}px` : props.width)};
  z-index: 101;

  &:before{
    position: absolute;
    left: ${props => (props.isPositionedRight ? `${props.arrowOffset}px` : 'auto')};
    right: ${props => (props.isPositionedRight ? 'auto' : `${props.arrowOffset}px`)};
    top: ${props => (props.isPositionedAbove ? 'auto' : `-${props.arrowHeight}px`)};
    bottom: ${props => (props.isPositionedAbove ? `-${props.arrowHeight}px` : 'auto')};

    width: 0;
    height: 0;
    border-left: ${props => props.arrowHeight}px solid transparent;
    border-right: ${props => props.arrowHeight}px solid transparent;
    border-bottom: ${props => (props.isPositionedAbove ? 'none' : `${props.arrowHeight}px solid ${props.theme.borders.colors.normal}`)};
    border-top: ${props => (props.isPositionedAbove ? `${props.arrowHeight}px solid ${props.theme.borders.colors.normal}` : 'none')};
    z-index: 101;
    content: ' ';
  }
`;

ContextMenuOuter.defaultProps = {
  isPositionedRight: false,
  isPositionedAbove: false,
  width: 200,
  offsetX: 0,
  offsetY: 0,
  arrowHeight: 5,
  arrowOffset: 0,
};

ContextMenuOuter.propTypes = {
  width: PropTypes.number,
  isPositionedAbove: PropTypes.bool,
  isPositionedRight: PropTypes.bool,
  offsetX: PropTypes.number,
  offsetY: PropTypes.number,
  arrowHeight: PropTypes.number,
  arrowOffset: PropTypes.number,
};

export default ContextMenuOuter;
