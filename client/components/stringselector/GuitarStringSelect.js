import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import media from '../../styles/media';
import { Input } from '../form/fields';

export const StringSelectField = Input.withComponent(Field).extend`
  height: 2rem;
  font-size: ${props => props.theme.fonts.sizes.text.small};
  padding: 3px;
  margin-bottom: 5px;

  ${media.tabletPortrait`
    height: 2.5rem;
    font-size: ${props => props.theme.fonts.sizes.text.normal};
    padding: 11px;
    margin-bottom: 10px;
  `}  

  ${media.tabletLandscape`
    height: 3rem;
    font-size: ${props => props.theme.fonts.sizes.text.normal};
    padding: 13px;
    margin-bottom: 11px;
  `}
`;

const GuitarStringSelect = props => (
  <StringSelectField name={props.name} component="select">
    {props.children}
  </StringSelectField>
);

GuitarStringSelect.defaultProps = {
  children: [],
};

GuitarStringSelect.propTypes = {
  name: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default GuitarStringSelect;
