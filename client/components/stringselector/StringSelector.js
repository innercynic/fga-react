import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import GuitarHead from './GuitarHead';
import GuitarStringSelectorContainer from './GuitarStringSelectorContainer';
import GuitarStringSelect from './GuitarStringSelect';

const stringOptions = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'];
const defaultStrings = ['E', 'A', 'D', 'G', 'B', 'E'];

class StringSelector extends Component {
  componentWillMount() {
  // If strings aren't set (e.g new song), apply defaults
    if (this.props.fields.length === 0) {
      defaultStrings.map(value => this.props.fields.push(value));
    }
  }

  createOptions() {
    return stringOptions.map(stringOption =>
      <option value={stringOption} key={stringOption}>{stringOption}</option>,
    );
  }
  render() {
    return (
      <Flex>
        <GuitarStringSelectorContainer >
          <GuitarStringSelect name={`${this.props.fields.name}[2]`}>{this.createOptions()}</GuitarStringSelect>
          <GuitarStringSelect name={`${this.props.fields.name}[1]`}>{this.createOptions()}</GuitarStringSelect>
          <GuitarStringSelect name={`${this.props.fields.name}[0]`}>{this.createOptions()}</GuitarStringSelect>
        </GuitarStringSelectorContainer>
        <Box mx={14}>
          <GuitarHead />
        </Box>
        <GuitarStringSelectorContainer>
          <GuitarStringSelect name={`${this.props.fields.name}[3]`}>{this.createOptions()}</GuitarStringSelect>
          <GuitarStringSelect name={`${this.props.fields.name}[4]`}>{this.createOptions()}</GuitarStringSelect>
          <GuitarStringSelect name={`${this.props.fields.name}[5]`}>{this.createOptions()}</GuitarStringSelect>
        </GuitarStringSelectorContainer>
      </Flex>
    );
  }
}

StringSelector.propTypes = {
  fields: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default StringSelector;
