import styled from 'styled-components';
import media from '../../styles/media';

const GuitarHeadSVG = styled.svg`
  ${media.phone`
    width: 135px;
    height: 210px;
  `}  

  ${media.tabletPortrait`
    width: 160px;
    height: 250px;
  `}  

  ${media.tabletLandscape`
    width: 200px;
    height: 300px;
  `}

  > g, path {
    stroke: ${props => props.theme.svg.guitarhead.stroke};
    stroke-width: 11px;
    fill: ${props => props.theme.svg.guitarhead.fill};
  }
`;

export default GuitarHeadSVG;
