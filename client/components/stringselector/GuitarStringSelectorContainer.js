import styled from 'styled-components';
import { Box } from 'grid-styled';

import media from '../../styles/media';

const GuitarStringSelectorContainer = styled(Box)`
   ${media.phone`
    padding-top: 45px;
  `}  

  ${media.tabletPortrait`
    padding-top: 56px;
  `}  

  ${media.tabletLandscape`
    padding-top: 67px;
  `}
`;

export default GuitarStringSelectorContainer;
