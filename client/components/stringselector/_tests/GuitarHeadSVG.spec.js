import React from 'react';

import GuitarHeadSVG from '../GuitarHeadSVG';
import TestUtils from '../../../utils/test/TestUtils';
import MockData from '../../../utils/test/MockData';

let ThemeProviderWrapper = TestUtils.getMockThemeProvider();

test('it renders', () => {
  const mockTheme = MockData.mockThemes[MockData.mockThemeName];
  const guitarHeadSVG = shallow(
    <GuitarHeadSVG theme={mockTheme}/>
  );

  expect(guitarHeadSVG).toMatchSnapshot();
});
