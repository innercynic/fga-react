import React from 'react';
import 'jest-styled-components';

import MockData from '../../../utils/test/MockData';
import GuitarStringSelect, { StringSelectField } from '../GuitarStringSelect';

test('it renders', () => {
  const guitarStringSelect = shallow(
    <GuitarStringSelect name="mockStringSelectField" />,
  );

  expect(guitarStringSelect).toMatchSnapshot();
});

test('its custom string select field renders', () => {
  const stringSelectField = shallow(
    <StringSelectField name="mockStringSelectField" component="select" theme={MockData.mockThemes[MockData.mockThemeName]} />,
  );

  expect(stringSelectField).toMatchSnapshot();
});