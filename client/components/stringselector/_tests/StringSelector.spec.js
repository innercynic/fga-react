import React from 'react';

import StringSelector from '../StringSelector';

let props = {
  fields: {
    push: jest.fn(),
    length: 0,
    name: "mockFieldArrayName",
  },
};

test('it renders', () => {
  const stringSelector = shallow(
    <StringSelector {...props} />,
  );

  expect(stringSelector).toMatchSnapshot();
});