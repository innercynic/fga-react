import React from 'react'
import 'jest-styled-components'
import sinon from 'sinon';

import GuitarHead from '../GuitarHead';

test('it renders', () => {
  const guitarHead = shallow(
    <GuitarHead />,
  );

  expect(guitarHead).toMatchSnapshot();
});

test('it renders when width is passed as prop', () => {
  const guitarHead = shallow(
    <GuitarHead width={500}/>,
  );
  expect(guitarHead).toMatchSnapshot();
});

test('it adds and removes event listeners for window resize', () => {
  const mockAddEventListener = sinon.spy(window, 'addEventListener');
  const mockremoveEventListener = sinon.spy(window, 'removeEventListener');

  const guitarHead = shallow(
    <GuitarHead />,
    { lifecycleExperimental: true }
  );

  expect(mockAddEventListener.called).toBeTruthy();
  guitarHead.unmount();
  expect(mockremoveEventListener.called).toBeTruthy();
});

test('it forces update when resizeSVG is called', () => {
  const guitarHead = shallow(
    <GuitarHead />,
    { lifecycleExperimental: true }
  );

  const mockForceUpdate = jest.fn();
  guitarHead.instance().forceUpdate = mockForceUpdate;
  guitarHead.instance().resizeSVG();

  expect(mockForceUpdate).toHaveBeenCalled();
});

test('it creates a transformation for svg for device smaller than tablet', () => {
  let mediaSizes = {
    phone: 0,
    tabletPortrait: 600,
    tabletLandscape: 900,
    desktop: 1200,
    hd: 1600,
  };

  let transformation = GuitarHead.transform(500, mediaSizes);

  expect(transformation).toMatchSnapshot();
});

test('it creates a transformation for svg for device smaller than tablet in landscape orientation', () => {
  let mediaSizes = {
    phone: 0,
    tabletPortrait: 600,
    tabletLandscape: 900,
    desktop: 1200,
    hd: 1600,
  };

  let transformation = GuitarHead.transform(700, mediaSizes);

  expect(transformation).toMatchSnapshot();
});

test('it creates a transformation for svg for tablet in landscape orientation or bigger', () => {
  let mediaSizes = {
    phone: 0,
    tabletPortrait: 600,
    tabletLandscape: 900,
    desktop: 1200,
    hd: 1600,
  };

  let transformation = GuitarHead.transform(1000, mediaSizes);

  expect(transformation).toMatchSnapshot();
});