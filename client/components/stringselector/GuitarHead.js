import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { sizes } from '../../styles/media';
import GuitarHeadSVG from './GuitarHeadSVG';

class GuitarHead extends Component {
  static transform(width, mediaSizes) {
    let translateY = 302;
    let scale = 0.1;

    if (width < mediaSizes.tabletPortrait) {
      translateY = 200;
      scale = 0.065;
    } else if (width < mediaSizes.tabletLandscape) {
      translateY = 250;
      scale = 0.080;
    }

    return `translate(0, ${translateY}) scale(${scale},-${scale})`;
  }

  constructor(props) {
    super(props);
    this.resizeSVG = this.resizeSVG.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeSVG);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeSVG);
  }

  resizeSVG() {
    this.forceUpdate();
  }

  render() {
    const width = this.props.width || window.innerWidth;

    return (
      <GuitarHeadSVG>
        <g transform={GuitarHead.transform(width, sizes)} stroke="none">
          <path d="M910 3006 c-79 -22 -125 -53 -216 -143 -112 -111 -141 -126 -243
          -121 l-77 3 -33 -37 c-31 -36 -139 -98 -169 -98 -8 0 -12 -6 -9 -14 27 -69
          100 -298 112 -348 15 -67 10 -84 -23 -74 -18 6 -20 13 -15 62 7 63 -1 74 -55
          74 -43 0 -100 -24 -134 -57 -26 -25 -28 -32 -28 -114 0 -87 0 -87 35 -117 53
          -47 133 -65 170 -38 15 11 17 22 12 65 l-6 51 39 0 c46 0 42 8 71 -160 37
          -223 48 -331 33 -337 -28 -11 -37 4 -38 62 l-1 60 -53 3 c-43 2 -62 -2 -98
          -23 -24 -14 -48 -34 -54 -45 -5 -10 -10 -55 -10 -100 0 -79 1 -83 31 -111 37
          -34 90 -52 144 -47 l40 3 3 63 c3 62 3 63 30 60 26 -3 27 -5 33 -86 4 -45 8
          -169 10 -275 l4 -192 -30 0 c-30 0 -30 0 -27 53 2 31 -1 59 -8 68 -29 33 -140
          4 -190 -50 -22 -25 -25 -36 -25 -106 0 -70 3 -81 25 -106 49 -52 136 -78 182
          -53 18 10 20 17 14 65 l-6 54 35 0 35 0 0 -146 0 -146 50 -55 c95 -104 154
          -243 168 -398 4 -45 11 -79 17 -77 11 2 11 650 0 760 l-7 62 -35 0 c-25 0 -43
          8 -63 29 -52 51 -52 111 0 162 39 40 104 41 150 3 17 -15 32 -25 34 -23 4 5
          -42 429 -48 445 -3 8 -18 10 -44 7 -50 -7 -95 18 -117 63 -20 42 -19 62 5 101
          32 53 96 77 142 52 11 -6 22 -9 24 -7 4 4 -35 236 -56 336 -11 50 -22 63 -35
          42 -9 -15 -72 -12 -106 6 -18 9 -37 31 -46 51 -32 78 18 155 102 157 41 1 50
          -3 81 -36 36 -40 44 -81 23 -119 -7 -15 -4 -52 15 -147 57 -285 124 -730 206
          -1367 23 -176 49 -378 58 -450 l17 -130 48 -3 c27 -2 54 2 61 7 7 6 16 39 20
          74 7 68 37 286 128 932 33 237 64 468 69 515 4 47 18 139 29 205 50 283 57
          340 49 363 -32 85 39 175 124 157 49 -11 97 -65 97 -110 0 -32 -33 -86 -61
          -100 -34 -18 -91 -19 -107 -3 -8 8 -15 8 -21 3 -7 -8 -61 -345 -61 -378 0 -4
          9 -3 19 3 75 40 174 -49 150 -135 -13 -45 -62 -85 -104 -85 -20 0 -38 -6 -40
          -12 -7 -20 -46 -401 -41 -405 2 -3 13 0 24 6 34 18 97 13 125 -9 36 -28 52
          -81 37 -124 -14 -44 -42 -68 -88 -75 l-37 -6 -6 -135 c-4 -74 -8 -259 -10
          -410 -4 -239 -2 -275 11 -275 11 0 18 22 26 90 21 162 66 279 150 388 l44 57
          0 145 0 145 31 0 30 0 -6 -54 c-6 -48 -5 -55 13 -65 33 -18 74 -13 130 15 65
          32 82 63 82 148 0 56 -4 69 -27 97 -49 57 -163 90 -192 55 -7 -8 -11 -37 -9
          -68 3 -51 2 -53 -22 -53 l-25 0 2 185 c1 102 5 227 8 278 l7 93 26 -3 c26 -3
          27 -5 23 -57 -4 -63 4 -71 70 -71 36 0 58 7 95 33 l49 32 3 87 c1 49 -2 97 -8
          108 -6 10 -30 31 -54 45 -36 21 -55 25 -98 23 l-53 -3 0 -60 c0 -56 -2 -60
          -24 -63 l-24 -4 7 84 c8 96 40 300 58 373 11 43 17 50 38 51 45 3 50 -5 47
          -64 -2 -54 -1 -56 29 -65 41 -12 106 7 148 43 26 24 35 40 39 77 9 72 -6 142
          -34 165 -58 45 -154 64 -178 34 -7 -8 -11 -37 -9 -67 3 -45 0 -54 -16 -57 -10
          -2 -22 0 -27 5 -10 10 42 178 105 337 20 50 35 90 33 91 -1 1 -32 15 -68 32
          -36 17 -84 48 -106 69 l-41 39 -75 -3 c-95 -5 -128 11 -227 112 -41 43 -95 90
          -120 106 -85 54 -213 73 -310 46z"
          />
          <path d="M710 1548 c0 -7 7 -65 15 -128 21 -156 40 -369 70 -790 14 -195 29
          -400 33 -455 l7 -100 43 -1 c23 -1 44 1 46 5 6 10 -69 590 -150 1166 -43 307
          -44 315 -55 315 -5 0 -9 -6 -9 -12z"
          />
          <path d="M1300 1546 c0 -4 -13 -100 -30 -214 -73 -507 -170 -1238 -165 -1249
          6 -16 71 -17 81 -2 4 7 24 201 45 433 21 232 50 538 65 681 32 305 34 346 17
          352 -7 3 -13 2 -13 -1z"
          />
          <path d="M731 973 c-1 -6 -12 -25 -26 -41 l-24 -29 10 -404 c6 -222 12 -410
          14 -416 3 -8 22 -13 51 -13 l47 0 -6 133 c-13 259 -49 742 -57 762 -5 11 -9
          15 -9 8z"
          />
          <path d="M1290 916 c0 -13 -13 -170 -30 -348 -16 -177 -31 -361 -32 -408 l-3
          -85 48 -3 c32 -2 49 1 52 10 3 7 9 193 13 413 9 394 9 400 -11 423 -26 28 -37
          28 -37 -2z"
          />
        </g>
      </GuitarHeadSVG>
    );
  }
}

GuitarHead.defaultProps = {
  width: null,
};

GuitarHead.propTypes = {
  width: PropTypes.number,
};
export default GuitarHead;

