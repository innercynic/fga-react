import React from 'react';

import MockData from '../../../../utils/test/MockData';
import { SetlistTuningInstructionsContainer } from '../SetlistTuningInstructionsContainer';

let props;

beforeEach(() => {
  props = {
    requestSetlist: jest.fn(),
    clearSelectedSetlist: jest.fn(),
    processTuningInstructions: jest.fn(),
    setlist: MockData.mockSetlists[0],
    tuningInstructions: [],
    match: {
      params: {
        id: MockData.mockSetlists[0]._id,
      },
      path: "",
      url: "",
    },
  };
})

test('it renders', () => {
  const setlistTuningInstructionsContainer = shallow(
    <SetlistTuningInstructionsContainer {...props} />,
  );

  expect(setlistTuningInstructionsContainer).toMatchSnapshot();
});

test('it renders loader if fetching setlist or processing tuning instructions', () => {
  const setlistTuningInstructionsContainer = shallow(
    <SetlistTuningInstructionsContainer {...props} setlist={null} />,
  );

  expect(setlistTuningInstructionsContainer).toMatchSnapshot();
});

test('it requests the setlist if not passed as prop', () => {
  const setlistTuningInstructionsContainer = shallow(
    <SetlistTuningInstructionsContainer {...props} setlist={null} />,
    { lifecycleExperimental: true }
  );

  expect(props.requestSetlist).toHaveBeenCalled();
});

test('it requests the setlist if setlist passed as prop has different id to path param', () => {
  const setlistWithDifferentId = {
    ...MockData.mockSetlists[0],
    _id: 123,
  };

  const setlistTuningInstructionsContainer = shallow(
    <SetlistTuningInstructionsContainer {...props} setlist={setlistWithDifferentId}/>,
    { lifecycleExperimental: true }
  );

  expect(props.requestSetlist).toHaveBeenCalled();
});

test('it processes tuning instructions when receiving setlist prop', () => {
  const setlistTuningInstructionsContainer = shallow(
    <SetlistTuningInstructionsContainer {...props} />,
    { lifecycleExperimental: true }
  );

  const mockProcessTuningInstructions = jest.fn();
  setlistTuningInstructionsContainer.instance().processTuningInstructions = mockProcessTuningInstructions;

  setlistTuningInstructionsContainer.setProps({
    setlist: MockData.mockSetlists[1]
  });

  expect(mockProcessTuningInstructions).toHaveBeenCalledWith(MockData.mockSetlists[1].songs);
});


test('it processes tuning instructions for a setlist', () => {
  const setlistTuningInstructionsContainer = shallow(
    <SetlistTuningInstructionsContainer {...props} />,
  );

  setlistTuningInstructionsContainer.instance().processTuningInstructions(MockData.mockSetlists[0].songs);

  expect(props.processTuningInstructions).toHaveBeenCalled();
});
