import React from 'react';

import SetlistTuningInstructions from '../SetlistTuningInstructions';

let props;
const mockTuningInstructions = [{
  from: "FirstSong",
  to: "SecondSong",
  instructions: ["Mock tuning instructions"]
}];

beforeEach(() => {
  props = {
    tuningInstructions: mockTuningInstructions,
    setlistName: "mockSetlistName",
  };
})

test('it renders', () => {
  const setlistTuningInstructions = shallow(
    <SetlistTuningInstructions {...props} />,
  );

  expect(setlistTuningInstructions).toMatchSnapshot();
});