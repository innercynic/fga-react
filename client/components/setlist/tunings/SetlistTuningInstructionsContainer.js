import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import { SetlistShape } from '../../../proptypes';
import { requestSetlist, clearSelectedSetlist, processTuningInstructions } from '../../../actions/setlists/setlist';

import SetlistTuningInstructions from './SetlistTuningInstructions';
import { Container, Heading, SubHeading, Text, LoaderWithOverlay } from '../../general';

export class SetlistTuningInstructionsContainer extends Component {
  constructor(props) {
    super(props);

    this.processTuningInstructions = this.processTuningInstructions.bind(this);
  }

  componentWillMount() {
    if (!this.props.match) return;

    if (!this.props.match.params.id) {

    } else {
      const setlistId = this.props.match.params.id;

      if (!this.props.setlist || this.props.setlist._id !== setlistId) {
        this.props.clearSelectedSetlist();
        this.props.requestSetlist({ id: setlistId });
      } else {
        this.processTuningInstructions(this.props.setlist.songs);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.setlist !== this.props.setlist) {
      this.processTuningInstructions(nextProps.setlist.songs);
    }
  }

  processTuningInstructions(setlistSongs) {
    this.props.processTuningInstructions(setlistSongs);
  }

  render() {
    if (!this.props.tuningInstructions || !this.props.setlist) {
      return <LoaderWithOverlay fullscreen isLoading />;
    }

    return (
      <SetlistTuningInstructions
        setlistName={this.props.setlist.name}
        tuningInstructions={this.props.tuningInstructions}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    setlist: state.setlist.selected,
    tuningInstructions: state.setlist.tuningInstructions,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    requestSetlist,
    clearSelectedSetlist,
    processTuningInstructions,
  }, dispatch);
}

SetlistTuningInstructionsContainer.defaultProps = {
  setlist: null,
};

SetlistTuningInstructionsContainer.propTypes = {
  requestSetlist: PropTypes.func.isRequired,
  clearSelectedSetlist: PropTypes.func.isRequired,
  processTuningInstructions: PropTypes.func.isRequired,
  setlist: SetlistShape,
};

export default connect(mapStateToProps, matchDispatchToProps)(SetlistTuningInstructionsContainer);
