import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import styled from 'styled-components';

import { SetlistShape } from '../../../proptypes';
import { requestSetlist, clearSelectedSetlist } from '../../../actions/setlists/setlist';

import { Container, Heading, SubHeading, Text, LoaderWithOverlay } from '../../general';

const StepNumber = Container.extend`
  border-radius: 50%;    
  margin-right: 10px;
  height: 30px;
  flex: 0 0 30px;
`;

const SetlistTuningInstructions = ({ setlistName, tuningInstructions }) => (
  <Container column p={[3, 3, 4, 4]} >
    <Box mb={20}>
      <Heading>Tuning instructions for {setlistName}</Heading>
    </Box>

    {tuningInstructions.map((songChange, index) => (
      <Flex column key={`songChange-${index}`}mb={10}>
        <Flex mb={10} align="center">
          <StepNumber forContentItem justify="center" align="center">
            <SubHeading>{index + 1}</SubHeading>
          </StepNumber>
          <SubHeading>{songChange.from} to {songChange.to}</SubHeading>
        </Flex>
        {songChange.instructions.map((step, index) => (
          <Box key={`instruction-${index}`} mb={5}>
            <Text>{step}</Text>
          </Box>
        ))}
      </Flex>
    ))}
  </Container>
);

SetlistTuningInstructions.propTypes = {
  tuningInstructions: PropTypes.arrayOf(PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
  )).isRequired,
  setlistName: PropTypes.string.isRequired,
};

export default SetlistTuningInstructions;
