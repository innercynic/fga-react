import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import { FlexExtended, Icon, Text } from '../../general';

const SetlistMenuItem = props => (
  <Flex my={10} ml={10} align="center">
    <FlexExtended flex={1} clickable p={5} align="center" onClick={props.clickHandler}>
      <Box mr={[1]}><Icon icon={props.icon} size="large" /></Box>
      {!props.hideMenuLabels &&
        <Text>{props.label}</Text>
      }
      {props.children}
    </FlexExtended>
  </Flex>
);

SetlistMenuItem.defaultProps = {
  children: [],
};

SetlistMenuItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  clickHandler: PropTypes.func.isRequired,
  hideMenuLabels: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

export default SetlistMenuItem;
