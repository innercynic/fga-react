import React from 'react';

import SetlistMenuItem from '../SetlistMenuItem';
import { FlexExtended, Text } from '../../../general';

let props;

beforeEach(() => {
  props = {
    clickHandler: jest.fn(),
    hideMenuLabels: false,
    label: "MockMenuItemName",
    icon: "mockIconName",
  };
})

test('it renders', () => {
  const setlistMenuItem = shallow(
    <SetlistMenuItem {...props} />,
  );

  expect(setlistMenuItem).toMatchSnapshot();
});

test('it renders with labels hidden', () => {
  const setlistMenuItem = shallow(
    <SetlistMenuItem {...props} hideMenuLabels={true} />,
  );

  expect(setlistMenuItem.find(Text)).toHaveLength(0);
});

test('it calls on click handler prop', () => {
  const setlistMenuItem = shallow(
    <SetlistMenuItem {...props} />,
  );

  setlistMenuItem.find(FlexExtended).simulate('click');

  expect(props.clickHandler).toHaveBeenCalled();
});