import React from 'react';

import MockData from '../../../../utils/test/MockData';
import SetlistMenu from '../SetlistMenu';

let props;

beforeEach(() => {
  props = {
    menuIsOpen: true,
    toggleMenu: jest.fn(),
    sortByEfficiency: jest.fn(),
    sortByTuning: jest.fn(),
    sortByArtist: jest.fn(),
    setlist: MockData.mockSetlists[0],
    setlistHasChanged: false,
    onSaveSetlist: jest.fn(),
    showTuningInstructions: jest.fn(),
  };
});

test('it renders', () => {
  const setlistMenu = shallow(
    <SetlistMenu {...props} />,
  );

  expect(setlistMenu).toMatchSnapshot();
});

test('it renders with buttons hidden when setlist is too short', () => {
  props.setlist.songs = [];

  const setlistMenu = shallow(
    <SetlistMenu {...props} />,
  );

  expect(setlistMenu).toMatchSnapshot();
});