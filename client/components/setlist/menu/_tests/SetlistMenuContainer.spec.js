import React from 'react';
import sinon from 'sinon';

import MockData from '../../../../utils/test/MockData';
import { SetlistMenuContainer } from '../SetlistMenuContainer';

let props;

beforeEach(() => {
  props = {
    sortByEfficiency: jest.fn(),
    sortByTuning: jest.fn(),
    sortByArtist: jest.fn(),
    onSaveSetlist: jest.fn(),
    setlist: MockData.mockSetlists[0],
    setlistHasChanged: false,
    showTuningInstructions: jest.fn(),
  };
})

test('it renders', () => {
  const setlistMenuContainer = shallow(
    <SetlistMenuContainer {...props} />,
  );

  expect(setlistMenuContainer).toMatchSnapshot();
});

test('it hides labels when there is not enough space', () => {
  const setlistMenuContainer = shallow(
    <SetlistMenuContainer {...props} />,
  );

  expect(setlistMenuContainer.instance().shouldHideLabels(360)).toBeTruthy();
  expect(setlistMenuContainer.instance().shouldHideLabels(650)).toBeTruthy();
  expect(setlistMenuContainer.instance().shouldHideLabels(800)).toBeFalsy();
});

test('it updates isHidingLabels state when window changes size', () => {
  const setlistMenuContainer = shallow(
    <SetlistMenuContainer {...props} />,
  );

  setlistMenuContainer.instance().shouldHideLabels = jest.fn()
  .mockReturnValueOnce(true).mockReturnValue(false);

  setlistMenuContainer.instance().handleWindowSizeChange();
  expect(setlistMenuContainer.instance().state.hideMenuLabels).toBeTruthy();

  setlistMenuContainer.instance().handleWindowSizeChange();
  expect(setlistMenuContainer.instance().state.hideMenuLabels).toBeFalsy();
});

test('it toggles if sort menu is open', () => {
  const setlistMenuContainer = shallow(
    <SetlistMenuContainer {...props} />,
  );

  setlistMenuContainer.instance().toggleMenu();
  expect(setlistMenuContainer.instance().state.menuIsOpen).toBeTruthy();
});

test('it adds and removes event listeners for window resize', () => {
  const mockAddEventListener = sinon.spy(window, 'addEventListener');
  const mockremoveEventListener = sinon.spy(window, 'removeEventListener');

  const setlistMenuContainer = shallow(
    <SetlistMenuContainer {...props} />,
    { lifecycleExperimental: true }
  );

  expect(mockAddEventListener.called).toBeTruthy();
  setlistMenuContainer.unmount();
  expect(mockremoveEventListener.called).toBeTruthy();
});