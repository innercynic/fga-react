import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { SetlistShape } from '../../../proptypes';

import SetlistMenu from './SetlistMenu';

export class SetlistMenuContainer extends Component {
  constructor(props) {
    super(props);

    this.toggleMenu = this.toggleMenu.bind(this);

    this.state = {
      menuIsOpen: false,
      hideMenuLabels: this.shouldHideLabels(window.innerWidth),
    };

    this.shouldHideLabels = this.shouldHideLabels.bind(this);
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  shouldHideLabels(windowWidth) {
    if (windowWidth > 599 && windowWidth < 700) {
      return true;
    }
    if (windowWidth < 380) {
      return true;
    }

    return false;
  }

  handleWindowSizeChange() {
    const isHidingLabels = this.shouldHideLabels(window.innerWidth);

    if (this.state.hideMenuLabels !== isHidingLabels) {
      this.setState({
        hideMenuLabels: isHidingLabels,
      });
    }
  }

  toggleMenu() {
    this.setState({
      menuIsOpen: !this.state.menuIsOpen,
    });
  }

  render() {
    return (
      <SetlistMenu
        menuIsOpen={this.state.menuIsOpen}
        hideMenuLabels={this.state.hideMenuLabels}
        toggleMenu={this.toggleMenu}
        {...this.props}
      />
    );
  }
}

SetlistMenuContainer.propTypes = {
  sortByEfficiency: PropTypes.func.isRequired,
  sortByTuning: PropTypes.func.isRequired,
  sortByArtist: PropTypes.func.isRequired,
  onSaveSetlist: PropTypes.func.isRequired,
  setlist: SetlistShape.isRequired,
  setlistHasChanged: PropTypes.bool.isRequired,
  showTuningInstructions: PropTypes.func.isRequired,
};

export default SetlistMenuContainer;
