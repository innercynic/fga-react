import React from 'react';
import PropTypes from 'prop-types';
import { Box } from 'grid-styled';

import { SetlistShape } from '../../../proptypes';

import ContextMenu from '../../menu/context/ContextMenu';
import SetlistMenuItem from './SetlistMenuItem';
import { FlexExtended, Icon, Text } from '../../general';

const SetlistMenu = props => (
  <FlexExtended justify="flex-end">
    {props.setlist.songs.length > 0 && props.setlistHasChanged &&
      <SetlistMenuItem
        clickHandler={props.onSaveSetlist}
        hideMenuLabels={props.hideMenuLabels}
        label="Save Changes"
        icon="save"
      />
    }

    {props.setlist.songs.length > 1 &&
      <SetlistMenuItem
        clickHandler={props.showTuningInstructions}
        hideMenuLabels={props.hideMenuLabels}
        label="Tuning Instructions"
        icon="instructions"
      />
    }

    {props.setlist.songs.length > 2 &&
      <SetlistMenuItem
        clickHandler={props.toggleMenu}
        hideMenuLabels={props.hideMenuLabels}
        label="Sort"
        icon="sort"
      >
        <FlexExtended position="relative">
          <ContextMenu
            isOpen={props.menuIsOpen}
            closeContextMenu={props.toggleMenu}
            offsetY={20}
            offsetX={-30}
            arrowHeight={6}
            arrowOffset={43}
            width={290}
          >
            <FlexExtended clickable menuItemClicked={props.sortByEfficiency} p={[2]}>
              <Box mr={[1]}><Icon clickable icon="numerically" size="large" /></Box>
              <Text>Most Efficient Tuning Sequence</Text>
            </FlexExtended>


            <FlexExtended clickable menuItemClicked={props.sortByName} p={[2]}>
              <Box mr={[1]}><Icon clickable icon="alphabetically" size="large" /></Box>
              <Text>Alphabetically By Song Name</Text>
            </FlexExtended>

            <FlexExtended clickable menuItemClicked={props.sortByTuning} p={[2]}>
              <Box mr={[1]}><Icon clickable icon="alphabetically" size="large" /></Box>
              <Text>Alphabetically By Tuning</Text>
            </FlexExtended>

            <FlexExtended clickable menuItemClicked={props.sortByArtist} p={[2]}>
              <Box mr={[1]}><Icon clickable icon="person" size="large" /></Box>
              <Text>Artist</Text>
            </FlexExtended>

          </ContextMenu>
        </FlexExtended>
      </SetlistMenuItem>
    }
  </FlexExtended>
);

SetlistMenu.defaultProps = {
  hideMenuLabels: false,
};

SetlistMenu.propTypes = {
  menuIsOpen: PropTypes.bool.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  sortByEfficiency: PropTypes.func.isRequired,
  sortByTuning: PropTypes.func.isRequired,
  sortByArtist: PropTypes.func.isRequired,
  setlist: SetlistShape.isRequired,
  setlistHasChanged: PropTypes.bool.isRequired,
  onSaveSetlist: PropTypes.func.isRequired,
  showTuningInstructions: PropTypes.func.isRequired,
  hideMenuLabels: PropTypes.bool,
};

export default SetlistMenu;

