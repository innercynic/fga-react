import React from 'react';

import MockData from '../../../../utils/test/MockData';
import CreateSetList from '../CreateSetList';
import ChooseSetlistSongs from '../ChooseSetlistSongs';
import ConfigureSetlistContainer from '../ConfigureSetlistContainer';

let props;

beforeEach(() => {
  props = {
    setlist: MockData.mockSetlists[0],
    songs: MockData.mockSongs,
    isMobile: false,
    isChoosingSong: false,
    isAddingName: false,
    setlistHasChanged: false,
    onChangeName: jest.fn(),
    onSaveSetlist: jest.fn(),
    onSongClick: jest.fn(),
    closeModal: jest.fn(),
    showChooseSong: jest.fn(),
    hideChooseSong: jest.fn(),
    onSearch: jest.fn(),
    editName: jest.fn(),
  };
})

test('it renders', () => {
  const createSetList = shallow(
    <CreateSetList {...props} />,
  );

  expect(createSetList).toMatchSnapshot();
});

test('it renders "New Setlist" for name when creating new set list', () => {
  const createSetList = shallow(
    <CreateSetList {...props} setlist={{songs: []}} />,
  );

  expect(createSetList).toMatchSnapshot();
});

test('it hides setlist when choosing song on mobile', () => {
  const createSetList = shallow(
    <CreateSetList {...props} isChoosingSong={true} isMobile={true} />,
  );

  expect(createSetList.find(ConfigureSetlistContainer)).toHaveLength(0);
  expect(createSetList.find(ChooseSetlistSongs)).toHaveLength(1);
});

test('it hides song chooser when not choosing song on mobile', () => {
  const createSetList = shallow(
    <CreateSetList {...props} isChoosingSong={false} isMobile={true} />,
  );

  expect(createSetList.find(ConfigureSetlistContainer)).toHaveLength(1);
  expect(createSetList.find(ChooseSetlistSongs)).toHaveLength(0);
});

test('it calls edit name prop when icon is clicked', () => {
  const createSetList = shallow(
    <CreateSetList {...props} />,
  );

  createSetList.find('#edit-name').simulate('click');

  expect(props.editName).toHaveBeenCalled();
});

test('it shows song chooser when icon is clicked on mobile', () => {
  const createSetList = shallow(
    <CreateSetList {...props} isMobile={true} />,
  );

  createSetList.find('#show-choose').simulate('click');

  expect(props.showChooseSong).toHaveBeenCalled();
});

test('it hides song chooser when icon is clicked on mobile', () => {
  const createSetList = shallow(
    <CreateSetList {...props} isMobile={true} isChoosingSong={true} />,
  );

  createSetList.find('#hide-choose').simulate('click');

  expect(props.hideChooseSong).toHaveBeenCalled();
});