import React from 'react';

import MockData from '../../../../utils/test/MockData';
import ConfigureSetlist from '../ConfigureSetlist';

let props;

beforeEach(() => {
  props = {
    getScrollContainer: jest.fn(),
    onSortStart: jest.fn(),
    onSortEnd: jest.fn(),
    onSaveSetlist: jest.fn(),
    showTuningInstructions: jest.fn(),
    sortByEfficiency: jest.fn(),
    sortByTuning: jest.fn(),
    sortByArtist: jest.fn(),
    view: jest.fn(),
    remove: jest.fn(),
    setlist: MockData.mockSetlists[0],
    setlistHasChanged: false,
    disableFlipMove: false,
  };
})

test('it renders', () => {
  const configureSetlist = shallow(
    <ConfigureSetlist {...props} />,
  );

  expect(configureSetlist).toMatchSnapshot();
});