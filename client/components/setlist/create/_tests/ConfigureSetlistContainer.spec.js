import React from 'react';

import MockData from '../../../../utils/test/MockData';
import { ConfigureSetlistContainer } from '../ConfigureSetlistContainer';

let props;

beforeEach(() => {
  props = {
    requestSongs: jest.fn(),
    clearSongs: jest.fn(),
    selectSong: jest.fn(),
    onSaveSetlist: jest.fn(),
    createSetlist: jest.fn(),
    setSetlistSongs: jest.fn(),
    repositionSetlistItem: jest.fn(),
    removeFromSetlist: jest.fn(),
    push: jest.fn(),
    setlist: MockData.mockSetlists[0],
    setlistHasChanged: false,
    isLoading: false,
  };
});

test('it renders', () => {
  const configureSetlistContainer = shallow(
    <ConfigureSetlistContainer {...props} />,
  );

  expect(configureSetlistContainer).toMatchSnapshot();
});

test('it enables flipmove and dispatches reposition setlist item action when user changes setlist order', () => {
  const configureSetlistContainer = shallow(
    <ConfigureSetlistContainer {...props} />,
  );

  configureSetlistContainer.instance().onSortEnd({ oldIndex: 0, newIndex: 1 });

  expect(configureSetlistContainer.instance().state.disableFlipMove).toBeFalsy();
  expect(props.repositionSetlistItem).toHaveBeenCalledWith(0,1);
});

test('it disables flipmove when user starts changing setlist order', () => {
  const configureSetlistContainer = shallow(
    <ConfigureSetlistContainer {...props} />,
  );

  configureSetlistContainer.instance().onSortStart();

  expect(configureSetlistContainer.instance().state.disableFlipMove).toBeTruthy();
});

test('it calls onSaveSetlist prop when it exists', () => {
  const configureSetlistContainer = shallow(
    <ConfigureSetlistContainer {...props} />,
  );

  configureSetlistContainer.instance().onSaveSetlist();

  expect(props.onSaveSetlist).toHaveBeenCalled();
});

test('it updates existing setlist by calling createSetlist with an id', () => {
  const configureSetlistContainer = shallow(
    <ConfigureSetlistContainer {...props} onSaveSetlist={null} />,
  );

  configureSetlistContainer.instance().onSaveSetlist();

  expect(props.createSetlist).toHaveBeenCalledWith(MockData.mockSetlists[0], MockData.mockSetlists[0]._id);
});

test('sets whether flip move is disabled', () => {
  const configureSetlistContainer = shallow(
    <ConfigureSetlistContainer {...props} onSaveSetlist={null} />,
  );

  configureSetlistContainer.instance().setFlipMoveIsDisabled(true);
  expect(configureSetlistContainer.instance().state.disableFlipMove).toBeTruthy();

  configureSetlistContainer.instance().setFlipMoveIsDisabled(false);
  expect(configureSetlistContainer.instance().state.disableFlipMove).toBeFalsy();
});

test('redirects users to song page of a set list item', () => {
  const configureSetlistContainer = shallow(
    <ConfigureSetlistContainer {...props} onSaveSetlist={null} />,
  );

  configureSetlistContainer.instance().viewSetlistSong(MockData.mockSetlists[0].songs[0]);

  expect(props.selectSong).toHaveBeenCalled();
  expect(props.push).toHaveBeenCalled();
});

test('removes a song from the setlist', () => {
  const configureSetlistContainer = shallow(
    <ConfigureSetlistContainer {...props} onSaveSetlist={null} />,
  );

  configureSetlistContainer.instance().removeSetlistSong(MockData.mockSetlists[0].songs[0]);

  expect(props.removeFromSetlist).toHaveBeenCalled();
});