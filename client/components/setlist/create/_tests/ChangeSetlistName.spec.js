import React from 'react';

import { ChangeSetlistName } from '../ChangeSetlistName';

let props;

beforeEach(() => {
  props = {
    isAddingName: true,
    closeModal: jest.fn(),
    handleSubmit: jest.fn(),
  };
})

test('it renders', () => {
  const changeSetlistName = shallow(
    <ChangeSetlistName {...props} />,
  );

  expect(changeSetlistName).toMatchSnapshot();
});

test('it closes modal when Cancel button is clicked', () => {
  const changeSetlistName = shallow(
    <ChangeSetlistName {...props} />,
  );

  changeSetlistName.find("#cancel").simulate('click');

  expect(props.closeModal).toHaveBeenCalled();
});

test('it calls prop when form is submitted', () => {
  const changeSetlistName = shallow(
    <ChangeSetlistName {...props} />
  );

  changeSetlistName.find('form').props().onSubmit();

  expect(props.handleSubmit).toHaveBeenCalled();
});