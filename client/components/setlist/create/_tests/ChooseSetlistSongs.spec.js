import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

import MockData from '../../../../utils/test/MockData';
import ChooseSetlistSongs from '../ChooseSetlistSongs';
import { Text } from '../../../general';

let props;

beforeEach(() => {
  props = {
    onSearch: jest.fn(),
    onSongClick: jest.fn(),
    songs: MockData.mockSongs,
    isMobile: true,
  };
})

test('it renders', () => {
  const chooseSetlistSongs = shallow(
    <ChooseSetlistSongs {...props} />,
  );

  expect(chooseSetlistSongs).toMatchSnapshot();
});

test('it renders scrollbars when not on mobile', () => {
  const chooseSetlistSongs = shallow(
    <ChooseSetlistSongs {...props} isMobile={false} />,
  );

  expect(chooseSetlistSongs.find(Scrollbars)).toHaveLength(1);
});

test('it shows a help message before songs are searched for', () => {
  props.songs = [];
  const chooseSetlistSongs = shallow(
    <ChooseSetlistSongs {...props} />,
  );

  expect(chooseSetlistSongs.find(Text)).toHaveLength(1);
});