import React from 'react';
import sinon from 'sinon';

import MockData from '../../../../utils/test/MockData';
import { CreateSetListContainer } from '../CreateSetListContainer';

let props;

beforeEach(() => {
  props = {
    isLoadingSetlist: false,
    setlistHasChanged: false,
    isAddingName: false,
    requestSongs: jest.fn(),
    clearSongs: jest.fn(),
    isAddingSetlistName: jest.fn(),
    requestSetlist: jest.fn(),
    setNewSetlist: jest.fn(),
    createSetlist: jest.fn(),
    clearSelectedSetlist: jest.fn(),
    addToSetlist: jest.fn(),
    setSetlistName: jest.fn(),
    match: {
      params: {
        id: MockData.mockSetlists[0]._id,
      },
      path: "",
      url: "",
    },
    setlist: MockData.mockSetlists[0],
    songs: MockData.mockSongs,
  };
});

test('it renders', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
  );

  expect(createSetlistContainer).toMatchSnapshot();
});

test('it renders only the loader if there is no selected setlist', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} setlist={null} />,
  );

  expect(createSetlistContainer).toMatchSnapshot();
});

test('it renders only the loader if setlist is being fetched', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} isLoadingSetlist={true} />,
  );

  expect(createSetlistContainer).toMatchSnapshot();
});


test('it requests the setlist if not passed as prop', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} setlist={null} />,
    { lifecycleExperimental: true }
  );

  expect(props.requestSetlist).toHaveBeenCalled();
});

test('it requests the setlist if setlist passed as prop has different id to path param', () => {
  const setlistWithDifferntId = {
    ...MockData.mockSetlists[0],
    _id: 123,
  };

  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} setlist={setlistWithDifferntId}/>,
    { lifecycleExperimental: true }
  );

  expect(props.requestSetlist).toHaveBeenCalled();
});

test('it adds and removes event listeners for window resize', () => {
  const mockAddEventListener = sinon.spy(window, 'addEventListener');
  const mockremoveEventListener = sinon.spy(window, 'removeEventListener');

  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
    { lifecycleExperimental: true }
  );

  expect(mockAddEventListener.called).toBeTruthy();
  createSetlistContainer.unmount();
  expect(mockremoveEventListener.called).toBeTruthy();
});

test('it sets isMobile state to true when window size is small enough', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
  );
  
  window.innerWidth= 300;
  createSetlistContainer.instance().handleWindowSizeChange();

  expect(createSetlistContainer.instance().state.isMobile).toBeTruthy();
});

test('it sets isMobile state to false when window size is big enough', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
  );
  
  window.innerWidth= 800;
  createSetlistContainer.instance().handleWindowSizeChange();

  expect(createSetlistContainer.instance().state.isMobile).toBeFalsy();
});

test('it dispatchs is adding setlist name action with value false when edit name modal is closed', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
  );
  
  createSetlistContainer.instance().closeModal();

  expect(props.isAddingSetlistName).toHaveBeenCalledWith(false);
});

test('it sets isChoosingSong state to false when hideChooseSong is called', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
  );
  
  createSetlistContainer.instance().hideChooseSong();

  expect(createSetlistContainer.instance().state.isChoosingSong).toBeFalsy();
});

test('it sets isChoosingSong state to true when showChooseSong is called', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
  );
  
  createSetlistContainer.instance().showChooseSong();

  expect(createSetlistContainer.instance().state.isChoosingSong).toBeTruthy();
});

test('it sets isChoosingSong state to false and dispatches addToSetlist action after song is chosen', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
  );
  
  createSetlistContainer.instance().onSongClick(MockData.mockSongs[0]);

  expect(createSetlistContainer.instance().state.isChoosingSong).toBeFalsy();
  expect(props.addToSetlist).toHaveBeenCalledWith(MockData.mockSongs[0]);
});

test('it dispatches requestSongs action when onSearch is called', () => {
  const createSetlistContainer = shallow(
    <CreateSetListContainer {...props} />,
  );
  
  createSetlistContainer.instance().onSearch({ searchValue: "mockSearchValue" });

  expect(props.requestSongs).toHaveBeenCalledWith({"query": "name~mockSearchValue"});
});