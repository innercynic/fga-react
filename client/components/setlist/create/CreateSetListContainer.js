import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReactRouterPropTypes from 'react-router-prop-types';
import PropTypes from 'prop-types';

import { SetlistShape, SongShape } from '../../../proptypes';
import { requestSongs, clearSongs } from '../../../actions/songs/songs';
import { sizes } from '../../../styles/media';

import {
  addToSetlist,
  requestSetlist,
  clearSelectedSetlist,
  createSetlist,
  setNewSetlist,
  setSetlistName,
  isAddingSetlistName,
} from '../../../actions/setlists/setlist';

import CreateSetList from './CreateSetList';
import { FlexExtended, LoaderWithOverlay } from '../../general';

const SMALL_SCREEN_WIDTH = sizes.tabletPortrait;

export class CreateSetListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: window.innerWidth < SMALL_SCREEN_WIDTH,
      isChoosingSong: false,
    };

    this.showChooseSong = this.showChooseSong.bind(this);
    this.hideChooseSong = this.hideChooseSong.bind(this);
    this.onSongClick = this.onSongClick.bind(this);
    this.onSearch = this.onSearch.bind(this);

    this.onSaveSetlist = this.onSaveSetlist.bind(this);

    this.editName = this.editName.bind(this);
    this.saveName = this.saveName.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
  }

  componentWillMount() {
    if (!this.props.match) return;

    if (!this.props.match.params.id) {
      this.props.setNewSetlist();
    } else {
      const setlistId = this.props.match.params.id;

      if (!this.props.setlist || this.props.setlist._id !== setlistId) {
        this.props.clearSelectedSetlist();
        this.props.requestSetlist({ id: setlistId });
      }
    }

    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange() {
    if (this.state.isMobile !== (window.innerWidth < SMALL_SCREEN_WIDTH)) {
      this.setState({
        isMobile: window.innerWidth < SMALL_SCREEN_WIDTH,
      });
    }
  }

  closeModal() {
    this.props.isAddingSetlistName(false);
  }

  saveName({ name }) {
    this.props.setSetlistName(name);
    this.closeModal();
  }

  onSaveSetlist() {
    if (this.props.setlist._id) {
      this.props.createSetlist(this.props.setlist, this.props.setlist._id);
    } else if (!this.props.setlist.name) {
      this.props.isAddingSetlistName(true);
    } else {
      this.props.createSetlist(this.props.setlist);
    }
  }

  editName() {
    this.props.isAddingSetlistName(true);
  }

  showChooseSong() {
    this.setState({
      isChoosingSong: true,
    });
  }

  hideChooseSong() {
    this.setState({
      isChoosingSong: false,
    });
  }

  onSongClick(item) {
    this.setState({
      isChoosingSong: false,
    }, () => {
      this.props.addToSetlist(item);
    });
  }

  onSearch({ searchValue }) {
    this.props.requestSongs({ query: `name~${searchValue}` });
  }

  get viewProps() {
    return {
      songs: this.props.songs,
      onSongClick: this.onSongClick,
      onSearch: this.onSearch,

      setlist: this.props.setlist,
      setlistHasChanged: this.props.setlistHasChanged,
      onSaveSetlist: this.onSaveSetlist,

      editName: this.editName,
      isAddingName: this.props.isAddingName,
      onChangeName: this.saveName,
      closeModal: this.closeModal,
      initialValues: { name: this.props.setlist.name || '' },

      isMobile: this.state.isMobile,
      isChoosingSong: this.state.isChoosingSong,
      showChooseSong: this.showChooseSong,
      hideChooseSong: this.hideChooseSong,
    };
  }

  render() {
    if (this.props.isLoadingSetlist || !this.props.setlist) {
      return (
        <FlexExtended height={250}>
          <LoaderWithOverlay isLoading={this.props.isLoadingSetlist} />
        </FlexExtended>
      );
    }

    return (
      <CreateSetList {...this.viewProps} />
    );
  }
}

function mapStateToProps(state) {
  return {
    setlist: state.setlist.selected,
    songs: state.songs.data,
    isLoadingSetlist: state.setlist.isFetching,
    isAddingName: state.setlist.isAddingSetlistName,
    setlistHasChanged: state.setlist.changed,
    // nextPage: state.songs.nextPage,
    // hasMorePages: state.songs.hasMore,
    // currentQuery: state.songs.currentQuery,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    requestSongs,
    clearSongs,
    isAddingSetlistName,
    requestSetlist,
    setNewSetlist,
    createSetlist,
    clearSelectedSetlist,
    addToSetlist,
    setSetlistName,
  }, dispatch);
}

CreateSetListContainer.defaultProps = {
  currentQuery: '',
  setlist: null,
};

CreateSetListContainer.propTypes = {
  isLoadingSetlist: PropTypes.bool.isRequired,
  setlistHasChanged: PropTypes.bool.isRequired,
  isAddingName: PropTypes.bool.isRequired,
  requestSongs: PropTypes.func.isRequired,
  clearSongs: PropTypes.func.isRequired,
  isAddingSetlistName: PropTypes.func.isRequired,
  requestSetlist: PropTypes.func.isRequired,
  setNewSetlist: PropTypes.func.isRequired,
  createSetlist: PropTypes.func.isRequired,
  clearSelectedSetlist: PropTypes.func.isRequired,
  addToSetlist: PropTypes.func.isRequired,
  setSetlistName: PropTypes.func.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
  setlist: SetlistShape,
  songs: PropTypes.arrayOf(SongShape).isRequired,
  // nextPage: PropTypes.number.isRequired,
  // currentQuery: PropTypes.string,
};

export default connect(mapStateToProps, matchDispatchToProps)(CreateSetListContainer);
