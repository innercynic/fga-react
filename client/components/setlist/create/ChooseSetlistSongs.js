import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import { Scrollbars } from 'react-custom-scrollbars';
import List from '../../list/List';

import { SongShape } from '../../../proptypes';
import SearchForm from '../../search/SearchForm';
import SongListItem from '../../song/list/SongListItem';
import { Text } from '../../general';

const ChooseSetlistSongs = props => (
  <Flex column width={[1, 1, 1 / 2]} mr={10}>
    <Box mb={10}>
      <SearchForm onSubmit={props.onSearch} />
    </Box>

    {props.songs.length < 1 &&
    <Box>
      <Text>Search for songs to add to your setlist</Text>
    </Box>
    }

    <Box flex={1}>
      {!props.isMobile &&
      <Scrollbars>
        <Box px={10}>
          <List
            items={props.songs}
            onItemClick={props.onSongClick}
            itemKey="_id"
            getItemContent={song => <SongListItem song={song} />}
          />
        </Box>
      </Scrollbars>
      }
      {props.isMobile &&
      <Box px={10}>
        <List
          items={props.songs}
          onItemClick={props.onSongClick}
          itemKey="_id"
          getItemContent={song => <SongListItem song={song} />}
        />
      </Box>
      }
    </Box>
  </Flex>
);

ChooseSetlistSongs.propTypes = {
  onSearch: PropTypes.func.isRequired,
  onSongClick: PropTypes.func.isRequired,
  songs: PropTypes.arrayOf(SongShape).isRequired,
  isMobile: PropTypes.bool.isRequired,
};

export default ChooseSetlistSongs;
