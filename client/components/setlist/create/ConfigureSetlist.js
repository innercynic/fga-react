import React from 'react';
import PropTypes from 'prop-types';
import { Box } from 'grid-styled';

import { SetlistShape } from '../../../proptypes';
import { SortableList } from '../../list/List';
import SetlistSong from '../list/SetlistSong';
import SetlistMenuContainer from '../menu/SetlistMenuContainer';

const ConfigureSetlist = props => (
  <Box width={[1, 1, 1 / 2]}>
    <SetlistMenuContainer
      setlist={props.setlist}
      sortByEfficiency={props.sortByEfficiency}
      sortByName={props.sortByName}
      sortByTuning={props.sortByTuning}
      sortByArtist={props.sortByArtist}
      onSaveSetlist={props.onSaveSetlist}
      showTuningInstructions={props.showTuningInstructions}
      setlistHasChanged={props.setlistHasChanged}
    />

    <SortableList
      items={props.setlist.songs}
      itemKey="_id"
      getItemContent={song =>
        <SetlistSong song={song} remove={props.remove} view={props.view} />
      }
      onSortEnd={props.onSortEnd}
      onSortStart={props.onSortStart}
      disableFlipMove={props.disableFlipMove}
      getContainer={props.getScrollContainer}
      useDragHandle
    />
  </Box>
);

ConfigureSetlist.propTypes = {
  getScrollContainer: PropTypes.func.isRequired,
  onSortStart: PropTypes.func.isRequired,
  onSortEnd: PropTypes.func.isRequired,
  onSaveSetlist: PropTypes.func.isRequired,
  showTuningInstructions: PropTypes.func.isRequired,
  sortByEfficiency: PropTypes.func.isRequired,
  sortByTuning: PropTypes.func.isRequired,
  sortByArtist: PropTypes.func.isRequired,
  view: PropTypes.func.isRequired,
  remove: PropTypes.func.isRequired,
  setlist: SetlistShape.isRequired,
  setlistHasChanged: PropTypes.bool.isRequired,
  disableFlipMove: PropTypes.bool.isRequired,
};

export default ConfigureSetlist;
