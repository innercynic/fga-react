import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import { reduxForm } from 'redux-form';

import validate from '../../../validation/setlist';
import { Heading, Button } from '../../general';
import { Label } from '../../form/fields';
import { RFInput, RFFieldError } from '../../form/reduxformfields';
import OverlayModal from '../../modals/OverlayModal';

export const ChangeSetlistName = props => (
  <OverlayModal isOpen={props.isAddingName} onRequestClose={props.closeModal} maxWidth={400}>
    <Flex column p={[3, 3, 4, 4]} align="center">
      <Box my={5}>
        <Heading>Name your setlist</Heading>
      </Box>
      <Box my={5}>
        <form onSubmit={props.handleSubmit}>
          <Box mb={5}>
            <Label htmlFor="name">Name</Label>
            <RFFieldError name="name" />
          </Box>
          <Box mb={10}>
            <RFInput name="name" />
          </Box>
          <Box mb={10}>
            <Button id="save-name" type="submit" width="100%">Save</Button>
          </Box>
        </form>
      </Box>
      <Flex my={10}>
        <Button
          id="cancel"
          width="100%"
          onClick={props.closeModal}
        >
            Cancel
        </Button>
      </Flex>
    </Flex>
  </OverlayModal>
);

ChangeSetlistName.propTypes = {
  isAddingName: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'setlist',
  validate,
})(ChangeSetlistName);
