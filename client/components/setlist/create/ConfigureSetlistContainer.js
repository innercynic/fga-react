import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';

import { SetlistShape } from '../../../proptypes';
import { selectSong } from '../../../actions/songs/song';

import {
  repositionSetlistItem,
  setSetlistSongs,
  createSetlist,
  removeFromSetlist,
  sortSetlist,
  SortTypes,
} from '../../../actions/setlists/setlist';

import ConfigureSetlist from './ConfigureSetlist';

export class ConfigureSetlistContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disableFlipMove: false,
    };

    this.onSortEnd = this.onSortEnd.bind(this);
    this.onSortStart = this.onSortStart.bind(this);
    this.sortByEfficiency = this.sortByEfficiency.bind(this);
    this.sortByNameAlphabetically = this.sortByNameAlphabetically.bind(this);
    this.sortByTuningAlphabetically = this.sortByTuningAlphabetically.bind(this);
    this.sortByArtist = this.sortByArtist.bind(this);
    this.onSaveSetlist = this.onSaveSetlist.bind(this);
    this.setFlipMoveIsDisabled = this.setFlipMoveIsDisabled.bind(this);
    this.showTuningInstructions = this.showTuningInstructions.bind(this);
    this.viewSetlistSong = this.viewSetlistSong.bind(this);
    this.removeSetlistSong = this.removeSetlistSong.bind(this);
  }

  onSortEnd({ oldIndex, newIndex }) {
    this.props.repositionSetlistItem(oldIndex, newIndex);
    this.setFlipMoveIsDisabled(false);
  }

  onSortStart() {
    this.setFlipMoveIsDisabled(true);
  }

  onSaveSetlist() {
    if (this.props.onSaveSetlist) {
      this.props.onSaveSetlist();
    } else if (this.props.setlist._id) {
      this.props.createSetlist(this.props.setlist, this.props.setlist._id);
    }
  }

  getScrollContainer() {
    return document.querySelectorAll('#scroller')[0].firstChild;
  }

  setFlipMoveIsDisabled(isDisabled) {
    this.setState({
      disableFlipMove: isDisabled,
    });
  }

  sortByEfficiency() {
    this.props.sortSetlist(this.props.setlist.songs, SortTypes.BY_TUNING_EFFICIENCY);
  }

  sortByNameAlphabetically() {
    this.props.sortSetlist(this.props.setlist.songs, SortTypes.BY_NAME_ALPHABETICALLY);
  }

  sortByTuningAlphabetically() {
    this.props.sortSetlist(this.props.setlist.songs, SortTypes.BY_TUNING_ALPHABETICALLY);
  }

  sortByArtist() {
    this.props.sortSetlist(this.props.setlist.songs, SortTypes.BY_ARTIST);
  }

  showTuningInstructions() {
    this.props.push(`/setlists/${this.props.setlist._id}/tuninginstructions`);
  }

  viewSetlistSong(song) {
    this.props.selectSong(song);
    this.props.push(`/songs/${song._id}`);
  }

  removeSetlistSong(song) {
    this.props.removeFromSetlist(song);
  }

  get viewProps() {
    return {
      disableFlipMove: this.state.disableFlipMove,
      onSortStart: this.onSortStart,
      onSortEnd: this.onSortEnd,
      getScrollContainer: this.getScrollContainer,
      setlist: this.props.setlist,
      sortByEfficiency: this.sortByEfficiency,
      sortByName: this.sortByNameAlphabetically,
      sortByTuning: this.sortByTuningAlphabetically,
      sortByArtist: this.sortByArtist,
      onSaveSetlist: this.onSaveSetlist,
      showTuningInstructions: this.showTuningInstructions,
      remove: this.removeSetlistSong,
      view: this.viewSetlistSong,
      setlistHasChanged: this.props.setlistHasChanged,
    };
  }

  render() {
    return (
      <ConfigureSetlist {...this.viewProps} />
    );
  }
}

function mapStateToProps(state) {
  return {
    setlist: state.setlist.selected,
    setlistHasChanged: state.setlist.changed,
    isLoadingSetlist: state.setlist.isFetching,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    createSetlist,
    setSetlistSongs,
    repositionSetlistItem,
    removeFromSetlist,
    sortSetlist,
    selectSong,
    push,
  }, dispatch);
}

ConfigureSetlistContainer.defaultProps = {
  onSaveSetlist: null,
};

ConfigureSetlistContainer.propTypes = {
  requestSongs: PropTypes.func.isRequired,
  clearSongs: PropTypes.func.isRequired,
  selectSong: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onSaveSetlist: PropTypes.func,
  createSetlist: PropTypes.func.isRequired,
  setSetlistSongs: PropTypes.func.isRequired,
  repositionSetlistItem: PropTypes.func.isRequired,
  removeFromSetlist: PropTypes.func.isRequired,
  setlist: SetlistShape.isRequired,
  setlistHasChanged: PropTypes.bool.isRequired,
  push: PropTypes.func.isRequired,

};

export default connect(mapStateToProps, matchDispatchToProps)(ConfigureSetlistContainer);
