import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import { Prompt } from 'react-router-dom';

import { SetlistShape, SongShape } from '../../../proptypes';

import { Container, SubHeading, Icon } from '../../general';
import ChangeSetlistName from './ChangeSetlistName';
import ChooseSetlistSongs from './ChooseSetlistSongs';
import ConfigureSetlistContainer from './ConfigureSetlistContainer';

const CreateSetList = props => (
  <Container column flex={1} width={1} maxWidth={700} p={[3, 3, 4, 4]} minHeight={400}>
    <Prompt when={props.setlistHasChanged} message="Leave without saving changes?" />

    <Box width={[1]} mb={10}>
      {props.setlist.name ?
        <Flex align="center">
          <SubHeading>{props.setlist.name}</SubHeading>
          <Flex ml={10} mt={5} align="center">
            <Icon id="edit-name" clickable icon="edit" size="large" onClick={props.editName} />
          </Flex>
          {props.isMobile && !props.isChoosingSong &&
          <Flex ml={20} mt={5} align="center">
            <Icon
              id="show-choose"
              clickable
              icon="listadd"
              size="large"
              onClick={props.showChooseSong}
            />
          </Flex>
          }
          {props.isMobile && props.isChoosingSong &&
          <Flex ml={20} mt={5} align="center">
            <Icon
              id="hide-choose"
              clickable
              icon="back"
              size="large"
              onClick={props.hideChooseSong}
            />
          </Flex>
          }

        </Flex>
        :
        <SubHeading>New Setlist</SubHeading>
      }
    </Box>

    <Flex flex={1}>
      {((props.isMobile && props.isChoosingSong) || !props.isMobile) &&
        <ChooseSetlistSongs
          onSearch={props.onSearch}
          songs={props.songs}
          isMobile={props.isMobile}
          onSongClick={props.onSongClick}
        />
      }

      {((props.isMobile && !props.isChoosingSong) || !props.isMobile) &&
        <ConfigureSetlistContainer
          onSaveSetlist={props.onSaveSetlist}
        />
      }
    </Flex>

    <ChangeSetlistName
      isAddingName={props.isAddingName}
      closeModal={props.closeModal}
      initialValues={props.initialValues}
      onSubmit={props.onChangeName}
    />
  </Container>
);

CreateSetList.defaultProps = {
  initialValues: {},
};

CreateSetList.propTypes = {
  setlist: SetlistShape.isRequired,
  songs: PropTypes.arrayOf(SongShape).isRequired,
  isMobile: PropTypes.bool.isRequired,
  isChoosingSong: PropTypes.bool.isRequired,
  isAddingName: PropTypes.bool.isRequired,
  setlistHasChanged: PropTypes.bool.isRequired,
  onChangeName: PropTypes.func.isRequired,
  onSaveSetlist: PropTypes.func.isRequired,
  onSongClick: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  showChooseSong: PropTypes.func.isRequired,
  hideChooseSong: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
  editName: PropTypes.func.isRequired,
  initialValues: PropTypes.objectOf(PropTypes.string),
};

export default CreateSetList;
