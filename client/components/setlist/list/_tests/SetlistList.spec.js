import React from 'react';

import MockData from '../../../../utils/test/MockData';
import { SetlistList } from '../SetlistList';

let props;

beforeEach(() => {
  props = {
    requestSetlists: jest.fn(),
    selectSetlist: jest.fn(),
    push: jest.fn(),
    isLoading: false,
    setlists: MockData.mockSetlists,
  };
})

test('it renders', () => {
  const setlistList = shallow(
    <SetlistList {...props} />,
  );

  expect(setlistList).toMatchSnapshot();
});

test('it loads setlists on mounting', () => {
  const setlistList = shallow(
    <SetlistList {...props} />,
    {lifecycleExperimental: true}
  );

  expect(props.requestSetlists).toHaveBeenCalled();
});

test('it redirects to create new setlist page', () => {
  const setlistList = shallow(
    <SetlistList {...props} />,
  );

  setlistList.find("#new-setlist").simulate('click');

  expect(props.push).toHaveBeenCalledWith("/setlists/new");
});

test('it redirects edit/view setlist page', () => {
  const setlistList = shallow(
    <SetlistList {...props} />,
  );

  setlistList.instance().onSetlistClick(MockData.mockSetlists[0]);

  expect(props.selectSetlist).toHaveBeenCalledWith(MockData.mockSetlists[0]);
  expect(props.push).toHaveBeenCalledWith(`/setlists/${MockData.mockSetlists[0]._id}`);
});