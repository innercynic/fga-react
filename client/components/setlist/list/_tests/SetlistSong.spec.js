import React from 'react';

import MockData from '../../../../utils/test/MockData';
import SetlistSong from '../SetlistSong';

let props;

beforeEach(() => {
  props = {
    song: MockData.mockSongs[0],
    view: jest.fn(),
    remove: jest.fn(),
  };
})

test('it renders', () => {
  const setlistSong = shallow(
    <SetlistSong {...props} />,
  );

  expect(setlistSong).toMatchSnapshot();
});

test('it toggles whether menu is open', () => {
  const setlistSong = shallow(
    <SetlistSong {...props} />,
  );

  setlistSong.instance().toggleMenu();

  expect(setlistSong.instance().state.menuIsOpen).toBeTruthy();
});

test('it calls view song prop', () => {
  const setlistSong = shallow(
    <SetlistSong {...props} />,
  );

  setlistSong.instance().viewSong();

  expect(props.view).toHaveBeenCalled();
});

test('it calls remove song prop', () => {
  const setlistSong = shallow(
    <SetlistSong {...props} />,
  );

  setlistSong.instance().removeSong();

  expect(props.remove).toHaveBeenCalled();
});