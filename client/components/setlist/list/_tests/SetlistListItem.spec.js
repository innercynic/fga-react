import React from 'react';

import MockData from '../../../../utils/test/MockData';
import SetlistListItem from '../SetlistListItem';

let props;

beforeEach(() => {
  props = {
    setlistItem: MockData.mockSetlists[0],
  };
})

test('it renders', () => {
  const setlistListItem = shallow(
    <SetlistListItem {...props} />,
  );

  expect(setlistListItem).toMatchSnapshot();
});