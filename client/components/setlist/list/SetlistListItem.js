import React from 'react';

import { SetlistShape } from '../../../proptypes';
import { Container, Text } from '../../general';

const SetlistListItem = props => (
  <Container forContentItem clickable p={10} mb={10}>
    <Text>{props.setlistItem.name}</Text>
  </Container>
);

SetlistListItem.propTypes = {
  setlistItem: SetlistShape.isRequired,
};

export default SetlistListItem;
