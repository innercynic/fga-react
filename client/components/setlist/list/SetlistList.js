import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Box } from 'grid-styled';
import PropTypes from 'prop-types';

import { SetlistShape } from '../../../proptypes';
import { Container, LoaderWithOverlay, Button, Heading } from '../../general';
import { requestSetlists } from '../../../actions/setlists/setlists';
import { selectSetlist } from '../../../actions/setlists/setlist';

import List from '../../list/List';
import SetlistListItem from './SetlistListItem';


export class SetlistList extends Component {
  constructor(props) {
    super(props);

    this.onSetlistClick = this.onSetlistClick.bind(this);
    this.onNewSetlist = this.onNewSetlist.bind(this);
  }

  componentWillMount() {
    this.props.requestSetlists({});
  }

  onNewSetlist() {
    this.props.push('/setlists/new');
  }

  onSetlistClick(setlist) {
    this.props.selectSetlist(setlist);
    this.props.push(`/setlists/${setlist._id}`);
  }

  render() {
    return (
      <Container column flex={1} width={1} maxWidth={300} p={[3, 3, 4, 4]}>
        <Box mb={[1, 1, 2, 3]}>
          <Heading>My Setlists</Heading>
        </Box>
        <LoaderWithOverlay fullscreen isLoading={this.props.isLoading} />

        <List
          items={this.props.setlists}
          onItemClick={this.onSetlistClick}
          itemKey="_id"
          getItemContent={setlistItem => <SetlistListItem setlistItem={setlistItem} />}
        />
        <Box p={10}>
          <Button id="new-setlist" width="100%" onClick={this.onNewSetlist}>New Setlist</Button>
        </Box>


      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    setlists: state.setlists.data,
    isLoading: state.setlists.isFetching,
    nextPage: state.setlists.nextPage,
    hasMorePages: state.setlists.hasMore,
    currentQuery: state.setlists.currentQuery,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    requestSetlists,
    selectSetlist,
    push,
  }, dispatch);
}

SetlistList.propTypes = {
  requestSetlists: PropTypes.func.isRequired,
  selectSetlist: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  setlists: PropTypes.arrayOf(SetlistShape).isRequired,
};

export default connect(mapStateToProps, matchDispatchToProps)(SetlistList);
