import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import ContextMenu from '../../menu/context/ContextMenu';
import { FlexExtended, Text, ImgAsBackground, Container, Icon } from '../../general';
import { SongShape } from '../../../proptypes';

class SongListSong extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      menuIsOpen: false,
    };

    this.toggleMenu = this.toggleMenu.bind(this);
    this.viewSong = this.viewSong.bind(this);
    this.removeSong = this.removeSong.bind(this);
  }

  toggleMenu() {
    this.setState({
      menuIsOpen: !this.state.menuIsOpen,
    });
  }

  viewSong() {
    this.props.view(this.props.song);
  }

  removeSong() {
    this.props.remove(this.props.song);
  }

  render() {
    return (

      <Container forContentItem mb={10} p={5} pointer onClick={this.toggleMenu}>
        <Flex ml={5} mr={15}>
          <ImgAsBackground url={this.props.song.artist.thumbnail} width={50} height={50} asCircle />
        </Flex>
        <Flex align="center">
          <Text>{this.props.song.name}</Text>
        </Flex>
        <ContextMenu
          isOpen={this.state.menuIsOpen}
          closeContextMenu={this.toggleMenu}
          offsetY={0}
          offsetX={0}
          arrowHeight={0}
          arrowOffset={60}
          isPositionedAbove
          width="100%"
          asRow
          rounded
        >
          <FlexExtended
            height={60}
            justify="center"
            align="center"
            flex="1"
            clickable
            menuItemClicked={this.viewSong}
            p={[2]}
          >
            <Box mr={[1]}><Icon icon="song" size="large" /></Box>
            <Text>View</Text>
          </FlexExtended>

          <FlexExtended
            height={60}
            justify="center"
            align="center"
            flex="1"
            clickable
            menuItemClicked={this.removeSong}
            p={[2]}
          >
            <Box mr={[1]}><Icon icon="remove" size="large" /></Box>
            <Text>Remove</Text>
          </FlexExtended>

        </ContextMenu>
      </Container>
    );
  }
}

SongListSong.propTypes = {
  song: SongShape.isRequired,
  view: PropTypes.func.isRequired,
  remove: PropTypes.func.isRequired,
};

export default SongListSong;

