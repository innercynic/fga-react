import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import { SortableHandle } from 'react-sortable-hoc';

import { Icon } from '../general';

const DragHandle = SortableHandle(() =>
  (<Flex align="center" pl={10}>
    <Icon clickable icon="sorthandle" size="huge" />
  </Flex>),
);

const ListItem = props => (
  <Flex
    width={props.itemBreakPoints}
    isSelected={props.isSelected}
    onClick={() => { props.onItemClick(props.item); }}
  >
    <Box flex="1">
      { props.getItemContent(props.item) }
    </Box>
    {props.sortable &&
      <DragHandle />
    }
  </Flex>
);

ListItem.defaultProps = {
  isSelected: null,
  onItemClick() {},
  itemBreakPoints: 1,
  sortable: false,
};

ListItem.propTypes = {
  isSelected: PropTypes.bool,
  sortable: PropTypes.bool,
  onItemClick: PropTypes.func,
  getItemContent: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  itemBreakPoints: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.number), PropTypes.number]),
};

export default ListItem;
