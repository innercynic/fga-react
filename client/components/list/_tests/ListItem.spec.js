import React from 'react'

import ListItem from '../ListItem';

let baseProps;

beforeEach(() => {
  baseProps = {
    item: {
      name: "Dave",
      age: 13,
    },
    getItemContent: function(item) {
      return <div>{item.name} - {item.age}</div>
    },
  }
});

test('it renders', () => {
  const listItem = shallow(
      <ListItem {...baseProps} />
  );

  expect(listItem).toMatchSnapshot();
});

test('it calls the onItemClick prop when clicked', () => {
  const mockItemClickHandler = jest.fn();
  const listItem = shallow(
      <ListItem {...baseProps} onItemClick={mockItemClickHandler} />
  );

  listItem.simulate('click');

  expect(mockItemClickHandler).toHaveBeenCalled();
});

