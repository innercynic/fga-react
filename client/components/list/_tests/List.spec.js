import React from 'react'
import 'jest-styled-components'

import List from '../List';
import ListItem from '../ListItem';

let props;

beforeEach(() => {
  props = {
    onItemClick: jest.fn(),
    isSelected: false,
    itemKey: "id",
    items: [{
      id: 1,
      name: "Dave",
      age: 13,
    },{
      id: 2,
      name: "Bob",
      age: 80,
    }],
    getItemContent: function(item){
      return <div>{item.name} - {item.age}</div>
    },
  }
});

test('it renders', () => {
  const list = shallow(
      <List {...props} />
  );

  expect(list).toMatchSnapshot();
});

test('it renders its list items', () => {
  const list = shallow(
      <List {...props} />
  );

  expect(list.find(ListItem)).toHaveLength(2);
});

test('it renders as a grid (multiple elements per line)', () => {
  const list = shallow(
      <List {...props} displayAsGrid />
  );

  expect(list).toMatchSnapshot();
});
