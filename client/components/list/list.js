import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import FlipMove from 'react-flip-move';

import ListItem from './ListItem';

const FlexFlipMove = Flex.withComponent(FlipMove);

const List = (props) => {
  const Item = !props.sortable ? ListItem : SortableElement(ListItem);
  return (
    <FlexFlipMove
      wrap={props.displayAsGrid}
      column={!props.displayAsGrid}
      width="100%"
      justify={props.displayAsGrid ? 'center' : 'flex-start'}
      duration={250}
      easing="ease-out"
      disableAllAnimations={props.disableFlipMove}
    >
      {props.items.map((item, index) =>
        (<Item
          key={item[props.itemKey]}
          index={index}
          item={item}
          sortable={props.sortable}
          onItemClick={props.onItemClick}
          isSelected={props.selectedItem
            && props.selectedItem[props.itemKey] === item[props.itemKey]}

          getItemContent={props.getItemContent}
          itemBreakPoints={props.itemBreakPoints ? // eslint-disable-line no-nested-ternary
          props.itemBreakPoints : props.displayAsGrid
            ? [1, 1 / 2, 1 / 4, 1 / 5] : 1}
        />),
      )}
    </FlexFlipMove>
  );
};

List.defaultProps = {
  selectedItem: null,
  onItemClick() {},
  displayAsGrid: false,
  itemBreakPoints: null,
  disableFlipMove: true,
  sortable: false,
};

List.propTypes = {
  itemKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  selectedItem: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  items: PropTypes.array.isRequired, // eslint-disable-line react/forbid-prop-types
  getItemContent: PropTypes.func.isRequired,
  onItemClick: PropTypes.func,
  displayAsGrid: PropTypes.bool,
  itemBreakPoints: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.number), PropTypes.number]),
  sortable: PropTypes.bool,
  disableFlipMove: PropTypes.bool,
};

export const SortableList = SortableContainer(
  props => <List {...props} sortable />,
  { withRef: true },
);

export default List;
