import React from 'react';
import * as ReactRedux from 'react-redux';
import * as Redux from 'redux';
import sinon from 'sinon';
import ReactTestUtils from 'react-dom/test-utils';

import TestUtils from '../../../utils/test/TestUtils';
import { goBack } from 'react-router-redux';
import { openLogin } from '../../../actions/user/auth';

import { requireAuthentication } from '../RouteAuthenticationHOC';
import MockData from '../../../utils/test/MockData';

import LoginWarning from '../LoginWarning';

let state = {auth: {isLoggedIn: true}};

sinon.stub(Redux, 'bindActionCreators').callsFake((actions) => {
  return actions;
})

let stubbedConnect = sinon.stub(ReactRedux, 'connect').callsFake((mapStateToProps) => {
  let connectedPropsAndActions = {
    ...mapStateToProps({auth: {isLoggedIn: true}}),
  };

  return (ComposedComponent) => {
    return (props) => <ComposedComponent {...connectedPropsAndActions} {...props} />
  }
});

let ThemeProviderWrapper = TestUtils.getMockThemeProvider();

const RouteRequiringAuthentication = requireAuthentication(props => {
  return <div id="composed">COMPOSED</div>
});

test('it renders and displays composed component when logged in', () => {
  const authorisedRoute = mount(
      <RouteRequiringAuthentication />
  );

  expect(authorisedRoute).toMatchSnapshot();
  expect(authorisedRoute.find('#composed')).toHaveLength(1);
});