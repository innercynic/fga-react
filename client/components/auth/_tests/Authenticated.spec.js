import React from 'react';

import { AuthState } from '../Authenticated';


const IfAuthenticated = props => AuthState(props, true);
const IfNotAuthenticated = props => AuthState(props, false);

describe('IfAuthenticated', () => {
  test('it renders children when user is logged in', () => {

    const ifAuthenticated = shallow(
      <IfAuthenticated isLoggedIn={true}>
        <div id="child">CHILD</div>
      </IfAuthenticated>
    );

    expect(ifAuthenticated.find('#child')).toHaveLength(1);
  });

  test('it does not render children when user is not logged in', () => {

    const ifAuthenticated = shallow(
      <IfAuthenticated isLoggedIn={false}>
        <div id="child">CHILD</div>
      </IfAuthenticated>
    );

    expect(ifAuthenticated.find('#child')).toHaveLength(0);
  });
});

describe('IfNotAuthenticated', () => {
  test('it renders children when user is not logged in', () => {

    const ifAuthenticated = shallow(
      <IfNotAuthenticated isLoggedIn={false}>
        <div id="child">CHILD</div>
      </IfNotAuthenticated>
    );

    expect(ifAuthenticated.find('#child')).toHaveLength(1);
  });

  test('it does not render children when user is logged in', () => {

    const ifAuthenticated = shallow(
      <IfNotAuthenticated isLoggedIn={true}>
        <div id="child">CHILD</div>
      </IfNotAuthenticated>
    );

    expect(ifAuthenticated.find('#child')).toHaveLength(0);
  });
});

