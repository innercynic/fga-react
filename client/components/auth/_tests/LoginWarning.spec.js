import React from 'react';

import { LoginWarning } from '../LoginWarning';

let props;

beforeEach(() => {
  props = {
    openLogin: jest.fn(),
    goBack: jest.fn(),
  };
})

test('it renders', () => {
  const loginWarning = shallow(
    <LoginWarning {...props} />,
  );

  expect(loginWarning).toMatchSnapshot();
});

test('it calls open login prop when clicking Login Button', () => {
  const loginWarning = shallow(
    <LoginWarning {...props} />,
  );

  loginWarning.find('#login-button').simulate('click');

  expect(props.openLogin).toHaveBeenCalled();
});

test('it calls back prop when clicking Back Button', () => {
  const loginWarning = shallow(
    <LoginWarning {...props} />,
  );

  loginWarning.find('#back-button').simulate('click');

  expect(props.goBack).toHaveBeenCalled();
});

