import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import LoginWarning from './LoginWarning';


export const requireAuthentication = (ComposedComponent) => {
  const AuthenticationCheck = (props) => {
    if (props.loggedIn) {
      return (
        <ComposedComponent {...props} />
      );
    }

    return (
      <LoginWarning />
    );
  };

  AuthenticationCheck.propTypes = {
    loggedIn: PropTypes.bool.isRequired,
  };

  function mapStateToProps(state) {
    return {
      loggedIn: state.auth.isLoggedIn,
    };
  }


  return connect(mapStateToProps)(AuthenticationCheck);
};
