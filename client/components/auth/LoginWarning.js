import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import { goBack } from 'react-router-redux';
import { Container, Button, Text } from '../general';

import { openLogin } from '../../actions/user/auth';

export const LoginWarning = props => (
  <Container justify="center" flex={1} column maxWidth={400} p={[3, 3, 4, 4]} wrap="nowrap">
    <Box mb={10}>
      <Text>You must be logged in to access this area.</Text>
    </Box>
    <Flex>
      <Box p={10} flex="1">
        <Button id="login-button" width="100%" onClick={props.openLogin}>Login</Button>
      </Box>
      <Box p={10} flex="1">
        <Button id="back-button" width="100%" onClick={props.goBack}>Back</Button>
      </Box>
    </Flex>
  </Container>
);

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ goBack, openLogin }, dispatch);
}

LoginWarning.propTypes = {
  openLogin: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
};

export default connect(null, matchDispatchToProps)(LoginWarning);
