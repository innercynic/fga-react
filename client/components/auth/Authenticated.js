import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Box } from 'grid-styled';

export const AuthState = (props, shouldDisplay) =>
  (props.isLoggedIn === shouldDisplay ? <Box {...props}>{props.children}</Box> : null);

function mapStateToProps(state) {
  return {
    isLoggedIn: state.auth.isLoggedIn,
  };
}

AuthState.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export const IfAuthenticated = connect(mapStateToProps)(props => AuthState(props, true));
export const IfNotAuthenticated = connect(mapStateToProps)(props => AuthState(props, false));

