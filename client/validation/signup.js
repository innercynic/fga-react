const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Required';
  } else if (values.name.length > 31) {
    errors.name = 'Must be 30 characters or less';
  }

  if (!values.email) {
    errors.email = 'Required';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length < 8) {
    errors.password = 'Must be at least 8 characters';
  }

  if (!values.password_verification) {
    errors.password_verification = 'Required';
  } else if (values.password_verification !== values.password) {
    errors.password_verification = "Passwords don't match";
  }

  if (!values.image) {
    errors.image = 'Required';
  }

  return errors;
};

export default validate;

