import validate from '../song';

describe("song validation function", () => {
  test("it should validate an song", () => {
    const mockSong = {name: "Bob", artist:"Bob"};
    var errors = validate(mockSong);
    expect(Object.keys(errors).length).toBe(0);
  });

  test("it should return errors if validation fails", () => {
    const mockSong = {name: "TooLongNameTooLongNameTooLongName"};
    var errors = validate(mockSong);
    expect(errors).toMatchSnapshot();
  });
});