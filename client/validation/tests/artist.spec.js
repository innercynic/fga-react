import validate from '../artist';

describe("artist validation function", () => {
  test("it should validate an artist", () => {
    const mockArtist = {name: "Bob", image:"/bob/thumbnail"};
    var errors = validate(mockArtist);
    expect(Object.keys(errors).length).toBe(0);
  });

  test("it should return errors if validation fails", () => {
    const mockArtist = {name: "TooLongNameTooLongNameTooLongName"};
    var errors = validate(mockArtist);
    expect(errors).toMatchSnapshot();
  });
});