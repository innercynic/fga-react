const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Required';
  } else if (values.name.length > 31) {
    errors.name = 'Must be 30 characters or less';
  }

  if (!values.image) {
    errors.image = 'Required';
  }

  return errors;
};

export default validate;
