import {
  sortSetlistMultiplePass,
  sortSetlistByTuningAlphabetically,
  sortSetlistByNameAlphabetically,
  sortSetlistByArtistAlphabetically,
  getSetlistTuningInstructions,
} from '../../libs/songComparer';

export const SORT_SETLIST = 'SORT_SETLIST';
export const PROCESS_TUNING_INSTRUCTIONS = 'PROCESS_TUNING_INSTRUCTIONS';

export const BY_TUNING_EFFICIENCY = 'BY_TUNING_EFFICIENCY';
export const BY_NAME_ALPHABETICALLY = 'BY_NAME_ALPHABETICALLY';
export const BY_TUNING_ALPHABETICALLY = 'BY_TUNING_ALPHABETICALLY';
export const BY_ARTIST = 'BY_ARTIST';

export const songComparerMiddleware = store => next => (action) => {
  let result;

  if (action.type === SORT_SETLIST) {
    switch (action.sortType) {
      case BY_TUNING_EFFICIENCY:
        result = sortSetlistMultiplePass(action.payload, 20);
        break;
      case BY_NAME_ALPHABETICALLY:
        result = sortSetlistByNameAlphabetically(action.payload);
        break;
      case BY_TUNING_ALPHABETICALLY:
        result = sortSetlistByTuningAlphabetically(action.payload);
        break;
      case BY_ARTIST:
        result = sortSetlistByArtistAlphabetically(action.payload);
        break;
    }
  }

  if (action.type === PROCESS_TUNING_INSTRUCTIONS) {
    result = getSetlistTuningInstructions(action.payload);
  }

  if (result) {
    store.dispatch(action.successAction(result));
  }

  next(action);
};
