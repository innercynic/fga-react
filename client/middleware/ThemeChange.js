import Modal from 'react-modal';

import { getThemes } from '../themes';
import { SWITCH_THEME } from '../actions/user/settings';

// TODO Change after themeing is fully implemented
Modal.defaultStyles = {
  overlay: {
    position: 'fixed',
    backgroundColor: getThemes().darkfuture.backgrounds.modalOverlay,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1000,
  },
};

export const themeChangeMiddleware = store => next => (action) => {
  if (action.type === SWITCH_THEME) {
    const theme = getThemes()[action.payload];

    if (theme) {
      Modal.defaultStyles = {
        overlay: {
          position: 'fixed',
          backgroundColor: theme.backgrounds.modalOverlay,
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 1000,
        },
      };
    }
  }

  next(action);
};
