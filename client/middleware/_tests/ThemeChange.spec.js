import sinon from 'sinon';
import Modal from 'react-modal';

import * as Themes from '../../themes';
import MockData from '../../utils/test/MockData';
import { SWITCH_THEME } from '../../actions/user/settings'
import { themeChangeMiddleware } from '../ThemeChange';

const getThemesStub = sinon.stub(Themes, 'getThemes').returns(MockData.mockThemes);
let mockStore, mockNext, withStore, withNext;

beforeEach(() => {
  mockStore = { dispatch: jest.fn() };
  mockNext = jest.fn();

  withStore = themeChangeMiddleware(mockStore);
  withNext = withStore(mockNext);
});

test('it calls next when run', () => {
  withNext({ type:"MOCK_ACTION" });

  expect(mockNext).toHaveBeenCalled();
});

test('it changes React Modals default background color', () => {
  let mockAction = {
    type: SWITCH_THEME,
    payload: MockData.mockThemeName,
  };

  const expectModalBackgroundColor = MockData.mockThemes[MockData.mockThemeName].backgrounds.modalOverlay

  withNext(mockAction);

  expect(Modal.defaultStyles.overlay.backgroundColor).toEqual(expectModalBackgroundColor);
});