import sinon from 'sinon';

import * as SongComparer from '../../../libs/songComparer';
import {
  SORT_SETLIST,
  BY_TUNING_EFFICIENCY,
  PROCESS_TUNING_INSTRUCTIONS,
  songComparerMiddleware
} from '../SongComparer';

const efficiencySortStub = sinon.stub(SongComparer, 'sortSetlistMultiplePass').returns([]);
const tuningInstructionsStub = sinon.stub(SongComparer, 'getSetlistTuningInstructions').returns({});
let mockStore, mockNext, withStore, withNext;

beforeEach(() => {
  mockStore = { dispatch: jest.fn() };
  mockNext = jest.fn();

  withStore = songComparerMiddleware(mockStore);
  withNext = withStore(mockNext);
});

test('it calls next when run', () => {
  withNext({ type:"MOCK_ACTION" });

  expect(mockNext).toHaveBeenCalled();
});

test('it processes a setlist and dispatches success action', () => {
  let mockAction = {
    type: SORT_SETLIST,
    sortType: BY_TUNING_EFFICIENCY,
    payload: [],
    successAction: jest.fn(),
  };

  withNext(mockAction);

  expect(mockStore.dispatch).toHaveBeenCalled();
  expect(mockAction.successAction).toHaveBeenCalled();
  expect(efficiencySortStub.called).toBeTruthy();
});

test('it generates tuning instructions for a setlist and dispatches success action', () => {
  let mockAction = {
    type: PROCESS_TUNING_INSTRUCTIONS,
    payload: [],
    successAction: jest.fn(),
  };

  withNext(mockAction);

  expect(mockStore.dispatch).toHaveBeenCalled();
  expect(mockAction.successAction).toHaveBeenCalled();
  expect(tuningInstructionsStub.called).toBeTruthy();
});