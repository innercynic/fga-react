export const OPEN_LOGIN = 'OPEN_LOGIN';
export const CLOSE_LOGIN = 'CLOSE_LOGIN';
export const CLEAR_LOGIN_ERROR = 'CLEAR_LOGIN_ERROR';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export const openLogin = () => ({
  type: OPEN_LOGIN,
});

export const closeLogin = () => ({
  type: CLOSE_LOGIN,
});

export const clearLoginError = () => ({
  type: CLEAR_LOGIN_ERROR,
});

export const login = userInfo => ({
  type: LOGIN,
  data: userInfo,
});

export const logout = () => ({
  type: LOGOUT,
});
