export const OPEN_SETTINGS = 'OPEN_SETTINGS';
export const CLOSE_SETTINGS = 'CLOSE_SETTINGS';
export const SWITCH_THEME = 'SWITCH_THEME';

export const openSettings = () => ({
  type: OPEN_SETTINGS,
});

export const closeSettings = () => ({
  type: CLOSE_SETTINGS,
});

export const switchTheme = themeName => ({
  type: SWITCH_THEME,
  payload: themeName,
});
