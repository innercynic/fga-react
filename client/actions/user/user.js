import { USER } from '../ActionConstants';
import { createResourceActionCreators } from '../factory/ActionCreatorFactory';

export const {
  requestUser,
  createUser,
  selectUser,
  clearSelectedUser,
} = createResourceActionCreators(USER, true);
