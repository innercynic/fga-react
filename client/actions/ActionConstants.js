export const REQUEST_ = 'REQUEST_';
export const CREATE_ = 'CREATE_';
export const SELECT_ = 'SELECT_';
export const CLEAR_ = 'CLEAR_';

export const _SUCCESS = '_SUCCESS';
export const _FAIL = '_FAIL';

export const ARTIST = 'ARTIST';
export const ARTISTS = 'ARTISTS';

export const SONG = 'SONG';
export const SONGS = 'SONGS';

export const SETLIST = 'SETLIST';
export const SETLISTS = 'SETLISTS';

export const USER = 'USER';
