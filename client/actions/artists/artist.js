import { ARTIST } from '../ActionConstants';
import { createResourceActionCreators } from '../factory/ActionCreatorFactory';

export const IS_ADDING_ARTIST = 'IS_ADDING_ARTIST';

export const setIsAddingArtist = isAdding => ({
  type: IS_ADDING_ARTIST,
  payload: isAdding,
});

export const {
  requestArtist,
  createArtist,
  selectArtist,
  clearSelectedArtist,
} = createResourceActionCreators(ARTIST, true);
