import { ARTISTS } from '../ActionConstants';
import { createCollectionActionCreators } from '../factory/ActionCreatorFactory';

export const {
  requestArtists,
  clearArtists,
} = createCollectionActionCreators(ARTISTS);
