import { SETLIST } from '../ActionConstants';
import { createResourceActionCreators } from '../factory/ActionCreatorFactory';

import {
  SORT_SETLIST,
  PROCESS_TUNING_INSTRUCTIONS,
  BY_TUNING_EFFICIENCY,
  BY_NAME_ALPHABETICALLY,
  BY_TUNING_ALPHABETICALLY,
  BY_ARTIST,
} from '../../middleware/SongComparer';

export const ADD_TO_SETLIST = 'ADD_TO_SETLIST';
export const REMOVE_FROM_SETLIST = 'REMOVE_FROM_SETLIST';
export const REPOSITION_SETLIST_ITEM = 'REPOSITION_SETLIST_ITEM';
export const IS_ADDING_SETLIST_NAME = 'IS_ADDING_SETLIST_NAME';
export const SET_SETLIST_NAME = 'SET_SETLIST_NAME';
export const SET_NEW_SETLIST = 'SET_NEW_SETLIST';
export const SET_SETLIST_SONGS = 'SET_SETLIST_SONGS';
export const SET_TUNING_INSTRUCTIONS = 'SET_TUNING_INSTRUCTIONS';

export const addToSetlist = item => ({
  type: ADD_TO_SETLIST,
  payload: item,
});

export const isAddingSetlistName = isAdding => ({
  type: IS_ADDING_SETLIST_NAME,
  payload: isAdding,
});

export const setSetlistName = name => ({
  type: SET_SETLIST_NAME,
  payload: name,
});

export const setSetlistSongs = songs => ({
  type: SET_SETLIST_SONGS,
  payload: songs,
});

export const removeFromSetlist = itemIndex => ({
  type: REMOVE_FROM_SETLIST,
  payload: itemIndex,
});

export const repositionSetlistItem = (oldIndex, newIndex) => ({
  type: REPOSITION_SETLIST_ITEM,
  oldIndex,
  newIndex,
});

export const setNewSetlist = () => ({
  type: SET_NEW_SETLIST,
});

export const sortSetlist = (setlist, sortType) => ({
  type: SORT_SETLIST,
  payload: setlist,
  successAction: setSetlistSongs,
  sortType,
});

export const SortTypes = {
  BY_TUNING_EFFICIENCY,
  BY_NAME_ALPHABETICALLY,
  BY_TUNING_ALPHABETICALLY,
  BY_ARTIST,
};

export const setTuningInstructions = instructions => ({
  type: SET_TUNING_INSTRUCTIONS,
  payload: instructions,
});

export const processTuningInstructions = setlist => ({
  type: PROCESS_TUNING_INSTRUCTIONS,
  payload: setlist,
  successAction: setTuningInstructions,
});

export const {
  requestSetlist,
  createSetlist,
  selectSetlist,
  clearSelectedSetlist,
} = createResourceActionCreators(SETLIST);
