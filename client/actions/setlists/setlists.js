import { SETLISTS } from '../ActionConstants';
import { createCollectionActionCreators } from '../factory/ActionCreatorFactory';

export const {
  requestSetlists,
  clearSetlists,
} = createCollectionActionCreators(SETLISTS);
