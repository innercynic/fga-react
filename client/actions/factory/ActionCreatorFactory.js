import {
  REQUEST_,
  CREATE_,
  SELECT_,
  CLEAR_,
} from '../../actions/ActionConstants';

export const createResourceActionCreators = (type, typeContainsFiles = false) => {
  const typeInCapitalCase = type.charAt(0).toUpperCase() + type.slice(1).toLowerCase();
  const actionCreators = {};

  actionCreators[`request${typeInCapitalCase}`] = ({ id, fields }) => ({
    type: REQUEST_ + type,
    id,
    fields,
  });

  actionCreators[`create${typeInCapitalCase}`] = (data, id) => ({
    type: CREATE_ + type,
    data,
    id,
    isMultiPart: typeContainsFiles,
  });

  actionCreators[`select${typeInCapitalCase}`] = instance => ({
    type: SELECT_ + type,
    payload: instance,
  });

  actionCreators[`clearSelected${typeInCapitalCase}`] = () => ({
    type: CLEAR_ + type,
  });

  return actionCreators;
};

export const createCollectionActionCreators = (type) => {
  const typeInCapitalCase = type.charAt(0).toUpperCase() + type.slice(1).toLowerCase();
  const actionCreators = {};

  actionCreators[`request${typeInCapitalCase}`] = ({ page = 1, fields, query }) => ({
    type: REQUEST_ + type,
    page,
    fields,
    query,
  });

  actionCreators[`clear${typeInCapitalCase}`] = () => ({
    type: CLEAR_ + type,
  });

  return actionCreators;
};
