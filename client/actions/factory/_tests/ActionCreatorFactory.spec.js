import {
  createResourceActionCreators,
  createCollectionActionCreators
} from '../ActionCreatorFactory';

describe('createResourceActionCreators', () => {

  test('it returns resource action creators using type given', () => {
    let mockType = "MOCK";

    const actionCreators = createResourceActionCreators(mockType);

    expect(actionCreators.requestMock).toBeTruthy();
  });  

  test('it returns a "create" action creators configured to be a resource type including files', () => {
    let mockType = "MOCK";

    const actionCreators = createResourceActionCreators(mockType, true);

    let action = actionCreators.createMock({name: "mockName"});

    expect(action).toEqual({
      "data": { "name": "mockName" },
      "isMultiPart": true,
      "type": "CREATE_MOCK"
    });
  });

});

describe('createCollectionActionCreators', () => {

  test('it returns collection action creators using type given', () => {
    let mockCollectionType = "MOCKS";

    const actionCreators = createCollectionActionCreators(mockCollectionType);

    expect(actionCreators.requestMocks).toBeTruthy();
  });  
  
});