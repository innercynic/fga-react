import { SONG } from '../ActionConstants';
import { createResourceActionCreators } from '../factory/ActionCreatorFactory';

export const REQUEST_SONGS_BY_TUNING = 'REQUEST_SONGS_BY_TUNING';

export const requestSongsByTuning = tuning => ({
  type: REQUEST_SONGS_BY_TUNING,
  payload: tuning,
});

export const {
  requestSong,
  createSong,
  selectSong,
  clearSelectedSong,
} = createResourceActionCreators(SONG, false);
