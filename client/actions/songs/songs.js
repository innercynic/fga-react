import { SONGS } from '../ActionConstants';
import { createCollectionActionCreators } from '../factory/ActionCreatorFactory';

export const {
  requestSongs,
  clearSongs,
} = createCollectionActionCreators(SONGS);
