import { css } from 'styled-components';

export const clickable = css`
  cursor: pointer;

  &:hover {
    color: ${props => props.theme.fonts.colors.input.hover};
    background-color: ${props => props.theme.backgrounds.input.hover};
    box-shadow: 0 0 3pt 2pt ${props => props.theme.borders.colors.shadow};
  }
`;
