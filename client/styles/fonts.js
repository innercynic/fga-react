import { css } from 'styled-components';

const fontFaceDefinitions = css`
  @font-face {
    font-family: 'icons';
    src: url('/assets/fonts/icons/icons.eot?32337706');
    src: url('/assets/fonts/icons/icons.eot?32337706#iefix') format('embedded-opentype'),
         url('/assets/fonts/icons/icons.woff2?32337706') format('woff2'),
         url('/assets/fonts/icons/icons.woff?32337706') format('woff'),
         url('/assets/fonts/icons/icons.ttf?32337706') format('truetype'),
         url('/assets/fonts/icons/icons.svg?32337706#icons') format('svg');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'OrkneyRegular';
    src: url('/assets/fonts/orkney/Orkney Regular.eot?32337706');
    src: url('/assets/fonts/orkney/Orkney Regular.eot?32337706#iefix') format('embedded-opentype'),
         url('/assets/fonts/orkney/Orkney Regular.woff2?32337706') format('woff2'),
         url('/assets/fonts/orkney/Orkney Regular.woff?32337706') format('woff'),
         url('/assets/fonts/orkney/Orkney Regular.ttf?32337706') format('truetype'),
         url('/assets/fonts/orkney/Orkney Regular.svg?32337706#orkney') format('svg');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'aileronsregular';
    src: url('/assets/fonts/ailerons/ailerons-typeface-webfont.woff2') format('woff2'),
         url('/assets/fonts/ailerons/ailerons-typeface-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'Orbitron';
    src: url('/assets/fonts/orbitron/Orbitron-Regular.woff2') format('woff2'),
         url('/assets/fonts/orbitron/Orbitron-Regular.ttf') format('ttf');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'Patrick Hand SC';
    src: url('/assets/fonts/patrickhandsc/PatrickHandSC-Regular.eot');
    src: url('/assets/fonts/patrickhandsc/PatrickHandSC-Regular.woff2') format('woff2'),
         url('/assets/fonts/patrickhandsc/PatrickHandSC-Regular.woff') format('woff'),
         url('/assets/fonts/patrickhandsc/PatrickHandSC-Regular.ttf') format('truetype'),
         url('/assets/fonts/patrickhandsc/PatrickHandSC-Regular.svg#PatrickHandSC-Regular') format('svg'),
         url('/assets/fonts/patrickhandsc/PatrickHandSC-Regular.eot?#iefix') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
}


`;

export default fontFaceDefinitions;
