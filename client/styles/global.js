import { injectGlobal } from 'styled-components';

import fontFaceDefinitions from './fonts';

// eslint-disable-next-line no-unused-expressions
injectGlobal`
  ${fontFaceDefinitions}

  html,
  body {
    margin: 0;
    padding: 0;
    height: 100%;
    width: 100%;
    overflow:hidden;
    font-size: 16px;
    font-family: 'Open Sans', Helvetica, Arial, sans-serif;
  }

  * {
    box-sizing: border-box;
  }
  
  #root {
    height: 100%;
  }
`;
