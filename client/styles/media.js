import { css } from 'styled-components';

export const BASE_SIZE = 16;

export const sizes = {
  phone: 0,
  tabletPortrait: 600,
  tabletLandscape: 900,
  desktop: 1200,
  hd: 1600,
};

// Iterate through the sizes and create media query templates
const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css `
    @media (min-width: ${sizes[label] / BASE_SIZE}em) {
      ${css(...args)}
    }
  `;
  return acc;
}, {});

export default media;
