/**
 * Contains content codes for font icons
 */
export const getIcons = () => ({
  search: '\\E800',
  settings: '\\E801',
  login: '\\E803',
  logout: '\\E804',
  signup: '\\F234',
  email: '\\F0E0',
  password: '\\E802',
  error: '\\E805',
  person: '\\E806',
  alphabetically: '\\F15D',
  alphabeticallyreverse: '\\F15E',
  numerically: '\\F162',
  sort: '\\F160',
  sorthandle: '\\F0DC',
  instructions: '\\E807',
  listadd: '\\E808',
  add: '\\E80B',
  edit: '\\E80A',
  save: '\\E809',
  remove: '\\E80E',
  back: '\\E80D',
  song: '\\E80C',
});

