import React from 'react';

import { increaseSizeByPercent, decreaseSizeByPercent, roundDecimal } from '../CSSUtils';

test('it rounds decimals to given significant figures', () => {
  expect(roundDecimal(1.13537, 1)).toEqual(1.1);
  expect(roundDecimal(1.13537, 2)).toEqual(1.14);
  expect(roundDecimal(1.13537, 4)).toEqual(1.1354);
  expect(roundDecimal(0.243, 2)).toEqual(0.24);
  expect(roundDecimal(0.243, 5)).toEqual(0.243);
  expect(roundDecimal(0.243, 0)).toEqual(0);
});

test('size changers handle text based css size unit', () => {
  let increasedRem = increaseSizeByPercent('1rem', 0.5);
  let decreasedRem = decreaseSizeByPercent('1rem', 0.5);

  expect(increasedRem).toEqual('1.5rem');
  expect(decreasedRem).toEqual('0.5rem');
});

test('size changers handle percent based css size unit', () => {
  let increasedRem = increaseSizeByPercent('100%', 0.3);
  let decreasedRem = decreaseSizeByPercent('100%', 0.3);

  expect(increasedRem).toEqual('130%');
  expect(decreasedRem).toEqual('70%');
});

test('size changers handle floating point values', () => {
  let increasedRem = increaseSizeByPercent('1.43px', 0.789);
  let decreasedRem = decreaseSizeByPercent('1.4px', 0.1);

  expect(increasedRem).toEqual('2.56px');
  expect(decreasedRem).toEqual('1.26px');
});