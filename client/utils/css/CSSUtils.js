function getSizeMultiplicaitonData(cssSize, percentAsDecimal) {
  const [value, unit] = cssSize.match(/^([+-]?(?:\d+|\d*\.\d+))([a-z]*|%)$/).splice(1);
  const valueAsNumber = parseFloat(value);
  const sizeDifference = valueAsNumber * percentAsDecimal;

  return [valueAsNumber, sizeDifference, unit];
}

export function roundDecimal(value, decimals) {
  return Number(`${Math.round(`${value}e${decimals}`)}e-${decimals}`);
}

export function increaseSizeByPercent(cssSize, percentAsDecimal) {
  const [value, sizeDifference, unit] = getSizeMultiplicaitonData(cssSize, percentAsDecimal);
  return (roundDecimal(value + sizeDifference, 2)) + unit;
}

export function decreaseSizeByPercent(cssSize, percentAsDecimal) {
  const [value, sizeDifference, unit] = getSizeMultiplicaitonData(cssSize, percentAsDecimal);
  return (roundDecimal(value - sizeDifference, 2)) + unit;
}
