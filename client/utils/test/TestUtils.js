import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import configureMockStore from 'redux-mock-store';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { ReactWrapper } from 'enzyme';
import { reduxForm } from 'redux-form';
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import MockData from './MockData';

const TestUtils = {
  getMockStoreAndProvider,
  getMockThemeProvider,
  getMockForm,
};

export function getMockStoreAndProvider(storeData) {
  const mockStore = configureMockStore({form: formReducer});
  const store = mockStore(storeData ? storeData : {});

  return {
    mockStore: store,
    MockProviderWrapper: ({ children}) => (
    <Provider store={store}>
      {children}
    </Provider>
  )};
}

export function getMockForm(handleSubmit){
  return reduxForm({
    form: 'mock',
  })(({ children }) => (
    <form onSubmit={handleSubmit}>
      {children}
    </form>
  ));
}

export function getMockThemeProvider(themes, currentTheme){
  themes = themes ? themes : MockData.mockThemes;
  currentTheme = currentTheme ? currentTheme : MockData.mockThemeName;

  return ({ children}) => (
    <ThemeProvider theme={themes[currentTheme]}>
      {children}
    </ThemeProvider>
  );
}

export default TestUtils;
