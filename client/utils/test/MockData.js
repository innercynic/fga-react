import mockUsers from './mocks/users';
import mockArtists from './mocks/artists';
import mockSongs from './mocks/songs';
import mockSetlists from './mocks/setlists';
import mockThemes, {mockThemeName} from './mocks/themes'

export default {
  mockArtists,
  mockSongs,
  mockSetlists,
  mockUsers,
  mockThemes,
  mockThemeName,
};