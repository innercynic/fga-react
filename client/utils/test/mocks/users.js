export const mockUserData = {
  name: "Mock User Name",
  image: "images\\mockUserImage.jpg",
  thumbnail: "thumbnails\\mockUserThumbnail.jpg",
};

const mockUsers = [{
  _id: "1",
  email: "mock1@email.com",
  ...mockUserData,
},{
  _id: "2",
  email: "mock2@email.com",
  ...mockUserData,
},{
  _id: "3",
  email: "mock3@email.com",
  ...mockUserData,
}];

export default mockUsers;