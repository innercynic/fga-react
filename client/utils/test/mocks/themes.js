import { processThemes } from '../../../themes'
import { generateTheme } from '../../../themes/themegenerator';

const mockTheme =  generateTheme({
  THEME_NAME: 'Mock',

  BG_IMAGE: '/assets/images/mock.jpg',
  BG_OVERLAY_COLOR: 'transparent',
  BG_MODAL_OVERLAY_COLOR: 'transparent',

  COLOR_BG_PRIMARY: 'red',
  COLOR_BG_SECONDARY: 'blue',
  COLOR_BG_ACTIVE: 'green',

  COLOR_FONT_PRIMARY: 'blue',
  COLOR_FONT_SECONDARY: 'red',
  COLOR_FONT_ERROR: 'black',
  COLOR_FONT_TITLE: 'white',

  COLOR_BORDER: 'yellow',

  FONT_FAMILY_TITLE: 'mockfont',
  FONT_FAMILY_HEADING: 'mockfont',
  FONT_FAMILY_TEXT: "'mockfont'",

  FONT_SIZE_TITLE: '3rem',
  FONT_SIZE_HEADING: '1.5rem',
  FONT_SIZE_TEXT: '1rem',

  BORDER_SIZE_BASE: '1.5px',

  BORDER_RADIUS_BASE: '10px',

  SVG_FILL: 'pink',
  SVG_STROKE: 'transparent',
});

export const mockThemeName = "mocktheme1";

const mockThemes = processThemes({
  mocktheme1: {...mockTheme,
    themename: mockThemeName,
  },
  mocktheme2: {...mockTheme,
    themename: "mocktheme2",
  },
});

export default mockThemes;
