const mockArtistData = {
  name: "Mock Artist Name",
  info: "Mock artist information",
  image: "images\\mockArtistImage.jpg",
  thumbnail: "thumbnails\\mockArtistThumbnail.jpg",
};

const mockArtists = [{
  _id: "1",
  ...mockArtistData,
},{
  _id: "2",
  ...mockArtistData,
},{
  _id: "3",
  ...mockArtistData,
}];

export default mockArtists;