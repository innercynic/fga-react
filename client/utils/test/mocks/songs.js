import mockArtists from './artists';

const mockSongData = {
  name: "Mock Song Name",
  album: "Mock Song Album",
  artist: mockArtists[0],
  techniques: ["Tech1", "Tech2"],
  stringtunings: ["E", "A", "D", "G", "B", "E"],
};

const mockSongs = [{
  _id: "1",
  ...mockSongData,
},{
  _id: "2",
  ...mockSongData,
},{
  _id: "3",
  ...mockSongData,
}];

export default mockSongs;