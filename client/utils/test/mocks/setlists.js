import mockSongs from './songs';

const mockSetlistData = {
  name: "Mock Setlist Name",
  songs: mockSongs,
};

const mockSetlists = [{
  _id: "1",
  ...mockSetlistData,
},{
  _id: "2",
  ...mockSetlistData,
},{
  _id: "3",
  ...mockSetlistData,
}];

export default mockSetlists;