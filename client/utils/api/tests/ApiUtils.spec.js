import sinon from 'sinon';

import * as ApiUtils from '../ApiUtils';
import * as ActionToApiMappings from '../ActionToApiMappings';

sinon.stub(ActionToApiMappings, 'getEndPointToActionMapping').returns('/mock');

describe('createUrlFromAction', () => {
  test('should create a url with only a type property in config object param', () => {
    const mockAction = { type: 'MOCK_ACTION' };

    expect(ApiUtils.createUrlFromAction(mockAction)).toMatchSnapshot();
  });

  test('should create a url with type and id properties in config object param', () => {
    const mockAction = { type: 'MOCK_ACTION', id: 55 };

    expect(ApiUtils.createUrlFromAction(mockAction)).toMatchSnapshot();
  });
});

describe('buildQueryString', () => {
  test('should build a query string', () => {
    const mockQueryParams = { page: 1, fields: 'name,thumbnail', query: 'name~' };

    expect(ApiUtils.buildQueryString(mockQueryParams)).toMatchSnapshot();
  });

  test('should return an empty string if non-object is passed', () => {
    expect(ApiUtils.buildQueryString(55)).toBe("");
  });
});

describe("createFormData", () => {
  test('should create a FormData object', () => {
    const mockDataObject = {name: "Bob Smith"};

    expect(ApiUtils.createFormData(mockDataObject)).toMatchSnapshot();
  });

  test('should return null if non-object is passwrd', () => {
    expect(ApiUtils.createFormData()).toBe(null);
  });
});
