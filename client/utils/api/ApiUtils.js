import { getEndPointToActionMapping } from './ActionToApiMappings';


export function buildQueryString(queryStringElements) {
  let queryString = '';
  let queryCount = 0;

  if (typeof queryStringElements === 'object') {
    Object.keys(queryStringElements).forEach((key) => {
      if (queryStringElements[key] == null) return;

      if (queryCount === 0) queryString += '?';
      else queryString += '&';

      queryString += `${key}=${queryStringElements[key]}`;
      queryCount += 1;
    });
  }

  return queryString;
}

export function createUrlFromAction({ type, id, page, fields, query }) {
  return `${getEndPointToActionMapping(type)}`
  + `${id ? `/${id}` : ''}`
  + `${query || fields || page ? buildQueryString({ page, fields, query }) : ''}`;
}

export function createFormData(dataObject) {
  const multipartFormData = new window.FormData();


  if (typeof dataObject === 'object') {
    Object.keys(dataObject).map(key => multipartFormData.append(key, dataObject[key]));
  } else {
    return null;
  }

  return multipartFormData;
}
