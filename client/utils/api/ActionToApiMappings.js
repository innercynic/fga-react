import { LOGIN } from '../../actions/user/auth';
import { CREATE_USER } from '../../actions/user/user';

import { REQUEST_, CREATE_, SONG, SONGS, ARTIST, ARTISTS, SETLIST, SETLISTS, USER } from '../../actions/ActionConstants';

const BASE_URL = '/api';

const ARTISTS_ENDPOINT = `${BASE_URL}/artists`;
const SONGS_ENDPOINT = `${BASE_URL}/songs`;
const SETLISTS_ENDPOINT = `${BASE_URL}/setlists`;
const USERS_ENDPOINT = `${BASE_URL}/users`;
const LOGIN_ENDPOINT = '/auth/login';

const endPointToActionMappings = {};

endPointToActionMappings[LOGIN] = LOGIN_ENDPOINT;

endPointToActionMappings[REQUEST_ + SONGS] = SONGS_ENDPOINT;
endPointToActionMappings[REQUEST_ + SONG] = SONGS_ENDPOINT;
endPointToActionMappings[CREATE_ + SONG] = SONGS_ENDPOINT;

endPointToActionMappings[REQUEST_ + ARTISTS] = ARTISTS_ENDPOINT;
endPointToActionMappings[REQUEST_ + ARTIST] = ARTISTS_ENDPOINT;
endPointToActionMappings[CREATE_ + ARTIST] = ARTISTS_ENDPOINT;

endPointToActionMappings[REQUEST_ + SETLISTS] = SETLISTS_ENDPOINT;
endPointToActionMappings[REQUEST_ + SETLIST] = SETLISTS_ENDPOINT;
endPointToActionMappings[CREATE_ + SETLIST] = SETLISTS_ENDPOINT;

endPointToActionMappings[CREATE_ + USER] = USERS_ENDPOINT;

export function getEndPointToActionMapping(actionType) {
  return endPointToActionMappings[actionType];
}
