module.exports = {
	extends: 'airbnb',
	rules: {
		'linebreak-style': 0,
		'no-underscore-dangle': 0,
		'import/prefer-default-export': 0,
		'import/no-named-as-default': 0,
		'class-methods-use-this': 0,
		'react/sort-comp': 0,
		'react/jsx-filename-extension': [1, {
			"extensions": [".js", ".jsx"]
		}],
	},
	globals: {
		window: true,
		document: true,
		localStorage: true,
	},
	"env": {
		"jest": true
	}
};
