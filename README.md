# Fingerstylist #

This application is designed to allow users to join a community of fingerstyle guitarists who collaborate to create a database of knowledge about the art.
Users can contribute by uploading data about songs/artists/albums/techniques. It also allows users to search by tuning to find songs with matching tunings or similar using a tuning similarity algorithm.

### How do I get set up? ###

* Download or clone repo
* Run 'npm install'
* Run 'npm start' to start server and build webpack bundle
* Run 'npm run test' to run unit tests

### Technologies ###

* React/Redux for front end and NodeJS with Express and MongoDB for backend
* Jest for unit testing
* React router for routing
* JWT based authentication
* Styled Components for CSS styles and themeing capability

### Current Features ###

* Create account/Login
* Input song and artist data
* Change themes
* Create and sort setlists
* Generate tuning instructions for playing a set list

### Future Features ###

* Review and rate covars by other guitarists, and upload your own videos to be reviewed.
* Gamification to encourage community contribution.
* Comment system
* Technique archive
* UI theme generator
