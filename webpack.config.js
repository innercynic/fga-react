var path = require('path');
var LiveReloadPlugin = require('webpack-livereload-plugin');
var esLintConfig = require('./.eslintrc');
var webpack = require('webpack');

module.exports = {
  entry: ['babel-polyfill', './client/index.js'],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build')
  },
  plugins: [
    new LiveReloadPlugin(),
    // new webpack.optimize.UglifyJsPlugin({
    //   compress: {
    //     warnings: false,
    //     pure_getters: true,
    //     unsafe: true,
    //     unsafe_comps: true,
    //     screw_ie8: true
    //   },
    //   output: {
    //     comments: false,
    //   },
    // }),
  ],
  module: {
        loaders: [
            { 
              enforce: "pre",
              test: /\.js$/,
              exclude: /node_modules/, 
              loader: "eslint-loader",
              options: {
                fix: true,
              },
            },
            { 
            	test: /\.js$/,
              exclude: /node_modules/, 
            	loader: "babel-loader",
		          options: {
  		          presets: ["es2015", "react"],
  		          plugins: ["transform-object-rest-spread"]
		          }
            },
            {
              test: /\.css$/,
              use: [ 'style-loader', 'css-loader' ]
            }
        ]
    }
};