const REQUEST_ = "REQUEST_";


const ARTIST = "ARTIST";

const createGenericReducer = (type) => {
  let baseState = {
    new: null,
    selected: null,
    isAdding: false,
    isFetching: false,
    isPostingData: false,
    error: '',
    errorStatusCode: '',
  };

  return (state = baseState, action) => {
    switch (action.type) {
      case REQUEST_ + type:
        return {
          ...state,
          selected: null,
          isFetching: true,
        };

      default:
        return state;
    }
  }
}

var artist = createGenericReducer(ARTIST);


var action = {
  type: "REQUEST_ARTIST"
};

console.log(artist(action));