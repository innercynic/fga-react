//export path config (used by image uploads etc)
export default {
	appRoot: __dirname,
	staticPath: "build",
	imageUploadPath: "assets\\images\\uploads"
}