import passport from 'passport'
// import User from '../../api/user/user.model'
// import LocalStrategy from 'passport-local';

// import localStrategy from './local'
import jwtStrategy from './jwt'

export default function initializePassports(applicationRouter) {
	configureUserSerialization()

	// passport.use(localStrategy)
	passport.use(jwtStrategy)
	
	applicationRouter.use( passport.initialize() )
	applicationRouter.use( passport.session() )
}

function configureUserSerialization(){
	passport.serializeUser(function(user, done) {
	  done(null, user._id)
	})

	passport.deserializeUser(function(id, done) {
	  User.findById(id, function(err, user) {
	    done(err, user)
	  })
	})
}