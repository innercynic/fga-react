import LocalStrategy from 'passport-local'
import User from '../../api/user/user.model'

const localStrategyOptions = { usernameField: 'email' }

const localStategy = new LocalStrategy(localStrategyOptions,
	function(username, password, done) {
		User.findOne({ email: username.toLowerCase() }, function(err, user) {
			if (err) 
				return done(err)

			if (!user) 
				return done(null, false, { message: 'This email is not registered.' })

			if (!user.authenticate(password)) 
				return done(null, false, { message: 'This password is not correct.' })

			return done(null, user)
		});
	}
)

export default localStategy