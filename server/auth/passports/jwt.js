import { Strategy as JwtStrategy }  from 'passport-jwt'
import { ExtractJwt } from 'passport-jwt'

import User from '../../api/user/user.model'

import jwtConfig from '../jwt.config'

const jwtOptions = { 
	jwtFromRequest: ExtractJwt.fromAuthHeader(),
	secretOrKey: jwtConfig.secretOrKey
}

const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {  
  User.findById(payload._id, function(err, user) {
    if (err) { return done(err, false); }

    if (user) {
      done(null, user);
    } else {
      done(null, false);
    }
  });
});

export default jwtLogin