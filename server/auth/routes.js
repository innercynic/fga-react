import express from 'express'
import passport from 'passport'

import userController from '../api/user/user.controller'

var authRouter = express.Router();

// const requireLogin = passport.authenticate('local', { session: false });  
const requireAuth = passport.authenticate('jwt', { session: false });  

authRouter.post('/login', userController.login)

export default authRouter