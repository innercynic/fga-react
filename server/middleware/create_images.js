import { createImages } from '../utils/ImageUtils'

export const createImageAndThumbnail = (req, res, next) => {
  if(!req.file) return res.json({message: "File not uploaded: only JPG/JPEG/PNG images are accepted."})
  createImages(req.file.filename, (err, imagePaths) => {
    if (err) return this.handleError(res, err)

    req.body.image = imagePaths.imageWebPath
    req.body.thumbnail = imagePaths.thumbnailWebPath
    req.body.imageFilePath = imagePaths.imageStoragePath
    req.body.thumbnailFilePath = imagePaths.thumbnailStoragePath
    next()
  })
}