import { createImages } from '../utils/ImageUtils'
import passport from 'passport'
import { ExtractJwt } from 'passport-jwt'

export const populateUserData = (req, res, next) => {
  req.body.user = req.user._id;

  next()
}

export const populateUserQuery = (req, res, next) => {
  let userQuery = 'user=' + req.user._id;

  if(req.query.query){
    req.query.query += ',' + userQuery;
  }else{
    req.query.query = userQuery
  }

  next()
}