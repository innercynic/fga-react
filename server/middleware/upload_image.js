import multer from 'multer'
import pathConfig from '../../config'

const ACCEPTED_IMAGE_TYPES = ["image/png", "image/jpeg"]

export const upload = multer({ 
  dest: pathConfig.staticPath + '\\' + pathConfig.imageUploadPath,  
  fileFilter: (req, file, cb) => {
    if(!file.mimetype || ACCEPTED_IMAGE_TYPES.indexOf(file.mimetype) !== -1){
      cb(null, true)
    } else {
      cb(null, false)
    }
  }
})