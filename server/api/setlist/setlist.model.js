import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-paginate'

import SongSchema from '../song/song.model'

let setlistSchema = new mongoose.Schema({
	name: String,
	songs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Song' }],
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
})

setlistSchema.plugin(mongoosePaginate);

export default mongoose.model('Setlist', setlistSchema)