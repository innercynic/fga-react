import AbstractBaseController from '../base.controller'
import Setlist from './setlist.model'

export class SetlistController extends AbstractBaseController {
	constructor(){
		super()

		this.Model = Setlist

		this.getFieldsToPopulate = this.getFieldsToPopulate.bind(this);
	}

	getFieldsToPopulate(){
		return {
			path: 'songs',
			populate: {
				path: 'artist',
				model: 'Artist'
			}
		};
	}
}

const setlistController = new SetlistController()

export default setlistController