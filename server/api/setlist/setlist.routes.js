import express from 'express'
import paginate from 'express-paginate'
import passport from 'passport'

import setlistController from './setlist.controller'

import { populateUserData, populateUserQuery } from '../../middleware';

const router = express.Router()

router.use(paginate.middleware(10, 50));

router.get('/', passport.authenticate('jwt', {
  session: false
}), populateUserQuery, setlistController.getAll)

router.get('/:id', setlistController.getById)

router.post('/', passport.authenticate('jwt', {
  session: false
}), populateUserData, setlistController.create)

router.post('/:id', setlistController.update)
router.delete('/:id', setlistController.delete)

export default router