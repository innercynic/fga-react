import paginate from 'express-paginate'
import mongoose from 'mongoose'

const EQUALS = "="
const LIKE = "~"

class AbstractBaseController {
	constructor(){
		if (new.target === AbstractBaseController) {
			throw new TypeError("Cannot instantiate AbstractBaseController directly, it must be subclassed.");
		}

		this.getAll = this.getAll.bind(this);
		this.getById = this.getById.bind(this);
		this.find = this.find.bind(this);
		this.create = this.create.bind(this);
		this.update = this.update.bind(this);
    this.save = this.save.bind(this);
		this.delete = this.delete.bind(this);

		this.mongooseQueryBuilder = this.mongooseQueryBuilder.bind(this);
		this.updateModelInstance = this.updateModelInstance.bind(this);
		this.handleError = this.handleError.bind(this);
		this.getFields = this.getFields.bind(this);
		this.getFieldsToPopulate = this.getFieldsToPopulate.bind(this);
	}

	getAll(req, res) {
		this.Model.paginate(this.mongooseQueryBuilder(req.query.query), {
				select: this.getFields(req),
				populate: this.getFieldsToPopulate(),
				page: req.query.page,
				limit: req.query.limit
			}, function(err, results) {
				if (err) return this.handleError(res, err)

				return res.status(200).json({
					hasMore: paginate.hasNextPages(req)(results.pages),
					data: results.docs
				});
			}
		);
	}

	getById(req, res) {
		console.log("finding by id")

		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return res.status(400).json({message: "Invalid ID"})
		}

		this.Model.findById(req.params.id, this.getFields(req), {
					populate: this.getFieldsToPopulate()
				}, (err, modelInstance) => {
	   	if (err) return this.handleError(res, err)

	    if (!modelInstance) {
	      return res.status(404).send({message: 'Not Found'});
	    }

	    return res.json(modelInstance);
	  })
	}

	find(req, res) {
		this.Model.find({name: new RegExp(req.body.searchValue, "i")}, (err, artists) => {
	    if (err) return this.handleError(res, err)
		  else return res.status(201).json(artists)
		});
	}

	create(req, res) {
	  let modelInstance = new this.Model(req.body);

    this.save(modelInstance, res);
	}

	update(req, res) {
	  this.Model.findById(req.params.id, (err, modelInstance) => {
	    if (err) return handleError(res, err)
	    if (!modelInstance) return res.status(404).send('Error updating, ' + this.Model.modelName + ' not found')

      this.save(this.updateModelInstance(modelInstance, req.body), res);
	  });
	}

	save(modelInstance, res){
    modelInstance.save((err, savedModelInstance) => {
      if (err) return this.handleError(res, err)

      this.Model.populate(modelInstance, this.getFieldsToPopulate(), (err, populatedModelInstance) => {
        if (err) return this.handleError(res, err)
        else return res.status(201).json(populatedModelInstance)
      })
    });
	}

	delete(req, res) {
	  this.Model.findById(req.params.id, (err, modelInstance) => {
	    if (err) return handleError(res, err)
	    if (!modelInstance) return res.status(404).send('Error deleting, ' + this.Model.modelName + ' not found')

	    modelInstance.remove(function(err) {
	      if (err) return this.handleError(res, err)
	      else return res.status(204).send('No Content')
	    })
	  })
	}

	updateModelInstance(modelInstance, updatedProps){
    return Object.assign(modelInstance, updatedProps)
	}

	getFieldsToPopulate(){
		return []
	}

	getFields(req){
		return req.query.fields ? req.query.fields : ''
	}

	handleError(res, err) {
	  return res.status(500).send(err)
	}

	mongooseQueryBuilder(queryString){
		if(queryString == null) return {}

		let queries = queryString.split(',')

		let queryObjects = queries.map((query) => {
			let operator = query.includes(LIKE) ? LIKE : (query.includes(EQUALS) ? EQUALS : null)
			if(operator == null) return;

			let fieldName = query.split(operator)[0]
			let fieldQueryValue = query.split(operator)[1]

			let queryObject = {};
			queryObject[fieldName] = ( operator === LIKE ? new RegExp(fieldQueryValue, "i") : fieldQueryValue )

			return queryObject
		})

		let mongooseQueryObject = queryObjects.reduce(function(result, currentObject) {
	    for(let key in currentObject) {
	        result[key] = currentObject[key];
	    }
    	return result;
		}, {});
		
		return mongooseQueryObject
	}
}

export default AbstractBaseController