import AbstractBaseController from '../base.controller'
import Artist from './artist.model'
import { createImages } from '../../utils/ImageUtils'

export class ArtistController extends AbstractBaseController {
	constructor() {
		super();

		this.Model = Artist
	}
}

const artistController = new ArtistController()

export default artistController