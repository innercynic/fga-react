import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-paginate'

const artistSchema = new mongoose.Schema({
  name: String,
  info: String,
  image: String,
  thumbnail: String,
  active: Boolean
})

artistSchema.plugin(mongoosePaginate);

export default mongoose.model('Artist', artistSchema)