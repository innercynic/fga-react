import express from 'express'
import paginate from 'express-paginate'

import artistController from './artist.controller'
import { upload, createImageAndThumbnail } from '../../middleware'

const router = express.Router()

router.use(paginate.middleware(10, 50));

router.get('/', artistController.getAll)
router.get('/:id', artistController.getById)
router.post('/', upload.single('image'), createImageAndThumbnail, artistController.create)
router.post('/search', artistController.find)
router.patch('/:id', artistController.update)
router.delete('/:id', artistController.delete)

export default router