import AbstractBaseController from '../base.controller'
import Song from './song.model'

import { getSimilarSongs } from '../../../libs/SongComparer'

export class SongController extends AbstractBaseController {
	constructor(){
		super()

		this.Model = Song

		this.deriveExtraFields = this.deriveExtraFields.bind(this);
		this.updateModelInstance = this.updateModelInstance.bind(this);
		this.findSongsWithSimilarTunings = this.findSongsWithSimilarTunings.bind(this);
		this.getAll = this.getAll.bind(this);
	}

	getFieldsToPopulate(){
		return ['artist'];
	}

	getAll(req, res) {
		const queryData = this.mongooseQueryBuilder(req.query.query);

		if(queryData.tuning){
			const stringtunings = queryData.tuning.match(/[ABCDEFG]#?/g);

			this.Model.find({}, null, { populate: this.getFieldsToPopulate() }, (err, songs) => { 
				if (err) return this.handleError(res, err);

				const similarityLevel = queryData.similaritylevel || 4;
				const similarSongs = getSimilarSongs({stringtunings}, songs, similarityLevel)

				return res.status(200).json(this.getSimilarSongsPage(similarSongs, req.query.page, req.query.limit));
			});

		}else{
			super.getAll(req, res);
		}
	}

	getSimilarSongsPage(songs, pageNumber, resultLimit){
		let data, hasMore;
		const pageStartIndex = pageNumber * resultLimit - resultLimit;
		const pageEndIndex = pageNumber * resultLimit;

		if(songs[pageEndIndex]){
			data = songs.slice(pageStartIndex, pageEndIndex)
			hasMore = true

		} else if (songs[pageStartIndex]){
			data = songs.slice(pageStartIndex)
			hasMore = false

		}else{
			data = [];
			hasMore = false

		}

		return { data, hasMore }
	}

	findSongsWithSimilarTunings(queryData, res){
		console.log(stringtunings)

		// let song = this.Model.findById(req.params.id)
		// let allOtherSongs = this.Model.find().where('_id').ne(req.params.id).exec()

		// Promise.all([song, allOtherSongs]).then(values => { 
  // 		var similar = getSimilarSongs(values[0], values[1], 2)
  // 		return res.json(similar)
		// });
	}

	deriveExtraFields(modelInstance) {
	  modelInstance.stringtuningsAsString = _.clone(modelInstance.stringtunings)
	  .reverse().toString().replace(/,/g, "");
	  modelInstance.name = _.clone(modelInstance.title).toLowerCase().replace(/ /g, "");

	  return modelInstance
	}

	updateModelInstance(modelInstance, updateProps){
		modelInstance = super.updateModelInstance(modelInstance, updateProps)
		modelInstance.stringtunings = req.body.stringtunings;
		modelInstance.links = req.body.links;
		modelInstance = this.deriveExtraFields(modelInstance);

		modelInstance.markModified('stringtunings');
		modelInstance.markModified('links');

		return modelInstance
	}
}

const songController = new SongController()

export default songController