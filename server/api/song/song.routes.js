import express from 'express'
import paginate from 'express-paginate'
import multer from 'multer'

import songController from './song.controller'

import pathConfig from '../../../config'

const router = express.Router()

router.use(paginate.middleware(10, 50));

router.get('/', songController.getAll)
router.get('/:id', songController.getById)
router.get('/:id/similartunings', songController.findSongsWithSimilarTunings)
router.get('/similartunings', songController.findSongsWithSimilarTunings)
router.post('/', songController.create)
router.patch('/:id', songController.update)
router.delete('/:id', songController.delete)

export default router