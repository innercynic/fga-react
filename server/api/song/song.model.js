import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-paginate'

let songSchema = new mongoose.Schema({
	title: String,
	name: String,
	artist: { type: mongoose.Schema.Types.ObjectId, ref: 'Artist' },
	album: String,
	stringtunings : [String],
	techniques : [String],
	stringtuningsAsString : String,
	media: String,
	tab: String,
	tabPaid: Boolean
})

songSchema.methods.processFields = () => {
  console.log(this)
};

songSchema.plugin(mongoosePaginate);

export default mongoose.model('Song', songSchema)