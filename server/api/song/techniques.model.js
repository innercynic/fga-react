import mongoose from 'mongoose'

let techniquesSchema = new mongoose.Schema({
	title: String,
	name: String,
	alternativeNames: [String],
	description: String,
	links: [String]
})

export default mongoose.model('Technique', techniquesSchema)