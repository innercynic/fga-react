import express from 'express'
import paginate from 'express-paginate'
import multer from 'multer'

import userController from './user.controller'
import { upload, createImageAndThumbnail } from '../../middleware'

var extractFormTextValues = multer();
const router = express.Router()

router.use(paginate.middleware(10, 50));

router.get('/', userController.getAll)
router.get('/:id', userController.getById)

router.post('/', 
  upload.single('image'), 
  createImageAndThumbnail,
  userController.create)

router.post('/search', userController.find)
router.patch('/:id', userController.update)
router.delete('/:id', userController.delete)

export default router