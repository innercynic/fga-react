import jwt from 'jsonwebtoken'

import AbstractBaseController from '../base.controller'
import User from './user.model'
import { deleteFiles } from '../../utils/ImageUtils'

import jwtConfig from '../../auth/jwt.config'


export class UserController extends AbstractBaseController {
	constructor() {
		super();

		this.Model = User
	}

	create(req, res) {
		User.findOne({ email: req.body.email }, (err, user) => {
			if (err) return this.handleError(res, err)

			if (user) {
				deleteFiles([ req.body.imageFilePath, req.body.thumbnailFilePath ])
				return res.status(409).json({
					message: "Email already registered."
				})
			} else {
				let newUser = new this.Model(req.body)
				newUser.save(err => {
					if (err) return this.handleError(res, err)
					else return res.status(201).json(UserController.createPublicUserObject(newUser))
				})
			}
		});

	}
	login(req, res) {
		let {
			email,
			password
		} = req.body

		if (!email || !password) {
			res.status(400).json({
				message: "Invalid username/password combination provided."
			})
		} else {
			User.findOne({
				email: email.toLowerCase()
			}, function(err, user) {
				let status, json = {}

				if (err) {
					status = 500
					json.message = "Server error during login, please contact administrator"
				} else if (!user) {
					status = 401
					json.message = "This email is not registered."
				} else if (!user.authenticate(password)) {
					status = 401
					json.message = "Incorrect password."
				} else {
					status = 200
					json.user = UserController.createPublicUserObject(user);
					json.token = 'JWT ' + UserController.generateToken(user)
				}

				res.status(status).json(json)
			});
		}

		
	}

	static createPublicUserObject(user){
		return {
			_id: user._id,
			email: user.email,
			name: user.name,
			image: user.image,
			thumbnail: user.thumbnail,
		};
	}

	static getJWTSigningEntity(user) {
		return {
			_id: user._id,
			email: user.email
		}
	}

	static generateToken(user) {
		return jwt.sign(UserController.getJWTSigningEntity(user), jwtConfig.secretOrKey, {
			expiresIn: 10080 // in seconds
		});
	}
}

const userController = new UserController()

export default userController