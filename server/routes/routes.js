import express from 'express'
import passport from 'passport'
import bodyParser from 'body-parser'

import authRouter from '../auth/routes'
import { generateRouterFromController } from './router.factory'

import artistRoutes from '../api/artist/artist.routes'
import songRoutes from '../api/song/song.routes'
import setlistRoutes from '../api/setlist/setlist.routes'
import userRoutes from '../api/user/user.routes'

import initializePassports from '../auth/passports'

const applicationRouter = express.Router()

applicationRouter.use( bodyParser.json() )
applicationRouter.use( bodyParser.urlencoded({extended:true}) );

initializePassports(applicationRouter);

applicationRouter.use('/auth', authRouter)

applicationRouter.use('/api/songs', songRoutes)
applicationRouter.use('/api/artists', artistRoutes)
applicationRouter.use('/api/setlists', setlistRoutes)
applicationRouter.use('/api/users', userRoutes)


export { applicationRouter }