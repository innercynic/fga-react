import express from 'express'
import paginate from 'express-paginate'

import multer from 'multer'

export function generateRouterFromController(controller){
	const router = express.Router()

	router.use(paginate.middleware(2, 50));

	router.get('/', controller.getAll)
	router.get('/:id', controller.getById)
	router.post('/', controller.create)
	router.patch('/:id', controller.update)
	router.delete('/:id', controller.delete)

	return router
}