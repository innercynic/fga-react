import path from 'path'
import fs from 'fs-extra'
import gm from 'gm'

import pathConfig from '../../config'

const IMAGE_UPLOAD_DIR = path.join(
	pathConfig.appRoot, 
	pathConfig.staticPath, 
	pathConfig.imageUploadPath
)
const IMAGE_UPLOAD_PATH = pathConfig.imageUploadPath
const IMAGE_DIR = "images"
const THUMBNAIL_DIR = "thumbnails"

const imageMagick = gm.subClass({imageMagick: true});

export const createImages = (filename, callback) => {
	const imagePaths = createPaths(filename);

	imageMagick(imagePaths.uploadedImagePath)
		.resize(300, 300)
		.write(imagePaths.imageStoragePath, err => {
			if (err) {
				callback(err)
			} else {
				//Crop thumbnail to be square
				getCropParameters(imagePaths.uploadedImagePath, (size, x, y) => {
					imageMagick(imagePaths.uploadedImagePath)
						.crop(size, size, x, y)
						.resize(100, 100)
						.write(imagePaths.thumbnailStoragePath, (err) => {
							fs.unlinkSync(imagePaths.uploadedImagePath)
							callback(err, imagePaths)
						})
				})
			}
		})
}

const createPaths = filename => {
	const imageWebPath = path.join(IMAGE_UPLOAD_PATH, IMAGE_DIR,  filename)
	const thumbnailWebPath = path.join(IMAGE_UPLOAD_PATH, IMAGE_DIR, THUMBNAIL_DIR,  filename)

	return {
		uploadedImagePath: path.join(IMAGE_UPLOAD_DIR, filename),
		imageStoragePath: path.join(IMAGE_UPLOAD_DIR, IMAGE_DIR, filename),
		thumbnailStoragePath: path.join(IMAGE_UPLOAD_DIR, IMAGE_DIR, THUMBNAIL_DIR, filename),
		imageWebPath: `/${imageWebPath.split('\\').join('/')}`,
		thumbnailWebPath: `/${thumbnailWebPath.split('\\').join('/')}`,
	}
}

const deleteFromFileSystem = fileSystem => filename => fileSystem.unlinkSync(filename)
const deleteFile = deleteFromFileSystem(fs)
export const deleteFiles = filenames => filenames.map(deleteFile)

const getCropParameters = (image, callback) => {
	imageMagick( image ).size( (err, {width, height}) => {
		if(err || !width || !height) return

		const cropAxis = width < height ? "Y" : "X"
		const difference = cropAxis === "Y" ? height - width : width - height
		const size = width < height ? width : height
		const x = cropAxis === "Y" ? 0 : (difference / 2)
		const y = cropAxis === "Y" ? (difference / 2) : 0

		callback(size, x, y)
	})
}