import express from 'express'
import mongoose from 'mongoose'
import path from 'path'
import morgan from 'morgan'
import compression from 'compression'

import pathConfig from '../config'
import { applicationRouter } from './routes/routes'

const app = express()
const serverPort = 80;

app.use(compression());
app.use(morgan('dev'))
app.use(applicationRouter) 
app.use(express.static('build', {etag: false, maxage : 0}))

app.get('/*', function (req, res) {
   res.sendFile(path.join(pathConfig.appRoot, pathConfig.staticPath, 'index.html'));
});

mongoose.connect("mongodb://localhost:27017/fingerstyleguitar", () => {
	console.log("Connected to MongoDB")

	app.listen(serverPort, () => {
		console.log("Running on port " + serverPort)
	})
})

